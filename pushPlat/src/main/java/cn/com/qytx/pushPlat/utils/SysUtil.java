package cn.com.qytx.pushPlat.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SysUtil {
	public static void  toClient(Object ret,HttpServletResponse response){
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("utf-8");
			response.setContentType("text/html; charset=utf-8"); 
			out = response.getWriter();
			out.println(ret);
		} catch (IOException e) {
			e.printStackTrace();

		}finally{
			if(out != null){
				out.flush();
				out.close();
				out = null;
			}
		}
	}
	
	/**将obj转换成字符串
	 * @param obj obj为基本数据类型
	 * @return
	 */
	public static String  getStringFromObject(Object obj){
		if (obj == null) {
			return "";
		} else if(StringUtils.isBlank(obj.toString())){
			return "";
		}else{
			return obj.toString();
		}		
	}
	
	/**去的application应用
	 * @param request
	 * @return
	 */
	public static ApplicationContext  getApplicationContext(HttpServletRequest request){
		ApplicationContext application = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
		return application;
	}
}
