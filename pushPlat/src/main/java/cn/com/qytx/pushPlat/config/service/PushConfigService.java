package cn.com.qytx.pushPlat.config.service;

import java.util.List;
import java.util.Map;

/**
 * 功能: 推送配置 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年5月11日
 * 修改日期: 2015年5月11日
 * 修改列表:
 */
public interface PushConfigService {

	/**
	 * 功能： 获得单位推送配置信息
	 * @param appId
	 * @param companyId
	 * @return
	 */
	public List<Map<String, Object>> getPushConfig(String appId,int companyId);
	
	/**
	 * 功能：获得apns推送配置信息
	 * @param appId
	 * @return
	 */
	public Map<String, Object> getApnsInfo(String appId);
}
