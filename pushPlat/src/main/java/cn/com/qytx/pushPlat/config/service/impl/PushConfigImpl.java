package cn.com.qytx.pushPlat.config.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.pushPlat.config.dao.PushConfigDao;
import cn.com.qytx.pushPlat.config.service.PushConfigService;

/**
 * 功能: 推送配置 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年5月11日
 * 修改日期: 2015年5月11日
 * 修改列表:
 */
@Service("pushConfigService")
@Transactional
public class PushConfigImpl implements PushConfigService {
	
	@Autowired
	private PushConfigDao pushConfigDao;
	
	/**
	 * 功能： 获得单位推送配置信息
	 * @param appId
	 * @param companyId
	 * @return
	 */
	public List<Map<String, Object>> getPushConfig(String appId,int companyId){
		return pushConfigDao.getPushConfig(appId, companyId);
	}
	
	/**
	 * 功能：获得apns推送配置信息
	 * @param appId
	 * @return
	 */
	public Map<String, Object> getApnsInfo(String appId){
		return pushConfigDao.getApnsInfo(appId);
	}
}
