package cn.com.qytx.pushPlat.push.apns;

import java.io.InputStream;
import java.util.List;

import com.dbay.apns4j.IApnsService;
import com.dbay.apns4j.demo.Apns4jDemo;
import com.dbay.apns4j.impl.ApnsServiceImpl;
import com.dbay.apns4j.model.ApnsConfig;
import com.dbay.apns4j.model.Feedback;
import com.dbay.apns4j.model.Payload;

public class test {

	private static IApnsService apnsService;
	 private static IApnsService getApnsService() {
		    if (apnsService == null) {
		      ApnsConfig config = new ApnsConfig();
		      InputStream is = Apns4jDemo.class.getClassLoader().getResourceAsStream("aps_developer.p12");
		      config.setKeyStore(is);
		      config.setDevEnv(true);
		      //true 测试环境 false 正式环境
		      config.setPassword("900822");
		      config.setPoolSize(5);
		      apnsService = ApnsServiceImpl.createInstance(config);
		    }
		    return apnsService;
		  }
	 public static void main(String[] args) {
		    IApnsService service = getApnsService();
		    
		    // send notification
		    String token = "060914173e0630fc0710ed836a60b64094ee27015c279bc9be4ee8ab49381010";
		    
		    Payload payload = new Payload();
		    payload.setAlert("How are you?");
		    // If this property is absent, the badge is not changed. To remove the badge, set the value of this property to 0
		    payload.setBadge(1);
		    // set sound null, the music won't be played
//				payload.setSound(null);
		    payload.setSound("msg.mp3");
		    payload.addParam("uid", 123456);
		    payload.addParam("type", 12);
		    service.sendNotification(token, payload);
		    
		    // payload, use loc string
		    Payload payload2 = new Payload();
		    payload2.setBadge(1);
		    payload2.setAlertLocKey("GAME_PLAY_REQUEST_FORMAT");
		    payload2.setAlertLocArgs(new String[]{"Jenna", "Frank"});
		    service.sendNotification(token, payload2);
		    
		    // get feedback
//		    List<Feedback> list = service.getFeedbacks();
//		    if (list != null && list.size() > 0) {
//		      for (Feedback feedback : list) {
//		        System.out.println(feedback.getDate() + " " + feedback.getToken());
//		      }
//		    }
		    
		    try {
		      // sleep 5s.
		      Thread.sleep(5000);
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		    
		    // It's a good habit to shutdown what you never use
		    service.shutdown();
		    
//				System.exit(0);
		  }


}
