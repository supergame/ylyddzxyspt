package cn.com.qytx.pushPlat.config.http;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import cn.com.qytx.pushPlat.config.service.IConnection;
import cn.com.qytx.pushPlat.config.service.PushConfigService;
import cn.com.qytx.pushPlat.utils.RequestToMapUtil;
import cn.com.qytx.pushPlat.utils.SpringUtil;
import cn.com.qytx.pushPlat.utils.SysUtil;

import com.google.gson.Gson;

/**
 * 功能: 根据应用id及公司id获得推送平台信息 
 * 版本: 1.0
 * 开发人员: zyf
 * 创建日期: 2015年3月12日
 * 修改日期: 2015年3月12日
 * 修改列表:
 */
public class PushServerInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PushConfigService pushConfigService = (PushConfigService) SpringUtil.getBean("pushConfigService");
	
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			//获取用户传递过来的请求
			String appId = SysUtil.getStringFromObject(request.getParameter("appId"));//应用id
			String companyId = SysUtil.getStringFromObject(request.getParameter("companyId"));//公司id
			if(companyId.equals("")){
				companyId = "0";
			}
			if(StringUtils.isEmpty(appId)||StringUtils.isEmpty(companyId)){
				SysUtil.toClient("101||参数不正确", response);
				return ;
			}
			/**
			 * 获取单位推送配置信息
			 */
			List<Map<String,Object>> dbInfoList = pushConfigService.getPushConfig(appId, Integer.valueOf(companyId));
			if(dbInfoList!=null){
				Map<String,Object> dbInfo = dbInfoList.get(0);
				Map<String,Object> result = new HashMap<String, Object>();
				result.put("pushType", dbInfo.get("pushType"));
				result.put("appKey", dbInfo.get("appKey")==null?"":dbInfo.get("appKey"));
				result.put("secret", dbInfo.get("secret")==null?"":dbInfo.get("secret"));
				result.put("connectStr", "");
				if(SysUtil.getStringFromObject(dbInfo.get("pushType")).equals(IConnection.MQTT_PUSH_TYPE)){
					String connectStr = "tcp://";
					connectStr += SysUtil.getStringFromObject(dbInfo.get("mqttIpPort"));
					result.put("connectStr", connectStr);
				}
				Gson json = new Gson();
				SysUtil.toClient("100||"+json.toJson(result), response);
				return ;
			}else{
				SysUtil.toClient("102||请初始化应用信息", response);
				return ;
			}
		} catch (Exception e) {
			e.printStackTrace();
			SysUtil.toClient("102||服务器异常", response);	
		}
	}

}
