<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head >
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>医联体工单</title>
<link href="${ctx}css/author_main.css" rel="stylesheet" type="text/css" />
<jsp:include page="../../../common/flatHead.jsp" />
<script type="text/javascript">
	var basePath = "${ctx}";
	$(function(){
		$(".new_right_content").css({"height":$(window).height()});
	});
</script>

<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/Reminder.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}angularframe/css/ui-basepage.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />

<script type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js?version=${version}"></script>
<script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${ctx}js/logined/carDispatch/conjoined/conjoined.js"></script>

<style>
	.Wdate {
	    border: #ccc 1px solid;
	    background-position: center left 110px;
	}
</style>
</head>
<body>
<div class="new_right_content">
	<div class="index_detail pdb8">
		<p class="new_index_title ">
			医联体工单
		</p>
		<div class="list">
			<div class="overf mb20">
				<ul class="select_list fl">
					<li>
					<span class="select_list_title">派遣类型：</span>
						<select name="type" class="select_style" id="businessType">
							<option value="">全部</option>
							<option value="1">上转住院</option>
							<option value="2">上转检查</option>
							<option value="3">上转门诊</option>
							<option value="4">下转住院</option>
							<option value="5">急危重抢救</option>
							<option value="6">标本收取</option>
						</select>
					</li>
					<li style="width:160px;">
						<span class="select_list_title">当前状态：</span>
						<select name="state" class="select_style" id="status" style="width:85px;">
							<option value="">全部</option>
							<option value="1">待派遣</option>
							<option value="2">待发车</option>
							<option value="3">已发车</option>
							<option value="4">已回厂</option>
						</select>
					</li>
					<li class="key_word" >
						<span class="select_list_title">转诊单位：</span>
						<input type="text"  class="select_style" placeholder="请输入转诊单位" id="company"/>
					</li>
					<li  style="width: 365px;">
						<span class="select_list_title">创建时间：</span>
						<input id="startTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'endTime\')}'})" class="in_area Wdate" style="width:135px;margin-left:0px"/> -
						<input id="endTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'startTime\')}'})" class="in_area Wdate" style="width:135px"/>
					</li>
					
				</ul>
				<div class="fl pd4">					
					<span class="btn_inline01 btn_search_new" onclick="searchList();">查找</span>
					<span class="btn_inline01 btn_output_new" onclick="export_list();" style="cursor: pointer">导出</span>
				</div>
			</div>
			<table cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" id="myTable">
				<thead>
					<tr>
			        	<th width="30px">序号</th>
			        	<th width="90px">工单编号</th>
			        	<th width="90px">派遣类型</th>
			        	<th width="60px">患者姓名</th>
			       		<th width="110px">转诊单位</th>
			       		<th width="150px">登记时间</th>
			        	<th width="100px">当前状态</th>        	
			        	<th width="100px">操作</th>
			     	 </tr>
				</thead>
		</table>
		</div>
	</div>
</div>
</body>
</html>