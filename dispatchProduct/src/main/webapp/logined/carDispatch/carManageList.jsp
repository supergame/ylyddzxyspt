<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.Date"%>
<jsp:include page="../../common/taglibs.jsp" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>车辆管理</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx }/logined/carDispatch/css/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/Reminder.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}angularframe/css/ui-basepage.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
<script TYPE="text/javascript" src="${ctx}js/logined/carDispatch/carManageList.js"></script>
</head>
<body>
<input type="hidden" value="0" id="id"/>
	<div class="index_detail pb20">
		<p class="new_index_title ">
			车辆派遣-车辆维护
		</p>
	<div class="overf mb20">
				<ul class="select_list fl">
					<li style="margin-left: 20px;width:180px;">
						<span class="select_list_title">车辆类型：</span>
							<select name="type" class="select_style" id="carType">
								<option value="">全部</option>
								<option value="1">转诊车</option>
								<option value="2">急诊车</option>
							</select>
						</li>
					<li style="width:210px;">
						<span class="select_list_title">车牌号：</span>
						<input id="carNo" class="in_area " style="width:130px;" placeholder="请输入车牌号"/> 
					</li>
					<!-- <li style="width:210px;">
						<span class="select_list_title">车辆品牌：</span>
						<input id="carBrand" class="in_area " style="width:130px;" placeholder="请输入车辆品牌"/> 
					</li> -->
					<!-- <li style="width:210px;">
						<span class="select_list_title">车牌型号：</span>
						<input id="carModel" class="in_area  " style="width:130px;"placeholder="请输入车牌型号"/> 
					</li> -->
					<!-- <li style="width:180px;">
						<span class="select_list_title">当前状态：</span>
							<select name="type" class="select_style" id="status">
								<option value="">全部</option>
								<option value="1">空闲</option>
								<option value="2">待发车</option>
								<option value="3">待回场</option>
						</select>
					</li> -->
				</ul>
				<div class="fl pd4">					
					<span class="btn_inline01 btn_search_new" id="seach_btn">查找</span>
					<span class="btn_inline01 btn_add_new" id="add_btn" style="cursor:pointer">新增</span>
				</div>
			</div>
		<div class="mainPage" style="padding: 0 20px;position: relative;margin-top: 15px;">
			<div class="prettyList" style="width:100%;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					id="carTable" class="pretty dataTable">
				</table>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</body>
</html>
