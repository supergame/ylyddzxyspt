<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.Date"%>
<jsp:include page="../../common/taglibs.jsp" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="background: #fff;">
<head>
<title>车辆管理</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx }/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/Reminder.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}angularframe/css/ui-basepage.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
<script TYPE="text/javascript" src="${ctx}js/logined/carDispatch/carManageAdd.js"></script>
<script type="text/javascript" src="${ctx}js/common/validate_form.js"></script>
<style>	
.buttonArea {
	text-align: right;
	width: 100%;
	position: absolute; 
	bottom: 0;
	left: 0;
	border-top: 1px solid #e5e5e5;
	padding: 10px 0 8px 0;
}
.NewBtn {
	color: #333;
	font-size: 14px;
    border-radius: 3px;
    cursor: pointer;
    display: inline-block;
    height: 30px;
    line-height: 29px;
    margin-left: 10px;
    text-align: center;
    width: 80px;
    text-decoration: none;
}
.NewBtn.blue {
	color: #fff;
	background: #49a4b4;
}
.NewBtn.gray {
	background: #dce7e9;
	margin-right: 10px;
}
.formText {
	padding: 0 10px;
	box-sizing: border-box;
	border: 1px solid #d9d9d9;
}
.formText:focus {
	border: 1px solid #459fd2;
}
</style>
</head>
<body>
	<input type="hidden" name="id" id="id" value="0" />
	<div class="input" style="background: #fff; margin: 10px 0 0 0;">
		<form action="#" id="carForm" style="margin:0 auto;">
			<table width="100%" border="0" class="inputTable1">
				<tr style="height: 40px;">
					<th style="text-align: right;margin-top: 5px;min-width: 100px;"><span class="requireField" style="color: red;height: 24px;">*</span>&nbsp;车牌号：</th>
					<td>
						<input type="text" class="formText" size="50"
						id="carNo" maxlength="20" style="height: 28px;width: 92%;"/>
					</td>
				</tr>
				<!-- <tr style="height: 40px;">
					<th style="text-align: right;margin-top: 5px;min-width: 100px;"><span class="requireField"style="color: red;height: 24px;">*</span><label>&nbsp;车辆品牌：</label>
					</th>
					<td><input type="text" class="formText" size="50"
						id="carBrand" maxlength="20" style="height: 28px;width: 92%;"/>
					</td>
				</tr> -->
				<tr style="height: 40px;">
					<th style="text-align: right;margin-top: 5px;min-width: 100px;"><label>&nbsp;车辆手机号：</label>
					</th>
					<td><input type="text" class="formText" size="50"
						id="carPhone" maxlength="20" style="height: 28px;width: 92%;"/>
					</td>
				</tr>
				<tr style="height: 40px;">
					<th style="text-align: right;margin-top: 5px;min-width: 100px;"><label>&nbsp;车辆类型：</label>
					</th>
					<td>
						<!-- <select id="carType" style="height: 28px;width: 100px;border: 1px solid #d9d9d9;">
							<option value="1">转诊车</option>
							<option value="2">急诊车</option>
						</select> -->
						<input type="radio" name="carType" value="1" checked="checked">&nbsp;转诊车
 						<input type="radio" name="carType" value="2" style="margin-left: 10px;">&nbsp;急诊车
					</td>
				</tr>
				<tr style="height: 40px;">
					<th style="text-align: right;margin-top: 5px;min-width: 100px;"><label>&nbsp;是否有担架：</label>
					</th>
					<td>
						<input type="radio" name="havaStretcher" value="1" checked="checked">&nbsp;有担架
 						<input type="radio" name="havaStretcher" value="2" style="margin-left: 10px;">&nbsp;没有担架
					</td>
				</tr>
				<tr style="height: 40px;">
					<th style="text-align: right;margin-top: 5px;min-width: 100px;"><label>&nbsp;座位数：</label>
					</th>
					<td><input type="text" class="formText" size="50"
						id="seatNum" maxlength="20" style="height: 28px;width: 92%;"/>
					</td>
				</tr>
			</table>
		</form>
		<div class="buttonArea">
			<a href="javascript:;" class="NewBtn blue" id="saveCar">确认</a>
			<a href="javascript:;" class="NewBtn gray" id="cancelCar">取消</a>
		</div>
	</div>
</body>
</html>