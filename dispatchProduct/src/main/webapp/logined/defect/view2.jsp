<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="view2App">
	<head>
		<meta charset="UTF-8">
		<title>上报缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="css/step.css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link rel="stylesheet" href="css/screen.css" />
		
		<script type="text/javascript">
			var basePath = "${ctx}";
			var baseUrl = "${baseUrl}";
			
			function printPage(){
				var instanceId = $("#instanceId").val();
				var url = basePath+"logined/defect/printWork.jsp?instanceId="+instanceId+"&workType="+type;
				art.dialog.open(url,{
					id : "printWork",
				    title : "打印工单",
				    width : 800,
				    height : 520,
				    lock : true,
				    opacity: 0.1,// 透明度
				    button: [{
				    	name: '确定',
				    	focus: true,
				    	callback:function(){
				    		var iframe = this.iframe.contentWindow;
				    		iframe.printPage();
				    		return false;
				    	}
				    	},
				        {name: '关闭'}]
				});
			}
			
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>
			    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-theme-default.css">
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-theme-plain.css">
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-custom-width.css">
		<script src="${ctx}plugins/ngDialog/js/ngDialog.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/view2App.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/view2Controller.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/insertEquipment.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectPeople.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
		<style>
			.ngdialog.ngdialog-theme-default .ngdialog-content{padding:0px;padding-bottom:1em;}
			.describle_title{border-bottom:1px solid #ff9933;}
			.in_area{width:100%;}
			.ngdialog-buttons{padding:0 10px;}
			.des_photos_box{position:relative;overflow:initial;}
			.des_photos_box img:hover{transform:scale(10,10);position:absolute;top:0px;left:150px;background:#fff;z-index:999;box-shadow: 0 0 5px #333;}
			.paizhao{overflow:initial !important;min-height:76px;}
			.ng-cloak{display:none;}
		</style>
	</head>
	<body ng-controller="view2Controller">
		<input type="hidden" id="oper" value="<%=request.getParameter("oper") %>">
		<input type="hidden" id="newType" value="<%=request.getParameter("type") %>">
		<input type="hidden" id="instanceId" value="<%=request.getParameter("instanceId") %>">
		<input type="hidden" id="isOut" value="<%=request.getParameter("isOut") %>">
		<input type="hidden" id="iDisplayStart" value="<%=request.getParameter("iDisplayStart") %>">
		<input type="hidden" id="workType" >
		
		<div class="new_right_content">
		<div class="kh_score_box index_detail">
			<div class="pr">
				<div class="kh_score_title">
					{{apply.type==1?'配送工单详情':'归还工单详情'}}（编号：<span>{{apply.defectNumber}}</span>）
					<div ng-if="oper=='apply'&&(apply.state==-1||apply.state==0||apply.state==6||apply.state==1||apply.state==2)" class="btn_container01 AbsoluteBtnContainer mb20"> 
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="pushReminder()" ng-if="apply.state!=3&&apply.state!=5&&apply.state!=9">催单</span>
						<span class="btn_inline_new hm_btn_white " ng-click="openDialog(2);" ng-if="apply.state==2">验收通过</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(3);" ng-if="apply.state==2">验收驳回</span>
						<span class="btn_inline_new hm_btn_yellow_new" ng-click="editDefect()" ng-if="apply.state==0||apply.state==-1">修改</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="draftDefect();"> &nbsp;作废&nbsp; </span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='apply'&&apply.state==5" class="btn_container01 AbsoluteBtnContainer mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="agree();">同意</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="rejuest();"> &nbsp;拒绝&nbsp; </span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="selectPeople()">添加参与人</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div> 
					<div ng-if="oper=='dispatch'&&apply.state==-1" class="btn_container01 AbsoluteBtnContainer mb20">    
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="pushReminder()" >催单</span>
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(9)">调度</span>
						<!-- <span class="btn_inline_new hm_btn_red_new ml15" ng-click="applyDefect();" ng-if="apply.isDraft==0"> &nbsp;请求作废&nbsp; </span> -->
						<span class="btn_inline_new hm_btn_yellow_new " onclick="selectPeople()">添加参与人</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='approve'&&(apply.state==0||apply.state==6)" class="btn_container01 AbsoluteBtnContainer mb20">    
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="pushReminder()" >催单</span>
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(6)">接收</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="openDialog(0);" ng-if="apply.turnNum<turnNum"> &nbsp;转交&nbsp; </span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="selectPeople()">添加参与人</span>
						<!-- <span class="btn_inline_new hm_btn_red_new ml15" ng-click="applyDefect();" ng-if="apply.isDraft==0"> &nbsp;请求作废&nbsp; </span> -->
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='defect'&&(apply.state==1 || apply.state==7)" class="btn_container01 AbsoluteBtnContainer mb20">
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="draftDefect(1);"> &nbsp;作废&nbsp; </span>
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(1);">处理完成</span>
						<span class="btn_inline_new hm_btn_red_new " ng-click="openDialog(10);">转交</span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="insertEquipment();" ng-show="apply.type==1">添加设备</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-if="apply.isDelay==0||apply.isDelay==3" ng-click="openDialog(7);">申请延期</span>
						<span class="btn_inline_new hm_btn_yellow_new " onclick="selectPeople()">添加参与人</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='accept'&&apply.state==2" class="btn_container01 AbsoluteBtnContainer mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(2);">验收通过</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(3);">驳回</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='archive'" class="btn_container01 AbsoluteBtnContainer mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(4);">归档</span>
						<!-- <span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(5);">驳回验收</span> -->
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='delay'" class="btn_container01 AbsoluteBtnContainer mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-if="apply.isDelay==0||apply.isDelay==3" ng-click="openDialog(7);">申请延期</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='approveDelay'" class="btn_container01 AbsoluteBtnContainer mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="agreeDelay()" ng-if="apply.isDelay==1">同意延期</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="rejuestDelay()" ng-if="apply.isDelay==1">拒绝延期</span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='myJoin'||oper=='view'||(oper=='apply'&&apply.state!=-1&&apply.state!=0&&apply.state!=5&&apply.state!=6&&apply.state!=2&&apply.state!=1)||oper=='delayView'" class="btn_container01 AbsoluteBtnContainer mb20">
						<!-- 特殊处理 已接收 可以转交 -->
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(10);" ng-if="apply.state==1 || apply.state==7">转交处理</span>
						<span class="btn_inline_new hm_btn_yellow_new" ng-click="openDialog(20);" ng-if="apply.state==3&&newType=='accept'"> &nbsp;评价&nbsp; </span>
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
				
				
					<div ng-if="oper=='dispatch'&&apply.state!=-1" class="btn_container01 AbsoluteBtnContainer mb20">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					
					<div ng-if="oper=='approve'&&apply.state!=0&&apply.state!=6" class="btn_container01 AbsoluteBtnContainer mb20">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					
					<div ng-if="oper=='defect'&&apply.state!=1&&apply.state!=7" class="btn_container01 AbsoluteBtnContainer mb20">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='accept'&&apply.state!=2" class="btn_container01 AbsoluteBtnContainer mb20">    
						<span class="btn_inline_new hm_btn_white" onClick="printPage(1);"> &nbsp;打印&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
				</div>
			</div>
			<!--startprint-->
			<div class="kh_score_detail">
				<ul class="kh_detail_list">
					<li class="width66">
						<span class="kh_detail_list_title">设备名称：</span>
						<span class="kh_detail_list_content">{{apply.equipmentName}}</span>
					</li>
					<li class="width33">
						<span class="kh_detail_list_title">设备数量：</span>
						<span class="kh_detail_list_content">{{apply.equipmentNumber}}</span>
					</li>
					
					<li>
						<span class="kh_detail_list_title">事件状态：</span>
						<span class="kh_detail_list_content"><span class="{{apply.stateVo|stateColor}}">{{apply.stateVo}}{{apply.delayStateVo}}</span></span>
					</li>
					<li>
						<span class="kh_detail_list_title">紧急程度：</span>
						<span class="kh_detail_list_content {{apply.grade==1?'color_red':''}}">{{apply.gradeVo}}</span>
					</li>
					<li>
						<span class="kh_detail_list_title">事件类型：</span>
						<span class="kh_detail_list_content">{{apply.type==1?'配送':'归还'}}</span>
					</li>
					
					
					<li>
						<span class="kh_detail_list_title">上报电话：</span>
						<span class="kh_detail_list_content">{{apply.applyPhone?apply.applyPhone:'--'}}{{apply.applyPhoneName?'('+apply.applyPhoneName+')':''}}<span class="tel" ng-show="apply.applyPhone&&isOut==1" ng-click="outCall(apply.applyPhone,'','')"></span></span>
					</li>
					<li>
						<span class="kh_detail_list_title">上报科室：</span>
						<span class="kh_detail_list_content">{{apply.defectDepartmentVo?apply.defectDepartmentVo:'--'}}</span>
					</li>
					<li>
						<span class="kh_detail_list_title">上报地点：</span>
						<span class="kh_detail_list_content">{{apply.describe}}</span>
					</li>
					
					
					<li>
						<span class="kh_detail_list_title">上报人：</span>
						<span class="kh_detail_list_content">{{apply.createUserName?apply.createUserName:'--'}}({{apply.createUserPhone?apply.createUserPhone:'--'}})<span class="tel" ng-show="apply.createUserPhone&&isOut==1" ng-click="outCall(apply.createUserPhone,'','')"></span></span>
					</li>
					<li>
						<span class="kh_detail_list_title">工号：</span>
						<span class="kh_detail_list_content">{{apply.createUserWorkNo?apply.createUserWorkNo:'--'}}</span>
					</li>
					<li>
						<span class="kh_detail_list_title">上报时间：</span>
						<span class="kh_detail_list_content">{{apply.createTimeStr?apply.createTimeStr:'--'}}</span>
					</li>
					
					<li>
						<span class="kh_detail_list_title">处理科室：</span>
						<span class="kh_detail_list_content">{{processGroupName?processGroupName:"--"}}</span>
					</li>
					<li>
						<span class="kh_detail_list_title">处理人：</span>
						<span class="kh_detail_list_content">{{processUserName?processUserName:"--"}}</span>
					</li>
					
					<li>
						<span class="kh_detail_list_title">完工时间：</span>
						<span class="kh_detail_list_content">{{processTime?processTime:"--"}}</span>
					</li>
					
					<li ng-show="dispatchUserName!=''">
						<span class="kh_detail_list_title">调度人：</span>
						<span class="kh_detail_list_content">{{dispatchUserName}}</span>
					</li>
					<li ng-show="dispatchUserName!=''">
						<span class="kh_detail_list_title">调度人工号：</span>
						<span class="kh_detail_list_content">{{dispatchWorkNo}}</span>
					</li>
					
					<li ng-show="dispatchUserName!=''">
						<span class="kh_detail_list_title">调度时间：</span>
						<span class="kh_detail_list_content">{{apply.createTimeStr?apply.createTimeStr:'--'}}</span>
					</li>
				</ul>
				<!-- <div class="kh_detail_list_title tab_title" >所选设备：</div>
				<div class="score_table_box">
					<table class="score_table">
						<thead>
							<tr>
								<th width="25%">设备名称</th>
								<th width="25%">设备编号</th>
								<th width="25%">设备型号</th>
								<th width="25%">操作</th>
							</tr>
						</thead>
						<tbody id="equipmentTable">
						</tbody>
					</table>
				</div> -->
				
<!-- 				<div class="kh_detail_list_title tab_title">参与人：</div>
				<div class="score_table_box">
					<table class="score_table">
						<thead>
							<tr>
								 <th width="25%">姓名</th>
								<th width="20%">工号</th>
								<th width="20%">科室</th>
								<th width="25%">联系方式</th>
								<th width="10%">操作</th>
							</tr>
						</thead>
						<tbody id="peopleTable">
						</tbody>
					</table>
				</div>
 -->				
				<input type="hidden" value="{{processUserId}}" id="processUserId"/>
				
				<!--end .score_table_box-->
				<ul class="kh_detail_list no-border">
					<li style="width: 66.66%">
						<span class="text-gray kh_detail_list_title">备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</span>
						<span>{{apply.memo?apply.memo:'--'}}</span>
					</li>
					<li style="width: 33.33%">
						<span class="text-gray kh_detail_list_title">催单次数：</span>
						<span>{{apply.reminderNum?apply.reminderNum:'0'}}</span>
					</li>
					<li class="width100">
						<span class="text-gray kh_detail_list_title">录&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;音：</span>
						<span ng-show="apply.audioIds"><audio controls="controls" id="myVideo" preload="preload" src="{{audioSrc}}">    
							</audio></span>
						<span ng-show="!apply.audioIds">
		                	--
						</span>	
					</li>
					<li class="width100 pr  {{checkAttachmentIds.length==0?'':'paizhao'}}" style="padding-left: 105px;">
			            <span class="kh_detail_list_title" style="position: absolute;left: 15px;">处理图片：</span>
			            <div class="des_photos_box" ng-repeat="attachmentId in checkAttachmentIds">
				                <img src="${baseUrl}fileServer/download?fileId={{attachmentId}}" />
				            </div>
			            <span  ng-show="checkAttachmentIds.length==0">--</span>
		       		</li>
					<li style="width: 33.33%" ng-if="apply.state==3">
						<span class="text-gray kh_detail_list_title">评价意见：</span>
						<span>{{apply.achievementStr?apply.achievementStr:'暂无意见'}}</span>
					</li>
					<li style="width: 66.66%" ng-if="apply.state==3">
						<span class="text-gray kh_detail_list_title">评价内容：</span>
						<span>{{apply.evaluate?apply.evaluate:'暂无内容'}}</span>
					</li>
					
					
				</ul>
				<!--end .text-area-->
				<!-- <div class="step_box">
				    <div class="title">处理流程：</div>
					<ul class="defect_step">
						<li ng-repeat="history in historyList">
							<span>{{history.endTime}}</span>
							<span>{{history.processerName}}</span>({{history.processerPhone}}<span class="tel" ng-show="history.processerPhone!=''&&isOut==1" ng-click="outCall(history.processerPhone,'','')"></span>){{history.taskName|getHistoryDetail:history.approveResult:history.nextProcesserName:history.nextProcesserPhone:history.advice}}<span class="tel" ng-show="history.nextProcesserPhone!=''&&isOut==1" ng-click="outCall(history.nextProcesserPhone,'','')"></span>
						</li>
					</ul>
				</div> -->
				<!--.step_box-->
				<div class="TabReviseContainer special">
				    <div class="tab-box">
				        <ul>
				        	<li><span class="tab current">处理流程</span></li>
				        	<li><span class="tab">参与人</span></li>
				        	<li><span class="tab">所选设备</span></li>
				        </ul>
				    </div>
				    <!-- end .tab-box -->
				    <div class="tab-revise-content">
				        <!-- <ul class="defect_step">
							<li ng-repeat="history in historyList">
								<span>{{history.endTime}}</span>
								<span>{{history.processerName}}</span>({{history.processerPhone}}<span class="tel" ng-show="history.processerPhone!=''&&isOut==1" ng-click="outCall(history.processerPhone,'','')"></span>){{history.taskName|getHistoryDetail:history.approveResult:history.nextProcesserName:history.nextProcesserPhone:history.advice}}<span class="tel" ng-show="history.nextProcesserPhone!=''&&isOut==1" ng-click="outCall(history.nextProcesserPhone,'','')"></span>
							</li>
						</ul> -->
						<table class="score_table">
							<thead>
								<tr>
									<th width="25%">设备名称</th>
									<th width="25%">设备编号</th>
									<th width="25%">设备型号</th>
									<th width="25%">操作</th>
								</tr>
							</thead>
							<tbody id="equipmentTable1">
							</tbody>
						</table>
						<ul class="event-status-list">
							<li class="" ng-repeat="history in historyList">
								<div class="info-box">
									<p class="title">
										<span class="time">{{history.endTime}}</span>
										<span class="style">{{history.taskName|getHistoryDetail2:history.approveResult}}</span>
									 	{{history.processerDealName}} 
									</p>
								</div>
								<p class="prompt" ng-if="history.advice">备注：{{history.advice}}</p>
								<table class="score_table" ng-show="history.taskName==3&&apply.type==1" style="width: 336px;margin-left: 237px;">
									<thead>
										<tr>
											<th width="25%" style="padding:2px 0px">设备名称</th>
											<th width="25%">设备编号</th>
										</tr>
									</thead>
									<tbody>
									<tr ng-repeat="equipment in equipmentList">
										<td style="padding:2px 0px">{{equipment.name}}</td>
										<td>{{equipment.capitalNo}}</td>
									</tr>
									</tbody>
								</table>
								<!--end .info-box-->
							</li>
						</ul>
				    </div>
				    <!-- end .tab-revise-content -->
				    <div class="tab-revise-content hide" >
				         <table class="score_table">
							<thead>
								<tr>
									 <th width="25%">姓名</th>
									<th width="20%">工号</th>
									<th width="20%">科室</th>
									<th width="25%">联系方式</th>
									<th width="10%">操作</th>
								</tr>
							</thead>
							<tbody id="peopleTable">
							</tbody>
						</table>
				    </div>
				    <!-- end .tab-revise-content -->
				    <div class="tab-revise-content hide">
				          <table class="score_table">
							<thead>
								<tr>
									<th width="25%">设备名称</th>
									<th width="25%">设备编号</th>
									<th width="25%">设备型号</th>
									<th width="25%">操作</th>
								</tr>
							</thead>
							<tbody id="equipmentTable">
							</tbody>
						</table>
				    </div>
				    <!-- end .tab-revise-content -->
				    
				</div>
				<!-- end .TabReviseContainer -->
				
				
			</div>
			<!--end .kh_score_detail-->
		</div>
		<!--end .kh_score_box-->
	</div>
    	
    	<script>
	    	$(".tab-box ul li .tab").click(function(){
	    	    if($(this).hasClass("current"))
	    	    return;
	    			 
	    	    var index = $(".tab-box ul li .tab").index($(this));
	    	    $(".tab-box ul li .tab").removeClass("current");
	    	    $(".tab-box ul li .tab").eq(index).addClass("current");
	    			  
	    	    $(".tab-revise-content").addClass("hide");
	    	    $(".tab-revise-content").eq(index).removeClass("hide");
	    	})
    	</script>
	</body>
</html>
