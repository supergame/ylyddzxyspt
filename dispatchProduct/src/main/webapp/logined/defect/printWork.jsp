<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html >
	<head>
		<meta charset="UTF-8">
		<title>上报缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="css/style.css" rel="stylesheet" />
		<script type="text/javascript">
			function printPage(){
				var bdhtml=window.document.body.innerHTML;//获取当前页的html代码 
				sprnstr="<!--startprint-->";//设置打印开始区域 
				eprnstr="<!--endprint-->";//设置打印结束区域 
				prnhtml=bdhtml.substring(bdhtml.indexOf(sprnstr)+18); //从开始代码向后取html 
				prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));//从结束代码向前取html 
				window.document.body.innerHTML=prnhtml;
				window.print(); 
			}
			
		</script>
		<script type="text/javascript" src="${ctx}js/logined/defect/controller/printWork.js"></script>
	</head>
	<body style="margin:10px">
	<input type="hidden" id="instanceId" value="${param.instanceId }">
	<input type="hidden" id="workType" value="${param.workType }">
	<!--startprint-->
		<div class="table-box">
			<p class="table-title">郑州市中心医院后勤保障</p>
			<p>
				<span >工单编号：<span id="workNo">1235466</span></span>
				<span class="fr">处理人部门：<span id="processGroupName">综合维修班</span>&nbsp;&nbsp;处理人：<span id="processUserName">张xx</span></span>
			</p>
			<table cellpadding="0" cellspacing="0" width="100%">
				<colgroup>
					<col width="12%" />
					<col width="22%" />
					<col width="12%" />
					<col width="18%" />
					<col width="12%" />
					<col/>
				</colgroup>
				<tbody>
					<tr id="repairTr">
						<td>维修事项</td>
						<td colspan="5" id="workContent">女公共厕所堵</td>
					</tr>
					<tr id="otherTr" style="display: none">
						<td>设备名称</td>
						<td colspan="3" id="workContent">女公共厕所堵</td>
						<td>数量</td>
						<td id="equipmentNum">2</td>
					</tr> 
					<tr>
						<td>事件状态</td>
						<td id="workState">待接收</td>
						<td>紧急程度</td>
						<td id="workGrade">一般</td>
						<td>事件类型</td>
						<td id="workType">事件维修</td>
					</tr>
					<tr>
						<td>上报电话</td>
						<td id="applyPhone">8201(灭菌室)</td>
						<td>上报科室</td>
						<td id="applyGroupName">供应室灭菌间</td>
						<td>维修地点</td>
						<td id="workLocation">5号楼2楼</td>
					</tr>
					<tr>
						<td>上报人</td>
						<td id="applyUserName">刘福英(15136182731)</td>
						<td>工号</td>
						<td id="applyWorkNo">50500</td>
						<td>上报时间</td>
						<td id="applyTime">2017年06月27日 08:25</td>
					</tr>
					<tr>
						<td>备注</td>
						<td colspan="5" id="applyMemo"></td>
					</tr>
					<tr id="goodsTable">
						<td>物资名称</td>
						<td>物资组</td>
						<td>数量</td>
						<td>单价</td>
						<td>总价</td>
						<td>说明</td>
					</tr>
					
					<tr id="equipmentTable" style="display: none">
						<td >设备名称</td>
						<td>设备组</td>
						<td >设备编号</td>
						<td>使用时间</td>
						<td>停用时间</td>
						<td>使用时长</td>
					</tr>
					
					<tr>
						<td>维修结果</td>
						<td colspan="3">
							<span class="radio-box"><i class="radio"></i>非常满意</span>
							<span class="radio-box"><i class="radio"></i>满意</span>
							<span class="radio-box"><i class="radio"></i>一般</span>
							<span class="radio-box"><i class="radio"></i>不满意</span>
						</td>
						<td>科室确认</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--endprint-->
		<!--end .table-box-->
		<!-- <input type="button" onClick="printPage()" value="打印"/> -->
		<script>
			//radio
			$(".table-box .radio").on("click",function(){
				$(".table-box .radio").removeClass("checked");
				$(this).toggleClass("checked");
			})
		</script>
	</body>
</html>
