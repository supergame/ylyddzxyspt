<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html >
	<head>
		<meta charset="UTF-8">
		<title>值班签到缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<script type="text/javascript">
			var basePath = "${ctx}";
		</script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectGoodNum.js"></script>
	</head>
	<body  style="background-color:#fff;">
		<div class="elasticFrame formPage">
			<form name="myForm" novalidate="novalidate">
			  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="inputTable">
			    <tbody>
			      <tr >
			        <th><label>数量：</label></th>
			        <td>
			        	<input class="formText" type="text" name="" value="" placeholder="" id="goodNum" style="width:250px">
			        </td>
			      </tr>
			    </tbody>
			  </table>
		  </form>
		</div>
	</body>
</html>
