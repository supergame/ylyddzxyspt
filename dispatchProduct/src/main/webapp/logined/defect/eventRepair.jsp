<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html ng-app="eventAddApp">
  <head>
    <base href="<%=basePath%>">
    <%
		String type = (String)request.getParameter("type");
   		String eventLoginName = "";//(String)request.getParameter("eventLoginName");
   		String eventUserName = "--";//(String)request.getParameter("eventUserName");
   		String eventGroupName = "--";//(String)request.getParameter("eventGroupName");
   		String applyPhone = (String)request.getParameter("applyPhone");
   		String from = (String)request.getParameter("from");
   		if(eventGroupName==null||eventGroupName==""){
   			eventGroupName = "--";
   		}
   		if(applyPhone==null||applyPhone==""){
   			applyPhone = "--";
   		}
   		String eventUserId = (String)request.getParameter("eventUserId");
	%>
    <title>维修上报</title>
    <jsp:include page="../../common/flatHead.jsp" />
    <meta charset="UTF-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript">
		var basePath = "${ctx}";
		var baseUrl="${baseUrl}";
		$(function(){
			$(".new_right_content").css({"height":$(window).height()});
		});
	</script>
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}/css/jquery.autocomplete.css" />
	
	<link href="${ctx}flat/css/hm_style.css" rel="stylesheet">
	<link href="${ctx}logined/defect/css/form_default.css" rel="stylesheet">
	<link href="${ctx}logined/defect/css/style.css" rel="stylesheet">
	<style type="text/css">
		.hello{display:block;background-color: red}
		.uploadify-button {background-repeat:no-repeat;background-position:center left;}
		.annex ul li{float:left;margin-right:10px;}
		.txt p a{color: #ff5454 !important;}
		.selectLocation:hover{background-color: #e6e6e6}
		.new_right_content.no-padding {
					padding:0;
					background: none;
			}
	</style>
	<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
	<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
	<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>
	<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-uploadify.js"></script>
	<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/eventAddApp.js"></script>
	
	<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/addController.js"></script>
	<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectUser.js"></script>
	<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
	<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
	<script TYPE="text/javascript" src="${ctx}js/autocomplete/jquery.autocomplete.js"></script>
  </head>
  
  <body  ng-controller="addController">
  		<input type="hidden" value="<%=request.getParameter("instanceId")%>" id="instanceId">
		<input type="hidden" value="0" id="type">
		<input type="hidden" value="<%=eventLoginName%>" id="eventLoginName">
		<input type="hidden" value="<%=eventUserId%>" id="eventUserId">
		<input type="hidden" value="<%=applyPhone%>" id="applyPhone">
		<input type="hidden" value="<%=from%>" id="from">
		<input type="hidden"  id="applyPhoneName">
		<form novalidate="novalidate" name="myForm">
   <!--<p class="new_index_title mb20">归还上报</p>-->
				<ul class="describle_info describle_info_two" style="overflow:initial;">
					<li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">上报电话：</span>
						<span style="display: block;margin-top: 5px;"><%=applyPhone%></span>
					</li>
					<li class="pr percent30 fl" style="overflow: inherit;">
						<span class="des_info_title">上报人工号：</span>
						<div id="divselect_user" class="divselect4" style="width: 258px;position: relative;" >
			                <input type="text" class="select_style wd258 ng-pristine ng-valid ng-empty ng-touched" ng-model="loginName" id="loginName" ng-keyup="dispatchRefresh()" style="border: #d9d9d9 1px solid;">
			                <ul style="display:none;position: absolute;background-color: #fff;width: 255px;z-index:10;border: 1px solid #d9d9d9;border-top:none;height: 200px;overflow: auto" id="userListUl">
			                    <li ng-repeat="user in userList" style="height: 20px;padding:0 5px;cursor: pointer" class="selectLocation" ng-click="selectUser(user.userId,user.loginName,user.userName,user.groupId,user.groupName)" ><a href="javascript:;" selectid="{{user.userId}}"  style="color: #333;font-size: 14px;">{{user.loginName}}({{user.userName}})</a></li>
			                </ul>
           				 </div>
					</li>
					<%-- <li class="pr percent30 fl">
						<span class="des_info_title" style="width: 120px;">上报人工号：</span>
						<span style="width: 120px;float:left; margin-top: 4px" ng-model="loginName" id="loginName"><%=eventLoginName%></span>
			  			<!-- <input class="in_area" type="text" id="dispatchUserName" readonly="readonly" style="width: 258px" value="调度中心"> -->
					</li> --%>
					<li class="pr percent30 fl">
						<span class="des_info_title" style="width: 120px;">上报人姓名：</span>
						<span style="width: 120px;float:left; margin-top: 4px" id="dispatchUserName"><%=eventUserName%></span>
						<input type="hidden" id="userId" value="<%=eventUserId%>">
			  			<!-- <input class="in_area" type="text" id="dispatchUserName" readonly="readonly" style="width: 258px" value="调度中心"> -->
					</li>
					<li class="pr percent30 fl">
						<span class="des_info_title" style="width: 120px;">上报人部门：</span>
						<span style="width: 120px;float:left; margin-top: 4px" id="dispatchGroupName"><%=eventGroupName%></span>
						<!-- <input type="hidden" id="userId"> -->
			  			<!-- <input class="in_area" type="text" id="dispatchUserName" readonly="readonly" style="width: 258px" value="调度中心"> -->
					</li>
					<li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">维修事项：</span>
						<!-- <input type="text" class="in_area ng-pristine ng-untouched ng-valid ng-empty" ng-model="maintenanceItems" id="maintenanceItems" style="width: 258px"> -->
						<div id="divselect3" class="divselect4" style="width: 258px;position: relative;" >
			                <input type="text" class="select_style wd258" ng-model="maintenanceItems" id="maintenanceItems" ng-keyup="refresh()" style="border: #d9d9d9 1px solid;"/>
			                <ul style="display:none;position: absolute;background-color: #fff;width: 255px;z-index:10;border: 1px solid #d9d9d9;border-top:none;height: 200px;overflow: auto" id="locationListUl">
			                    <li ng-repeat="event in eventList" style="height: 20px;padding:0 5px;cursor: pointer" class="selectLocation" ng-click="selectP(event.id,event.eventName,event.urgencyLevel,event.completeTimeType,event.groupId,event.groupName)" ><a href="javascript:;" selectid="{{event.id}}"  style="color: #333;font-size: 14px;">{{event.eventName}}</a></li>
			                </ul>
           				 </div>
					</li>
					<li class="pr fl" style="width: 42%!important;margin-right: 3%;">
						<span class="des_info_title">处理部门：</span>
						<input type="hidden" id="processId" value="61">
						<input type="hidden" id="processType" value="2">
						<input type="hidden" id="showType" value="1">
						<input type="hidden" id="extension" value="1,2"/>
						<input class="in_area" type="text" id="processName" readonly="readonly" style="width: 258px" value="<%=eventGroupName%>">
						<span class="addMember"><a class="icon_add" style="cursor: pointer;" id="userSelect" fieldId="processId" fieldName="processName" fileType="processType" showType="showType" extension="extension">选择</a></span>
					</li>
					<li class="pr" >
						<span class="des_info_title"><!-- <em class="requireField">*</em> -->上报科室：</span>
						<input type="hidden" id="createGroupId" value=""/>
						<input type="hidden" id="showType" value="1"/>
			  			<input class="in_area" type="text" id="createGroupName" readonly="readonly" style="width: 258px" >
			  			<span class="addMember"><a
							class="icon_add" href="javascript:void(0)" id="createGroupSelect" fieldId="createGroupId" fieldName="createGroupName"  showType="showType" >选择</a></span>
					</li>
					<li class="pr">
						<span class="des_info_title">维修地点：</span>
						<input type="hidden" id="repaireId" value="">
						<input type="hidden" id="showType_repair" value="5">
						<input class="in_area" type="text" id="describe" ng-model="apply.describe" style="width: 258px">
						<span class="addMember">
							<a class="icon_add" href="javascript:void()" id="repaireSelect" fieldid="repaireId" fieldname="describe" showtype="showType_repair">选择</a>
						</span>
					</li>
					<li class="pr">
						<span class="des_info_title">备注：</span>
						<textarea class="defect_inword ng-pristine ng-untouched ng-valid ng-empty" ng-model="apply.memo" style="font-family: -webkit-body;width: 50%"></textarea>
					</li>
					
					<!-- <li class="pr">
						<span class="des_info_title">图片：</span>
						<span class="get_photo">
							<uploadfs class="add_file" config="uploadConfig" outfile="outfile" fs="fs" ></uploadfs>
						</span>
					</li> -->
					<li class="pr">
						<span class="des_info_title">紧急程度：</span>
						<!-- <label ng-repeat="a in gradeList" style="margin-right:8px" class="ng-binding ng-scope">
							<input ng-model="apply.grade" name="eventLevel" type="radio" value="2" ng-click="chageRadio(a.value)" class="ng-pristine ng-untouched ng-valid ng-not-empty">&nbsp;一般 
						</label>
						<label ng-repeat="a in gradeList" style="margin-right:8px" class="ng-binding ng-scope">
							<input ng-model="apply.grade" name="eventLevel" type="radio" value="1" ng-click="chageRadio(a.value)" class="ng-pristine ng-untouched ng-valid ng-not-empty">&nbsp;紧急 
						</label> -->
						<label ng-repeat="a in gradeList" style="margin-right:20px;margin-top: 8px;display: block; float: left;">
						   <input  ng-model="apply.grade"  name="eventLevel" type="radio" value="{{a.value}}"  ng-click="chageRadio(a.value)"/>&nbsp;{{a.name}} 
						</label>
					</li>
					 <li class="pr" style="display: none">
						<span class="des_info_title">期望完成时间：</span>
						<input type="text" name="defectTimeStr" id="defectTime" onfocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="in_area Wdate" style="width:258px;background-position-x:230px;cursor:pointer;">	
					</li> 
				</ul>
				<!--end .describle_info-->
				<div class="btn_container01">
					<!-- <span class="btn_inline_new hm_btn_blue ng-binding" ng-click="addApply(myForm.$valid)">上报</span> -->
					<span class="btn_inline_new hm_btn_blue"  ng-click="addApply(myForm.$valid)">{{instanceId?'提交':'提交'}}</span>
				</div>
				<!--end .btn_container01-->
			</form>
  </body>
  <script type="text/javascript">
  	$(function(){
  		$(document).click(function(){
  			$("#userListUl").hide();
  			$("#locationListUl").hide();
  		});
  	});
  
  </script>
</html>
