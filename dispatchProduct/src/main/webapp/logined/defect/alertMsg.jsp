<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>预览</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/systemManagement.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}js/common/ajaxfileupload.js"></script>
<script type="text/javascript">
/**
 * 开始上传
 * 
 * @return {Boolean}
 */
function startAjaxFileUpload() {
	var fileName = $("#fileToUpload").val();
	if (fileName == "") {
		$("#msg").html('请先选择需要导入的文件 ！');
		return false;
	}else{
		if(fileName.indexOf(".xls")==-1){
			$("#msg").html('请选择电子表格！');
			return false;
		}
	}
	var radioType = $("input:radio[name='radioType']:checked").val();
	var url = basePath + 'duty/importWorkDuty.action';
	$("#msg").html('正在导入，请稍候......'+'<img width="32" height="32" src="'+basePath+'flat/images/load.gif"></img>');
	$.ajaxFileUpload({
				url : url,
				secureuri : false,
				fileElementId : 'fileToUpload',
				dataType : 'text',
				data : {
					uploadFileName : fileName,
					radioType : radioType
				},
				success : function(data, status) {
					$("#msg").html(data);
					if(null != data && data.indexOf("共") == 0 ){
						freshPage = true;
						var win = art.dialog.open.origin;
					}					
				},
				error : function(data, status, e) {
					$("#msg").html("对不起！导入文件时出错！");
				}
			});

	return false;

}
</script>
</head>
<body class="bg_white">
<div class="importBox">
    <h4>选择导入文件：<input type="file" name="fileToUpload" id="fileToUpload" /></h4>
    <p style="text-align:left;padding-left:140px"><span id="msg"></span></p>
    <p style="text-align:left;padding-left:140px"><span>您可以导出当前的值班表修改后进行导入，或者下载值班表模板重新填写后导入。</span><a href="${ctx}down/dutyList.xls" id="importModule" class="ml10">点击获取值班表模板</a></p>
</div>
</body>
</html>