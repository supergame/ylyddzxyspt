<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="approveApp">
	<head>
		<meta charset="UTF-8">
		<title>值班签到缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<script type="text/javascript">
			var basePath = "${ctx}";
		</script>
	</head>
	<body style="background-color:#fff;">
		<div class="elasticFrame formPage">
			<form name="myForm" >
			  <table width="100%" cellspacing="0" cellpadding="0" border="0" class="inputTable">
			    <tbody>
			      <tr >
			        <th><label>维修标题</label></th>
			        <td>
			        	<input class="formText" type="text" name="" value="" placeholder="" id="advice" style="width:250px">
			        </td>
			      </tr>
			      <tr ng-if="dialogInfo.adviceTitle!=''">
			        <th><label>维修部门：</label></th>
			        <td>
			        	<select name="state" class="select_style" style="width:251px;">
							<option value="1">调度中心</option>
						</select>
			        </td>
			      </tr>
			      <tr ng-if="index==2">
			        <th><label>紧急程度：</label></th>
			        <td>
			            <select name="state" class="select_style" style="width:251px;">
							<option value="1">紧急</option>
							<option value="1">一般</option>
							<option value="1">普通</option>
						</select>
			        </td>
			      </tr>
			    </tbody>
			  </table>
		  </form>
		</div>
	</body>
</html>
