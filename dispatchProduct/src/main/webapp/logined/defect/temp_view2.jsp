<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html ng-app="view2App">
	<head>
		<meta charset="UTF-8">
		<title>上报缺陷</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog.css">
	    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400italic' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-theme-default.css">
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-theme-plain.css">
	    <link rel="stylesheet" href="${ctx}plugins/ngDialog/css/ngDialog-custom-width.css">
		<script type="text/javascript">
			var basePath = "${ctx}";
			var baseUrl = "${baseUrl}";
		</script>
		<script TYPE="text/javascript" src="${ctx}angularframe/lib/angular.min.js"></script>
		<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
		<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basetree.js"></script>
		<script src="${ctx}plugins/ngDialog/js/ngDialog.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/view2App.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/view2Controller.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>
		<style>
			.ngdialog.ngdialog-theme-default .ngdialog-content{padding:0px;padding-bottom:1em;}
			.describle_title{border-bottom:1px solid #ff9933;}
			.in_area{width:100%;}
			.ngdialog-buttons{padding:0 10px;}
			.des_photos_box{position:relative;overflow:initial;}
			.des_photos_box img:hover{transform:scale(10,10);position:absolute;top:0px;left:150px;background:#fff;z-index:999;box-shadow: 0 0 5px #333;}
			.paizhao{overflow:initial !important;min-height:76px;}
			.ng-cloak{display:none;}
		</style>
	</head>
	<body ng-controller="view2Controller">
		<input type="hidden" id="oper" value="<%=request.getParameter("oper") %>">
		<input type="hidden" id="instanceId" value="<%=request.getParameter("instanceId") %>">
		
		<!--右侧描述信息标题及列表-->
		<!-- <div>
			<p class="describle_title">缺陷信息</p> -->
		<div class="new_right_content">
			<div class="index_detail pdb8 wd1362">
				<p class="new_index_title mb20">
					配送工单详情
				</p>
				<ul class="describle_info">
				
					<li class="pr fl percent30">
						<span class="des_info_title">工单编号：</span>
						<span class="des_info_decration" ng-cloak>{{apply.defectNumber}}</span>
					</li>
					<li class="pr percent30 fl">
						<span class="des_info_title">工单状态：</span>
						<span class="des_info_decration" ng-cloak><span class="{{apply.stateVo|stateColor}}">{{apply.stateVo}}{{apply.delayStateVo}}</span></span>
					</li>
					<li class="pr percent30 fl">
						<span class="des_info_title">紧急程度：</span>
						<span class="des_info_decration" ng-cloak>{{apply.gradeVo}}</span>
					</li>
					
					<li class="pr percent30 fl">
						<span class="des_info_title">设备名称：</span>
						<span class="des_info_decration" ng-cloak>{{apply.equipmentName}}</span>
					</li> 
					<li class="pr percent30 fl">
						<span class="des_info_title">设备数量：</span>
						<span class="des_info_decration" ng-cloak>{{apply.equipmentNumber}}</span>
					</li> 
					<!-- <li class="pr percent30">
						<span class="des_info_title" >所属部门：</span>
						<span class="des_info_decration" ng-cloak>{{apply.defectDepartmentVo}}</span>
					</li> -->
					<li class="pr">
						<span class="des_info_title">借用人：</span>
						<span class="des_info_decration" ng-cloak>{{apply.createUserName?apply.createUserName:'--'}}({{apply.createUserPhone?apply.createUserPhone:'--'}})</span>
					</li>
					<li class="pr">
						<span class="des_info_title">借用地点：</span>
						<span class="des_info_decration" ng-cloak>{{apply.describe}}</span>
					</li>
	
					
					<%--  <li class="pr {{attachmentIds.length==0?'':'paizhao'}}">
			            <span class="des_info_title">拍照：</span>
			            <div class="des_photos_box" ng-repeat="attachmentId in attachmentIds">
			                <img src="${baseUrl}fileServer/download?fileId={{attachmentId}}" />
			            </div>
			            <span class="des_info_decration" ng-show="attachmentIds.length==0">--</span>
	        		</li> --%>
	        		
	        		<li class="pr">
	                	<span class="des_info_title">录音：</span>
		                <span class="des_info_decration" ng-show="apply.audioIds" >
		                	<audio controls="controls" id="myVideo" preload="preload" src="{{audioSrc}}">    
							</audio>
						</span>
						<span class="des_info_decration" ng-show="!apply.audioIds">
		                	--
						</span>	
           			 </li>
	        		
	        		
					<li class="pr">
						<span class="des_info_title">上报时间：</span>
						<span class="des_info_decration" ng-cloak>{{apply.createTimeStr?apply.createTimeStr:'--'}}</span>
					</li>
					<li class="pr">
						<span class="des_info_title">期望配送完成时间：</span>
						<span class="des_info_decration" ng-cloak >{{apply.defectTimeStr}}<span style="{{outTimeState==1?'color:red':''}}">{{outTimeState==1?'(逾期'+hour+')':(outTimeState==2?'':'(剩余'+hour+')')}}</span></span>
					</li>
					<li class="pr">
						<span class="des_info_title">备注：</span>
						<span class="des_info_decration" ng-cloak>{{apply.memo?apply.memo:'--'}}</span>
					</li>
					<li class="pr" ng-if="apply.achievementStr">
						<span class="des_info_title">满意度评价：</span>
						<span class="des_info_decration" ng-cloak>{{apply.achievementStr}}</span>
					</li>
				</ul>

				<div class="step_box">
					<ul class="defect_step">
						<li ng-repeat="history in historyList" ng-cloak>
							<font>{{history.endTime}}</font>
							<span>{{history.processerName}}</span>({{history.processerPhone}}){{history.taskName|getHistoryDetail:history.approveResult:history.nextProcesserName:history.nextProcesserPhone:history.advice}}
							<!-- <em class="liucheng_checked"></em>
							<p class="renyuan">{{history.taskName|getHistoryTitle}}：{{history.processerName}}({{history.processerPhone}})
								<span class="liucheng_time"></span>
							</p>
							<span class="liucheng_beizhu">{{history.advice}}</span> -->
						</li>
					</ul>
				</div>
					 <div ng-if="oper=='apply'&&(apply.state==-1||apply.state==0||apply.state==6||apply.state==1||apply.state==2)" class="btn_container01 mb20"> 
						<span class="btn_inline_new hm_btn_yellow_new" ng-click="editDefect()" ng-if="apply.state==0||apply.state==-1">编辑</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="draftDefect();"> &nbsp;作废&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='apply'&&apply.state==5" class="btn_container01 mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="agree();">同意</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="rejuest();"> &nbsp;拒绝&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div> 
					<div ng-if="oper=='dispatch'" class="btn_container01 mb20">    
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(9)">调度</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="applyDefect();" ng-if="apply.isDraft==0"> &nbsp;请求作废&nbsp; </span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='approve'" class="btn_container01 mb20">    
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(6)">接收</span>
						<span class="btn_inline_new hm_btn_red_new ml15" ng-click="openDialog(0);" ng-if="apply.turnNum<turnNum"> &nbsp;转交&nbsp; </span>
						<!-- <span class="btn_inline_new hm_btn_red_new ml15" ng-click="applyDefect();" ng-if="apply.isDraft==0"> &nbsp;请求作废&nbsp; </span> -->
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='defect'" class="btn_container01 mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(1);">处理完成</span>
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="insertEquipment();">添加设备</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-if="apply.isDelay==0||apply.isDelay==3" ng-click="openDialog(7);">申请延期</span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='accept'" class="btn_container01 mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(2);">验收通过</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(3);">驳回</span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='archive'" class="btn_container01 mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="openDialog(4);">归档</span>
						<!-- <span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="openDialog(5);">驳回验收</span> -->
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='delay'" class="btn_container01 mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-if="apply.isDelay==0||apply.isDelay==3" ng-click="openDialog(7);">申请延期</span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='approveDelay'" class="btn_container01 mb20">
						<span class="btn_inline_new hm_btn_yellow_new " ng-click="agreeDelay()" ng-if="apply.isDelay==1">同意延期</span>
						<span class="btn_inline_new hm_btn_yellow_new ml15" ng-click="rejuestDelay()" ng-if="apply.isDelay==1">拒绝延期</span>
						<span class="btn_inline_new hm_btn_white ml15" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
					<div ng-if="oper=='view'||(oper=='apply'&&apply.state!=-1&&apply.state!=0&&apply.state!=5&&apply.state!=6)||oper=='delayView'" class="btn_container01 mb20">
						<span class="btn_inline_new hm_btn_white" ng-click="goBack();"> &nbsp;返回&nbsp; </span>
					</div>
			
				<!-- <div class="btn_container01 mb20">
					<span class="btn_inline_new hm_btn_white">返回</span>
					<span class="btn_inline_new hm_btn_red_new ml15">作废</span>
					<span class="btn_inline_new hm_btn_yellow_new ml15">编辑</span>
				</div> -->
			</div>
		</div>
		<script type="text/ng-template" id="modalDialogId">
		<div class="ngdialog-message">
            <p class="describle_title">{{dialogInfo.theme}}</p>
			<ul class="describle_info">
				

				<li class="pr" ng-if="dialogInfo.userTitle!=''" style="height:42px;overflow:visible;">
					<span class="des_info_title">{{dialogInfo.userTitle}}：</span>
					 <div class="divselect12  divselect13 ">
			      		<cite class="cite12 text_ellipsis cite13" style="width:192px"  ng-click="toggleSelectUser()" ng-show="!flag">{{node1 | selectUserNames}}</cite>
						<cite class="cite12 text_ellipsis cite13" style="width:192px;{{flag?'background:url()':''}}"  ng-show="flag">{{node1 | selectUserNames}}</cite>
			      		<ul id="tree2" class="ztree leibie" basetree tree-data="data1" tree-set="set1" check-node="node1" select-node="node" default-select="defaultSelectUser" ng-show="showSelectUserValue==1" style="display: block;height:200px !important;width:200px !important;z-index:20000;position:absolute;top:28px;left:0px;border:1px solid #d5d5d5;" ng-mouseleave="showSelectUserValue=2">
						</ul>
		  			</div>
				</li>
				<li class="pr" ng-if="dialogInfo.adviceTitle!=''">
					<span class="des_info_title">{{dialogInfo.adviceTitle}}：</span>
					<input class="in_area percent30" type="text" name="" value="" placeholder="" ng-model="advice">
				</li>
			</ul>
        </div>
        <div class="ngdialog-buttons">
            <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog('button')">取消</button>
            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm({'advice':($$childTail.advice),'userId':($$childHead.node1|selectUserIds)})">确定</button>
        </div>
    	</script>
    	
	</body>
</html>
