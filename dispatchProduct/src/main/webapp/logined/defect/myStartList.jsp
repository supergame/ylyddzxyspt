<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="myStartApp">
<head >
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>我上报的</title>
<link href="${ctx}css/author_main.css" rel="stylesheet" type="text/css" />
<jsp:include page="../../common/flatHead.jsp" />
<script type="text/javascript">
	var basePath = "${ctx}";
	$(function(){
		$(".new_right_content").css({"height":$(window).height()});
	});
</script>
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/Reminder.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}angularframe/css/ui-basepage.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular-animate.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/lib/angular-sanitize.min.js"></script>
<script TYPE="text/javascript" src="${ctx }angularframe/directives/ui-basepage.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/app/myStartApp.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/filter/commonFilter.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/myStartController.js"></script>
<script TYPE="text/javascript" src="${ctx}js/logined/defect/service/service.js"></script>

<style>
	.ng-cloak{display:none;}
</style>
</head>
<body ng-controller="myStartController">
<input type="hidden" id="type" value="<%=request.getParameter("type") %>" />
<input type="hidden" id="isOut" value="<%=request.getParameter("isOut")%>" />
<input type="hidden" id="isForkGroup" value="<%=request.getParameter("isForkGroup")%>" />
<input type="hidden" id="oldIDisplayStart" value="<%=request.getParameter("iDisplayStart")%>" />

<div class="new_right_content">
	<div class="index_detail pdb8">
		<p class="new_index_title ng-cloak">
			{{title}}
		</p>
		<div class="list">
			<div class="overf mb20">
				<ul class="select_list fl">
				
					<!-- <li >
						<span class="select_list_title">时间：</span>
						<select name="timeType" class="select_style" ng-model="searchVo.timeType">
							<option value="1" selected="selected">最近一个月</option>
							<option value="">全部</option>
						</select>
					</li> -->
					<li>
					<span class="select_list_title">工单类型：</span>
						<select name="type" class="select_style" ng-model="searchVo.type">
							<option value="">全部</option>
							<!-- <option value="0">维修</option> -->
							<option value="1">配送</option>
							<option value="2">归还</option>
						</select>
					</li>
					<li ng-if="type!='delayView'">
						<span class="select_list_title">工单状态：</span>
						<select name="state" class="select_style" ng-model="searchVo.state">
							<option value="">全部</option>
							<option value="-1">待调度</option>
							<option value="0,6">待接收</option>
							<!-- <option value="5">待作废</option> -->
							<option value="1,7">待处理</option>
							<option value="2,8">待验收</option>
							<option value="3">已完结</option>
							<!-- <option value="4">已归档</option> -->
							<option value="9">已作废</option>
						</select>
					</li>
					
					<li ng-if="type=='delayView'">
						<span class="select_list_title">工单状态：</span>
						<select name="delayState" class="select_style" ng-model="searchVo.delayState">
							<option value="">全部</option>
							<option value="1,3">待延期</option>
							<option value="2">延期中</option>
						</select>
					</li>
					
					<!-- 
					<li>
						<span class="select_list_title">所属部门：</span>
						<select name="defectDepartment" class="select_style" ng-model="searchVo.defectDepartment">
							<option value="">全部</option>
							<option value="{{defectDepartment.value}}" ng-repeat="defectDepartment in defectDepartmentList">{{defectDepartment.name}}</option>
						</select>
					</li> -->
					<!-- <li>
						<span class="select_list_title">专业：</span>
						<select name="defectProfessional" class="select_style" ng-model="searchVo.defectProfessional">
							<option value="">全部</option>
							<option value="{{defectProfessional.value}}" ng-repeat="defectProfessional in defectProfessionalList">{{defectProfessional.name}}</option>
						</select>
					</li> -->
					<li  style="width: 436px;">
						<span class="select_list_title">上报时间：</span>
						<input id="startTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',maxDate:'#F{$dp.$D(\'endTime\')}'})" class="in_area Wdate" style="width:170px;margin-left:0px"/> -
						<input id="endTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'startTime\')}'})" class="in_area Wdate" style="width:170px"/>
					</li>
					<li class="key_word" ng-if="type=='view'">
						<span class="select_list_title">调度人：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入调度人" ng-model="searchVo.dispatchUserName"/>
					</li>
					<li class="key_word" ng-if="type=='view'">
						<span class="select_list_title">上报人：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入上报人" ng-model="searchVo.userName"/>
					</li>
				</ul>
				<ul class="select_list fl" ng-if="type=='view'">
					<li class="key_word">
						<span class="select_list_title">上报部门：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入上报部门" ng-model="searchVo.createGroupName"/>
					</li>
					<li class="key_word">
						<span class="select_list_title" style="width: 70px;text-align: right;">处理人：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入处理人" ng-model="searchVo.processUserName"/>
					</li>
					<li class="key_word" style="padding-left: 82px;">
						<span class="select_list_title">处理部门：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入处理部门" ng-model="searchVo.processUserGroup"/>
					</li>
					<li class="key_word" style="padding-left: 65px;">
						<span class="select_list_title">关键字：</span>
						<input type="text"  maxlength="30" class="select_style" placeholder="请输入名称" ng-model="searchVo.searchName"/>
					</li>
				</ul>
				<div class="fl pd4">					
					<span class="btn_inline01 btn_search_new" ng-click="searchList();">查找</span>
					<span class="btn_inline01 btn_output_new" ng-click="exportApply();" style="cursor: pointer">导出</span>
				</div>
			</div>
			<table cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" id="myTable">

				<thead>
				<tr>
		        	<th width="100px">事件编号</th>
		        	<th width="60px">事件类型</th>
		        	<th width="120px">处理事项</th>
		        	<th width="60px">是否紧急</th>
		        	<th ng-if="type=='view'" width="100px">上报电话</th>
		        	<th ng-if="type=='view'" width="60px">调度人</th>
		        	<th ng-if="type=='view'" width="50px">上报人</th>
        			<th ng-if="type=='view'" width="70px">上报部门</th>
        			<th ng-if="type=='view'&&isOut!=1" width="50px">协作人</th>
		       		<th width="110px">地点</th>
		       		<th width="120px">上报时间</th>
		       		<!-- <th  style="width:100px;">上报部门</th> -->
		       		<!-- <th>完工时间</th> -->
		        	<th class="data_r" width="60px">工单状态</th>        	
		        	<th class="data_r" width="70px">处理部门</th>
		        	<th class="data_r" width="60px">处理人</th>
		        	<th width="60px" ng-if="type!='view'">催单次数</th>
		        	<th class="tdCenter" width="{{isOut==1?'88':'60'}}px">操作</th>
		     	 </tr>
				</thead>
			<tbody>
		     		<tr ng-repeat="apply in aaData" ng-class-odd="'odd'" ng-class-even="'even'"
		     		style="font-size: 14px;" onClick="xuanRan(this)">
			        	<td class="tdCenter" ng-cloak>{{apply.defectNumber}}</td>
		        		<td class="tdCenter" ng-cloak>{{apply.type==2?"归还":(apply.type==1?"配送":"维修")}}</td>
		        		<td class="longTxt" title="{{apply.equipmentName}}{{apply.type==2||apply.type==1?'&nbsp;(数量:'+apply.equipmentNumber+')':''}}" ng-cloak>{{apply.equipmentName}}{{apply.type==2||apply.type==1?"&nbsp;(数量:"+apply.equipmentNumber+")":""}}</td>
			        	<td class="tdCenter" ng-cloak style="{{apply.grade==1?'color:red':''}}">{{apply.grade==1?'紧急':'一般'}}</td>
			        	<td class="tdCenter" ng-cloak ng-if="type=='view'">{{apply.applyPhone}}</td>
			        	<td class="tdCenter" ng-cloak ng-if="type=='view'">{{apply.dispatchUserName}}</td>
			        	<td class="tdCenter" ng-cloak ng-if="type=='view'">{{apply.createUserName}}</td>
		        		<td class="longTxt" ng-cloak title="{{apply.groupName}}" ng-if="type=='view'">{{apply.groupName}}</td>
			        	<td class="tdCenter" ng-cloak ng-if="type=='view'&&isOut!=1">{{apply.isSelectPeople==1?'是':'否'}}</td>
			        	<td class="longTxt" title="{{apply.describe}}" ng-cloak>{{apply.describe}} </td>
			        	<td class="tdCenter"  ng-cloak>{{apply.createTime}} </td>
			        	<!-- <td class="tdCenter"  ng-cloak>{{apply.groupName}} </td> -->
			        	<!-- <td class="tdCenter"  ng-cloak style="{{apply.outTimeState==1?'color:red':''}}">{{apply.outTimeState==0?'剩余'+apply.hour:(apply.outTimeState==1?('逾期'+apply.hour):apply.defectTime)}} </td> -->
			        	<td class="tdCenter"><span class="{{apply.state|stateColor}}" ng-cloak>{{apply.state}}{{apply.delayState}}</span></td>
			        	<td class="longTxt" ng-cloak title="{{apply.processGroupName}}">{{apply.processGroupName}}</td>
			        	<td class="tdCenter" ng-cloak title="{{apply.processUserName}}">{{apply.processUserName}}</td>
			        	<td class="tdCenter" ng-cloak ng-if="type!='view'">{{apply.reminderNum}}</td>
			        	<td class="tdCenter" ng-cloak>
			        		<a href="javascript:void(0);" ng-click="draftDefect(apply.instanceId);" ng-show="isOut==1&&apply.stateInt!=9&&apply.stateInt!=3">作废</a>
			        		<a href="javascript:void(0);" ng-click="goView(apply.instanceId,apply.type)">查看</a>
			        	</td>
			         </tr>
			      	<tr class="odd" ng-show="result==0"><td valign="top" colspan="{{type=='view'?14:11}}" class="dataTables_empty">没有检索到数据</td></tr>
			    </tbody>
		</table>
		<!-- 分页 -->
		<page page-search="getList()" i-total-display-records="result" i-display-length="iDisplayLength" i-display-start = "iDisplayStart" ></page>
		</div>
	</div>
</div>
</body>
</html>