<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>设备归还</title>
		<jsp:include page="../../common/flatHead.jsp" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/css/hm_style.css" />
		<link rel="stylesheet" href="${ctx}/css/jquery.autocomplete.css" />
		<style type="text/css">
			.hello{display:block;background-color: red}
			.uploadify-button {background-repeat:no-repeat;background-position:center left;}
			.annex ul li{float:left;margin-right:10px;}
			.txt p a{color: #ff5454 !important;}
			.selectLocation:hover{background-color: #e6e6e6}
			.new_right_content.no-padding {
					padding:0;
					background: none;
			}
		</style>
		<script type="text/javascript">
			var baseUrl="${baseUrl}";
			
		</script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/selectUser.js"></script>
		<script TYPE="text/javascript" src="${ctx}js/logined/defect/controller/returnCapital.js"></script>
	</head>
	<body>
		<input type="hidden" value="2" id="type">
		<input type="hidden" value="${param.capitalId }" id="capitalId">
			<div class="new_right_content no-padding" style="min-width:600px;">
			<div class="index_detail pdb8" style="margin-top: 30px;">
				<ul class="describle_info describle_info_two" style="overflow:initial;">
					<li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">设备名称：</span>
						<div id="divselect3" class="divselect4" style="width: 258px;position: relative;" >
			                <input type="hidden" class="select_style wd258"  id="maintenanceItems" style="border: #d9d9d9 1px solid;"/>
			                <span id="maintenanceItemsText" style="line-height: 32px;height:32px;"></span>
           				 </div>
					</li>
					<li class="pr fl" style="overflow: inherit;">
						<span class="des_info_title">设备数量：</span>
						<span style="line-height: 32px;height:32px;">1</span>
						<input type="hidden" class="in_area"  id="equipmentNumber" style="width: 258px" value="1"/>
					</li>
					<li class="pr"  >
						<span class="des_info_title">处理部门：</span>
						<input type="hidden" id="processId" value="61"/>
						<input type="hidden" id="showType" value="1"/>
						<input type="hidden" id="processType" value="2">
						<input type="hidden" id="extension" value="1,2"/>
			  			<input class="in_area" type="text" id="processName" readonly="readonly" style="width: 258px" value="调度中心">
			  			<span class="addMember"><a
							class="icon_add" href="#" id="userSelect" fieldId="processId" fieldName="processName" fileType="processType" showType="showType" extension="extension">选择</a></span>
					</li>
					<li class="pr" >
						<span class="des_info_title"><!-- <em class="requireField">*</em> -->上报科室：</span>
						<input type="hidden" id="createGroupId" value="${sessionScope.groupInfo.groupId }"/>
						<input type="hidden" id="showType" value="1"/>
			  			<input class="in_area" type="text" id="createGroupName" readonly="readonly" style="width: 258px" value="${sessionScope.groupInfo.groupName }">
			  			<!-- <span class="addMember"><a
							class="icon_add" href="#" id="createGroupSelect" fieldId="createGroupId" fieldName="createGroupName"  showType="showType" >选择</a></span> -->
					</li>
					<li class="pr">
						<span class="des_info_title">使用地点：</span>
						<input type="hidden" id="repaireId" value="61"/>
						<input type="hidden" id="showType_repair" value="5"/>
			  			<input class="in_area" type="text" id="describe_temp"  style="width: 258px">
			  			<!-- <span class="addMember"><a
							class="icon_add" href="#" id="repaireSelect" fieldId="repaireId" fieldName="describe" showType="showType_repair">选择</a></span> -->
					</li> 
					<li class="pr">
						<span class="des_info_title">备注：</span>
						<textarea class="defect_inword" id="memo" style="font-family: -webkit-body;width: 67%"></textarea>
					</li>
					<li class="pr">
						<span class="des_info_title">紧急程度：</span>
						<span style="line-height: 32px;height:32px;">
							<input  name="eventLevel" type="radio" value="1" />&nbsp;紧急
							<input  name="eventLevel" type="radio" value="2" checked="checked"/>&nbsp;一般
						</span>
					</li>
					<li class="pr" style="display: none"> 
						<span class="des_info_title"><em class="requireField">*</em>完工时间：</span> 
						  <input type="text" name="defectTimeStr" id="defectTime" onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="in_area Wdate" style="width:258px;background-position-x:230px;cursor:pointer;"/>
					 </li> 
				</ul>
		</div>
		</form>
	</body>
	<script type="text/javascript">
	  	$(function(){
	  		$(document).click(function(){
	  			$("#locationListUl").hide();
	  		});
	  	});
  </script>
</html>
