<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<jsp:include page="../../../../common/flatHead.jsp" />
		<link rel="stylesheet" href="../../css/style.css">			
		 <link href="../../css/reset.css" rel="stylesheet" type="text/css" />
		<link href="../../css/main.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
		<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
		<link href="../../css/datatable_default.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			.inputTable th{width:82px;}
			.searchArea{padding-top:20px;}
  			.dataTables_wrapper{
			 padding: 0 20px;
			}
			.canverse_right_tap{
			 height:60px;
			}
			.canverse_right_tap span{
			 margin:12px 10px;
			}
			.canverse_tap_area{
			 margin:12px 10px;
			}
		</style>
			
		<script type="text/javascript" src="${ctx}flat/plugins/datatable/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="${ctx}js/placeholder.js"></script>
		<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
		<script type="text/javascript" src="${ctx}flat/plugins/tree/skins/jquery.ztree.all-3.2.min.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/goods/good.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/goods/groupUseGoods.js"></script>
        <script type="text/javascript" src="${ctx}js/logined/report/goods/repairUseGoods.js"></script>
	
	</head>
	<body> 
			<div class="converse_right">
				<div class="canverse_right_two">
					<div class="canverse_right_tap">
						<span id="callLogDetailSpan" class="active">报修科室物资使用统计</span>
						<span>后勤维修人员使用材料统计</span> 
					</div>					
					<div class="canverse_tap_area">
						<!-- 报修科室物资使用统计 -->
                         <jsp:include page="groupUseGoods.jsp"/>	
                         <!-- 后勤维修人员使用材料统计-->
						 <jsp:include page="repairUseGoods.jsp"/>    
					</div>	
				</div>
			</div>
	</body>
</html>