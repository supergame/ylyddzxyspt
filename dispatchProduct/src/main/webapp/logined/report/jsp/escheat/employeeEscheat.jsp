<%--
  Created by IntelliJ IDEA.
  User: lilipo
  Date: 2017/5/13
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page  contentType="text/html;charset=UTF-8" language="java" %>
<%--员工归还工作量统计--%>
<div class="canverse_tap_box hide" >
    <div class="searchArea">
        <table cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td class="right">
                    <label style="padding-left:20px;color:#999;font-size:14px;">时间：</label>
                    <input type="text" onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'endDate_employee\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="begDate_employee" class="in_something2 pr Wdate" name=""/>
                    -
                    <input type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'begDate_employee\')}',skin:'default',dateFmt:'yyyy-MM-dd'})" id="endDate_employee" class="in_something2 pr Wdate" name=""/>
                    <label style="padding-left:20px;color:#999;font-size:14px;">员工：</label>
                    <input type="text"  id="employName" class="input_css" name="" style="width: 120px;"/>
                    <button class="btn_jinhu_add" onclick="query_employee()">查询</button>
                    <button class="btn_jinhu_add" onclick="report_export_employee()">导出</button>
                    <button class="btn_jinhu_add" onclick="change_employee(this,0)">图表视图</button>
                    <button class="btn_jinhu_add" id="list_button_e" onclick="change_employee(this,-1)">列表视图</button>
                </td>
                <!-- <td style="width:110px;">
                    
                </td> -->
            </tr>
            </tbody>
        </table>
    </div>

    <div class="IllegalsNum" id="view_employee" style="width: 70%;height:60%; margin-left:120px;"></div>
    
    <div class="list_IllegalsNum" id="list_employee">
        <div id="table_wrapper" class="dataTables_wrapper" role="grid">
            <div id="myTable_Squadron_processing" class="dataTables_processing" style="visibility: hidden;">正在加载数据...</div>
            <table id="tables" class="pretty dataTable" width="100%" cellspacing="0" cellpadding="0" border="0">
                <colgroup>
				    <col width="50px">
				    <col width="150px">
				    <col width="150px">
				    <col width="70px">
				    <col width="70px">
				    <col width="70px">
				    <col width="80px">
				    <col width="100px">
				    <col width="110px">
			    </colgroup>
                <thead>
                <tr>
                    <th >序号</th>
                    <th >姓名</th>
                    <th >班组名称</th>
                    <th >接工量</th>
                    <th >完工量</th>
                    <th >设备数</th>
                    <th >未完工量</th>
                    <th >归还总耗时</th>
                    <th >平均总耗时</th>
                </tr>
                </thead>
                <tbody id="tbody_employee">

                </tbody>
            </table>
        </div>
    </div>
</div>