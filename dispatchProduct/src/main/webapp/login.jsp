<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="common/taglibs.jsp"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>后勤调度系统</title>
<meta http-equiv="X-UA-Compatible" content="chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="${ctx}images/favicon.ico" mce_href="favicon.ico" rel="bookmark" type="image/x-icon" /> 
<link href="${ctx}images/favicon.ico" mce_href="favicon.ico" rel="icon" type="image/x-icon" /> 
<link href="${ctx}images/favicon.ico" mce_href="favicon.ico" rel="shortcut icon" type="image/x-icon" /> 
<link href="${ctx}flat/css/loginForward/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/loginForward/newLogin.css?version=${version}" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${ctx}js/common/jquery-1.8.0.min.js"></script>
<!--  dialog -->
<script type="text/javascript" src="${ctx}flat/plugins/dialog/artDialog.js?skin=default"></script>
<script type="text/javascript" src="${ctx}flat/plugins/dialog/artDialog_alert.js"></script>
<script type="text/javascript" src="${ctx}flat/plugins/dialog/iframeTools.js"></script>
<script type="text/javascript" src="${ctx}js/common/artClose.js"></script>
<!--  dialog end -->
<script type="text/javascript" src="${ctx}js/common/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}js/common/jquery.query.js"></script>
<script type="text/javascript" src="${ctx}js/login.js"></script>
 
<script type="text/javascript" src="${ctx}js/iecheck.js"></script>
<script type="text/javascript" src="${ctx}flat/js/placeholder_login.js"></script>
<script type="text/javascript">
        //防止被加载到框架中
        if(self.location!=top.location){
            top.location=self.location;
        }
        var basePath ="${ctx}";
        
        //登录框上下居中
        $(document).ready(function($) {
		/*resizeLayout();//内容上下居中*/
});
function resizeLayout() {
	// 主操作区域高度
      var wHeight = (window.document.documentElement.clientHeight || window.document.body.clientHeight || window.innerHeight);
      var cHeight = wHeight - 375;
$(".mainBox").css("padding-top",cHeight/2);
};
$(window).resize(function() {
			/*resizeLayout();*/
		});
    </script>

</head>
<body>
      <form  action="${ctx}j_spring_security_check" method="post" name="loginForm" id="loginForm">
	    <div class="login_header">
			<span class="logo-box">
				<div class="logo-left">
					<img src="flat/images/login/logo-new.png" class="logo" alt="logo" />
					<span class="name">
						<img src="flat/images/login/logo-new-name.png" alt="logo">
					</span>
				</div>
				
				<!--end .logo-left-->
				<span class="code-area">
					<img src="flat/images/login/code.png" alt="code">
					<span class="txt">下载手机APP</span>
				</span>
			</span>
			
		</div>
		
		
		<!--end .login_header-->
		<div class="login_wrapper" id="LoginHeight">
		<div class="login_bg pr">
			<div class="login_box pr">
				<div class="good">
					<img src="flat/images/login/bg-text.png" alt="text"/>
				</div>
				<div class="login_content">
					<p class="login_title">用户登录</p>
					<div class="user">
						<em class="user_ico"></em>
						<input class="input_block" maxlength="16" id="j_username" name="j_username" type="text" placeholder="请输入工号" />
					</div>
					<div class="user mt15">
						<em class="psw_ico"></em>
						<input class="input_block psw" type="password" maxlength="20" name="j_password" onkeyup="value=value.replace(/[^\w\.\*\@\#\%\^\&\(\)\?\/]/ig,'')" id="j_password" placeholder="请输入密码"/>
					</div>
					<div class=" yzma" style="display: none">
						<img alt="点击更换验证码" title="点击更换验证码"   onclick="loadimage();" name="randImage" id="randImage" src="${ctx}/common/code.jsp" />
					</div>
					<p class="error" id="error" style="display:none"><em></em><label id="perror"></label>&nbsp;</p>
					<label class="rember_info pr" style="padding-left: 0px;"><input id="cb_remember" type="checkbox" />&nbsp;&nbsp;记住用户名和密码</label>
					<!-- <span class="rember_info pr"><b class="rember_checked" id="cb_remember"></b>记住用户名和密码</span> -->
					<a class="login_btn" style="text-decoration: none;cursor: pointer" title="" id="btnLogin" >登录</a>
				</div>
			</div>
		</div>
		</div>
		<!--end .login_wrapper-->
	</form>
	
	<script>
		$(".rember_info").on("click",function(){
			$(this).children("b").toggleClass("rember_checked");
		})
		
	</script>
	<script type="text/javascript">
		window.onload = function modify() {
		var s = document.documentElement.clientHeight;document.getElementById("LoginHeight").style.height =s +"px";
		}
		window.onresize = modify;
	</script>
</body>

</html>