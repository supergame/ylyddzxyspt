/*
	<input type="text" ng-model="xxx" date-picker config="config"/>
	scope.config = {
					dateFmt: 'yyyy-MM-dd'
				};
*/
(function() {
	angular.module("ui-datepicker", []).directive("datePicker", function() {
		return {
			require: "ngModel",
			scope: {
				config: "="
			},
			link: function(scope, element, attrs, ngModel) {
				var defaultConfig = {
					onpicking: onpicking,
					oncleared: oncleared
				};
				//console.log(scope.config);
				var dst = angular.merge(defaultConfig, scope.config);
				//console.log(dst);
				element.val(ngModel.$viewValue);

				function onpicking(dp) {
					var date = dp.cal.getNewDateStr();
					scope.$apply(function() {
						ngModel.$setViewValue(date);
					});
				}

				function oncleared() {
					scope.$apply(function() {
						ngModel.$setViewValue("");
					});
				}
				element.bind('click', function() {
					WdatePicker(dst);
				});
			}
		}
	});
})();