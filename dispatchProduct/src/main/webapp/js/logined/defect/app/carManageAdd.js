$(document).ready(function() {
	//点击保存按钮
	$("#saveCar").click(function(){
		saveCar();
//	     return false;
	});
	//单击取消按钮
	$("#cancelCar").click(function(){
		 artDialog.close();
//	     return false;
	});
});
//点击保存
function saveCar() {
	//车牌号
	var carNo=$.trim($("#carNo").val());
	if (carNo == "") {
		art.dialog.alert('请输入车牌号');
		return;
	}
	//车辆品牌
    var carBrand = $.trim($("#carBrand").val());
    if (carBrand == "") {
    	art.dialog.alert('请输入车辆品牌');
		return;
	}
    //车辆型号
    var carModel = $.trim($("#carModel").val());
    //车辆类型
    var carType=$("#carType").val();
    
    
    var paramData = {
	    'carVo.carNo':carNo,
		'carVo.carBrand':carBrand,
		'carVo.carModel':carModel,
		'carVo.carType':carType
	};
    $.ajax({
		url : basePath+"cars/addCar.action",
		type : "post",
		dataType :'json',
		data:paramData,
		success : function(data) {
			if(data == 0) {
				var getDataTable=art.dialog.data('getDataTable');
				getDataTable();
				art.dialog.close();
				art.dialog.tips('保存车辆成功！');
            }else if(data == 1){
            	art.dialog.alert("车辆号重复!");
            } 
			else {
            	art.dialog.alert('保存车辆失败！');
            }
	}});
}