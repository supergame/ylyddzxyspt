$(document).ready(function() {
	//保存
	$("#saveCars").click(function(){
		var carNo=$.trim($("#carNo").val());
		if (carNo == "") {
			art.dialog.alert('请输入车牌号');
			return;
		}
		var carBrand = $.trim($("#carBrand").val());
		if (carBrand == "") {
		   	art.dialog.alert('请输入车辆品牌');
			return;
		}
		var carModel = $.trim($("#carModel").val());
		var carType=$("#carType").val();
		var paramData = {
				'carVo.id':$("#id").val(),
			    'carVo.carNo':carNo,
				'carVo.carBrand':carBrand,
				'carVo.carModel':carModel,
				'carVo.carType':carType
			};
		$.ajax({
			url: basePath+"cars/updateCars.action",
			type : "post",
			dataType :'json',
			data:paramData,
			success : function(data) {
				if(data == 0) {
					var getDataTable=art.dialog.data('getDataTable');
					getDataTable();
					art.dialog.close();
					art.dialog.tips('修改车辆成功！');
	            }else {
	            	art.dialog.tips('修改车辆失败！');
	            }
		}});
	})
	
	//取消
	$("#cancelCars").click(function(){
		 artDialog.close();
	});
	
})