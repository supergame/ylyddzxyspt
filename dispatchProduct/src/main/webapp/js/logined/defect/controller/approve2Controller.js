
var parent = art.dialog.parent;
var api = art.dialog.open.api;	

app.controller("approve2Controller",["$rootScope","$scope","defectService","$filter",function($rootScope,$scope,defectService,$filter){
	$scope.userId = "";
	$scope.userName = "";
	$scope.dialogInfo = art.dialog.data("dialogInfo");
	$scope.apply=art.dialog.data("apply");
	$scope.turnNum = art.dialog.data("turnNum");
	$scope.turnUser = art.dialog.data("turnUser");
	$scope.instanceId = art.dialog.data("instanceId");
	$scope.oper = art.dialog.data("oper");
	$scope.index = art.dialog.data("index");
	$scope.showSelectUserValue=2;
	$scope.flag=false;
	$scope.showType="";
	$scope.processType="";
	$scope.extension="";
	var equipmentMap = art.dialog.data("equipmentMap");
	$scope.getApplyUser = function(userId){
		defectService.getApplyUser({"userId":userId},function(result){
			if(result!=null&&result!=""){
				$("#userId").val(result.userId);
				$("#userName").val(result.userName);
			}
		});
	}
	
	if($scope.index==0){
		$scope.dialogInfo.userTitle = "转交部门";
		$scope.showType = 1;
		$scope.processType = 2;
		$scope.extension = "1,2";
	}
	if($scope.index==1){
		//$scope.getApplyUser($scope.apply.createUserId);
		$scope.dialogInfo.userTitle = "验收部门";
		$scope.showType = 1;
		$scope.processType = 2;
		$scope.userId = $scope.apply.createGroupId;
		$scope.userName = $scope.apply.createGroupName;
	}
	if($scope.index==9){
		$scope.dialogInfo.userTitle = "接收部门";
		$scope.showType = 1;
		$scope.processType = 2;
		$scope.extension = "1,2";
	}
	$(function(){
		if($scope.index==20){
			$("#evaluate").val(art.dialog.data("evaluate"));
			var achievement = art.dialog.data("achievement");
			$("input[type='radio'][name='achievement'][value='"+achievement+"']").prop("checked","checked");
		}
	})
	
	/**
	 * 获取useuser下拉树
	 */
	/*$scope.getUserSelect = function(){
		$scope.set1 = {
			isRadio: true,//true单选，false复选,默认true
			enable: true
		};
		defectService.getSelectUser({"showUser":3},function(result){
			$scope.data1 = result;
		});
	}*/
	//初始化人员树
	//$scope.getUserSelect();
	//选择人员
	/*$scope.toggleSelectUser = function (){
		if($scope.showSelectUserValue== 1){
			$scope.showSelectUserValue=2;
		}else {
			$scope.showSelectUserValue=1;
		}
		this.showSelectUserValue = $scope.showSelectUserValue;
	}*/
	/*if($scope.turnNum-$scope.apply.turnNum<=1&&$scope.index==0){
		$scope.flag=true;
		$scope.userId = $scope.turnUser.userId;
		$scope.userName = $scope.turnUser.userName;
	}*/
	if($scope.dialogInfo.userTitle=="归档人"){
        $scope.isShow=1;
	}else{
		$scope.isShow=0;
	}
	
	//处理结果提交
	$scope.saveApprove=function(){
		$scope.userId = $("#userId").val();
		$scope.processType = $("#processType").val();
		/*if($scope.dialogInfo.userTitle=="归档人"){
			$scope.processType=2;
			$scope.userId=61;
		}else{
			if($scope.dialogInfo.userTitle){
	   		 	if(!$scope.userId){
	   			 art.dialog.tips("请选择人员!");
	   			 return;
	   		 	}
		   	 }else{
		   		$scope.userId="";
		   	 }
		}*/
		if($scope.dialogInfo.userTitle){
   		 	if(!$scope.userId){
   			 art.dialog.tips("请选择部门/人员!");
   			 return;
   		 	}
	   	 }else{
	   		$scope.userId="";
	   	 }
		$scope.advice=$scope.dialogInfo.adviceDef;
		var achievement=$("input[name='achievement']:checked").val();
		var equipmentList = [];
		if($scope.index==1&&equipmentMap!=undefined){
			var size = equipmentMap.size();
			if(size>0){
				for(var i = 0;i<equipmentMap.size();i++){
					var arr = equipmentMap.arr;
					var tempEquip = arr[i].value;
					equipmentList.push(tempEquip.id);
				}
			}
		}
		if($scope.index==1 && equipmentList.length == 0 && $scope.apply.type == 1){
			art.dialog.alert("请先选择要配送的设备,再点击处理!");
			return false;
		}
		var params={
			instanceId:$scope.instanceId,
			taskName:$scope.dialogInfo.taskName,
			approveResult:$scope.dialogInfo.approveResult,
			advice:$scope.advice,
			nextUserId:$scope.userId,
			processType:$scope.processType,
			achievement:achievement,
			evaluate:$("#evaluate").val(),
			equipments:JSON.stringify(equipmentList)
		}
		//alert(JSON.stringify(params)+";;;"+JSON.stringify($scope.dialogInfo));
		//return;
		
		var type="approve";
		if($scope.dialogInfo.taskName==-1){
			type="dispatch";
		}else if($scope.dialogInfo.taskName==1){
			type="apply";
		}else if($scope.dialogInfo.taskName==2){
			type="approve";
		}else if($scope.dialogInfo.taskName==3||$scope.dialogInfo.taskName==6){
			type="defect";
		}else if($scope.dialogInfo.taskName==4){
			type="accept";
		}else if($scope.dialogInfo.taskName==5){
			type="archive";
		}
		defectService.approve(params,function(result){
			if(result&&result==1){
				art.dialog.tips("操作成功!");
				setTimeout(function(){
					art.dialog.data("result","success1");
					art.dialog.close();
				},1000);
			}else{
				art.dialog.alert("操作失败,请稍后重试!");
			}
		});
	}
	
	//处理结果提交
	$scope.updateEvaluate=function(){
		var achievement=$("input[name='achievement']:checked").val();
		var params={
				instanceId:$scope.instanceId,
				achievement:achievement,
				evaluate:$("#evaluate").val()
		}
		
		defectService.updateEvaluate(params,function(result){
			if(result&&result==1){
				art.dialog.tips("操作成功!");
				setTimeout(function(){
					art.dialog.data("result","success2");
					art.dialog.close();
				},1000);
			}else{
				art.dialog.alert("操作失败,请稍后重试!");
			}
		});
	}
	
	
	/**
	 * 初始化按钮
	 */
	$scope.initButton = function(){
		api.button(
				{
					name: '确定',
					callback: function () {
						if($scope.index==20){
							$scope.updateEvaluate();
						}else{
							$scope.saveApprove();
						}
						return false;
					},
					focus: true
				},
				{
					name: '取消',
					callback:function(){
						return true;
					}
				}
			);
	}
	
	$scope.initButton();
	
}]);