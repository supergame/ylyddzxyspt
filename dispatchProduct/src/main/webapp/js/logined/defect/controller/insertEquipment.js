var Capital= function(){
	this.id = "";
	this.name = "";
	this.capitalNo = "";
	this.capitalModel = "";
}

var Equipment = function(){
	this.id = "";
	this.name = "";
	this.capitalType=1;
	this.capitalNo="";
	this.capitalModel="";
};

var selectEquipments = {};

var equipmentMap = new Map();

/*选择设备*/
function insertEquipment(){
	var capitalType = 5;
	if(type==2){
		capitalType = 6;
	}
	var url = basePath+"logined/capital/selectCapital.jsp?type="+capitalType;
	art.dialog.data("capitalMap",equipmentMap);
	art.dialog.open(url,{
		title : "选择设备",
		width : 1200,
		height : 660,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		close:function(){
			equipmentMap = art.dialog.data("capitalMap");
			var size = equipmentMap.size();
			if(size>0){
				showEquipment();
			}
			/*var equipmentList = [];
			if(selectArr&&selectArr.length>0){
				for(var i = 0;i < selectArr.length;i++){
					var equipment = selectArr[i];
					equipmentMap.put(equipment.id+"",equipment);
				}
				showEquipment();
			}*/
			return true;
	   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
	});
}


function showEquipment(id){
	var size = equipmentMap.size();
	var html = "";
	if(size>0){
		var operType = $("#oper").val();
		for(var i = 0;i<equipmentMap.size();i++){
			var arr = equipmentMap.arr;
			var tempEquip = arr[i].value;
			html += '<tr><td width="25%">'+tempEquip.name+'</td><td width="25%">'+tempEquip.capitalNo+'</td><td width="25%">'+tempEquip.capitalModel+'</td>';
			if(operType=="defect"){
				html += '<td width="25%"><a href="javascript:;" onClick="deleteEquipment('+tempEquip.id+')" class="color_red">删除</a></td></tr>';
			}else{
				html += '<td width="25%">--</td></tr>';
			}
		}
	}else{
		html += '<tr><td colspan="4">您还没有添加设备</td></tr>';
	}
	if(id){
		$("#"+id).html(html);
	}else{
		$("#equipmentTable").html(html);
	}
}


function addUserCapital(){
	var instanceId=document.getElementById("instanceId").value;
	
}


function deleteEquipment(id){
	if(equipmentMap.get(id+"")){
		equipmentMap.remove(id+"");
	}
	showEquipment();
}

$(function(){
	showEquipment();
});