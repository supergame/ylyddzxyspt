app.controller("myWaitController",["$scope","defectService","$filter",function($scope,defectService,$filter){
	/**
	 * 获得数据字典
	 */
	var data={
			"infoType":"equipmentUnit",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.equipmentUnitList=result;
	});
	data={
			"infoType":"defectDepartment",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectDepartmentList=result;
	});
	data={
			"infoType":"defectProfessional",
			"sysTag":1	
	}
	defectService.getDicts(data,function(result){		
		$scope.defectProfessionalList=result;
	});
	/**
	 * defect 处理
	 * accept 验收
	 * archive 归档
	 */
	var type=document.getElementById("type").value;
	$scope.type = type;
	var isOut=document.getElementById("isOut").value;
	$scope.isOut = isOut;
	$scope.preOperTimeTitle = "上步操作时间";
	$scope.searchVo={};
	$scope.operType = "";
	if(type=="dispatch"){
		$scope.operType = "调度";
		$scope.title = "待调度";
		$scope.searchVo.taskName="'-1'";
	}else if(type=="approve"){
		$scope.title = "待接收";
		$scope.preOperTimeTitle = "上步操作时间";
		$scope.searchVo.taskName="'2'";
		$scope.operType = "接收";
	}else if(type=="defect"){
		$scope.preOperTimeTitle = "接收时间";
		$scope.title = "待处理";
		$scope.searchVo.taskName="'3'";
		$scope.operType = "处理";
	}else if(type=="accept"){
		$scope.preOperTimeTitle = "处理时间";
		$scope.title = "待验收";
		$scope.searchVo.taskName="'4'";
		$scope.operType = "验收";
	}else if(type=="archive"){
		$scope.preOperTimeTitle = "验收时间";
		$scope.title = "待归档";
		$scope.searchVo.taskName="'5'";
	}else if(type=="approveDelay"){
		$scope.preOperTimeTitle = "申请延期时间";
		$scope.title = "待批准";
		$scope.searchVo.taskName="'7'";
	}else if(type=="myJoin"){
		$scope.title = "待处理";
	}
	$scope.iDisplayLength = 15;
	$scope.iDisplayStart = 0;
	$scope.getList = function(){
		var num= $("#num").val();
		if(!num||num!=null){
			num='';
		}
		var startTime= $("#startTime").val();
		if(!startTime){
			startTime="";
		}
		var endTime= $("#endTime").val();
		if(!endTime){
			endTime="";
		}
		var myJoin = "";
		if(type=="myJoin"){
			myJoin = "1";
		}
		var params={
				"iDisplayLength":$scope.iDisplayLength,
				"iDisplayStart":$scope.iDisplayStart,
				"searchVo.equipmentUnit":$scope.searchVo.equipmentUnit,
				"searchVo.defectDepartment":$scope.searchVo.defectDepartment,
				"searchVo.defectProfessional":$scope.searchVo.defectProfessional,
				"searchVo.searchName":$scope.searchVo.searchName,
				"searchVo.type":$scope.searchVo.type,
				"taskName":$scope.searchVo.taskName,
				"num":num,
				"searchVo.startTime":startTime,
				"searchVo.endTime":endTime,
				"searchVo.myJoin":myJoin,
				"isOut":isOut
			}
		
		defectService.getMyWaitList(params,function(result){
			$scope.aaData = result.aaData;
			$scope.result = result.iTotalRecords;
		});
	}
	$scope.searchList=function(){
		$scope.iDisplayStart = 0;
		$scope.getList();
	}
	//查看详情
	$scope.goView=function(instanceId,state,type2){
		if((state==0||state==6)&&type=='defect'){
			type="approve";
		}
		if(type2==0){
			window.location.href=basePath+"logined/defect/view.jsp?oper="+type+"&instanceId="+instanceId+"&isOut="+$scope.isOut;
		}else{
			window.location.href=basePath+"logined/defect/view2.jsp?oper="+type+"&instanceId="+instanceId+"&isOut="+$scope.isOut;
		}
	}
	
	
	
	$scope.handleTask = function(apply,cclType){
		var theme="";
		var userTitle="";
		var adviceTitle="";
		var taskName="";
		var approveResult="";
		var adviceDef="";
		var index;
		if(type=="dispatch"){//调度
			index = 9;
		}
		if(type=="approve"){//接收
			index = 6;
		}
		if(type=="defect"){//处理
			index = 1;
		}
		if(type=="accept"){//验收
			index = 2;
		}
		if(index==1){
			taskName="3";
			approveResult="1";
			theme="处理完成";
			userTitle="验收人";
			adviceTitle="处理意见";
			adviceDef="已处理";
		}
		if(index==2){
			taskName="4";
			approveResult="1";
			$scope.adviceDef="已验收";
			theme="验收通过";
			adviceTitle="验收意见";
			adviceDef="已验收";
		}
		if(index==6){
			taskName="2";
			approveResult="1";
			theme="接收事件"
			userTitle="处理人";
			adviceTitle="接收意见";
			adviceDef="已接收";
		}
		if(index==9){
			taskName="0";
			approveResult="4";
			theme="调度事件"
			userTitle="接收人/部门";
			adviceTitle="调度意见";
			adviceDef="已调度";
		}
		
		$scope.openApprove(theme,userTitle,adviceTitle,taskName,approveResult,adviceDef,index,apply,'','',apply.instanceId,cclType);
	}
	
	
	
	$scope.turnTask = function(apply,cclType){
		var theme="";
		var userTitle="";
		var adviceTitle="";
		var taskName="";
		var approveResult="";
		var adviceDef="";
		var index = 0;
		if(index==0){
			taskName="2";
			approveResult="2";
			theme="转交事件";
			userTitle="转交人";
			adviceTitle="转交意见";
			adviceDef="已转交";
		}
		$scope.openApprove(theme,userTitle,adviceTitle,taskName,approveResult,adviceDef,index,apply,'','',apply.instanceId,cclType);
	}
	
	
	$scope.openApprove = function(theme,userTitle,adviceTitle,taskName,approveResult,adviceDef,index,apply,turnNum,turnUser,instanceId,cclType){
		var url = basePath+"logined/defect/approve.jsp";
		if(cclType!=0){
			url = basePath+"logined/defect/approve2.jsp";
		}
		var param = {"userTitle":userTitle,"adviceTitle":adviceTitle,"taskName":taskName,"approveResult":approveResult,"adviceDef":adviceDef};
		art.dialog.data("dialogInfo",param);
		art.dialog.data("apply",apply);
		art.dialog.data("turnNum",turnNum);
		art.dialog.data("turnUser",turnUser);
		art.dialog.data("instanceId",instanceId);
		art.dialog.data("oper",type);
		art.dialog.data("index",index);
		var type="approve";
		if(taskName==0){
			type="dispatch";
		}else if(taskName==1){
			type="apply";
		}else if(taskName==2){
			type="approve";
		}else if(taskName==3||taskName==6){
			type="defect";
		}else if(taskName==4){
			type="accept";
		}else if(taskName==5){
			type="archive";
		}
		art.dialog.open(url,{
			title : theme,
			width : 480,
			height : 220,
			lock : true,
			drag : true,
			opacity : 0.08,//透明度
			close:function(){
				if(art.dialog.data("result")=='success1'){
					window.location.href=basePath+"logined/defect/myWaitList.jsp?type="+type;
				}
				return true;
		   },button:[{name:"确定",focus:true}, {name:"取消", focus:false}]
		});
	}
	
	
}]);