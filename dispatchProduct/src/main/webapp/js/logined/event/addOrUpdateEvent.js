$(function(){
	chageTime(2);
	var eid=$("#eventId").val();
	$("#updateType").hide();
	if(eid){
		$("#updateType").show();
		findView();
	}
})
/**
 * 根据等级计算时间
 */
function chageTime(eventLevel){
	var time;
	if(eventLevel==1){
		time=addDate(1/12);
	}
	if(eventLevel==2){
		 time=addDate(1);
	}
	$("#defectTime").val(formatDateYYYYMMDDHHmm(time));
}

function addDate(addNums){
	var beginTime= new Date();
	var m = beginTime.valueOf();
	m+=addNums*24*60*60*1000;
	return new Date(m);
}

/**
 * 格式化时间
 */
function formatDateYYYYMMDDHHmm(d){
	var year = d.getFullYear();
	var month = d.getMonth()+1;
	var date = d.getDate();
	var hour = d.getHours();
	var minute = d.getMinutes();
	var second = d.getSeconds();
	if(month<10){
		month = "0"+month;
	}
	if(date<10){
		date = "0"+date;
	}
	if(hour<10){
		hour = "0"+hour;
	}
	if(minute<10){
		minute = "0"+minute;
	}
	return year+"-"+month+"-"+date+" "+hour+":"+minute+":00";
}



/**
 * 新增或修改事件
 */
function addOrUpdateEvent(){
	var eventId=$("#eventId").val()?$("#eventId").val():"";
	var eventName=$("#eventTitle").val()?$("#eventTitle").val():"";
	var parentId= $("#parentId").val()?$("#parentId").val():"";//维修部门
	var workHour=$("#workingHours").val()?$("#workingHours").val():"";
	var urgencyLevel=$("input[type='radio']:checked").val();
	var completeTime=$("#defectTime").val();
	var eventType=$("#eventType").val();
	if(eventType==0){
		art.dialog.alert("请选择具体分类！");
		return;
	}
	
	if(!eventName){
		art.dialog.alert("事件标题不能为空！");
		return;
	}
	if(!parentId){
		art.dialog.alert("维修部门不能为空！");
		return;
	}
	var param={
		   'event.id':eventId,
		   'event.eventName':eventName,
	       'event.groupId':parentId,
	       'event.workHour':workHour,
	       'event.urgencyLevel':urgencyLevel,
	       'event.completeTime':completeTime,
	       'event.eventType':eventType,
	}
	$.ajax({
		url:basePath+"event/event_addOrUpdate.action",
		type:'post',
		data:param,
		dataType:'json',
		success:function(result){
			if(result==0){
				art.dialog.alert("操作失败！");
			}else if(result==2){
				art.dialog.alert("事件标题不能重复！");
			}else{
				var grid = art.dialog.data("grid");
				grid();
				art.dialog.close();
			}
		}
	})
}
/**
 * 查询详情
 */
function findView(){
	var eventId=$("#eventId").val();
	$.ajax({
		url:basePath+"event/event_findView.action",
		type:'post',
		data:{"event.id":eventId},
		dataType:'json',
		success:function(result){
			$("#parentId").val(result.groupId);
			$("#eventType").val(result.eventType);
			getGroup();
			getEventType();
			$("#eventTitle").val(result.eventName);
			$("#workingHours").val(result.workHour);
			$("#defectTime").val(result.completeTime);
			var ridao = $("input[type='radio']");
			ridao.each(function(i,item){
				if(item.value==result.urgencyLevel){
					this.checked=true;
				}
			})
		}
	})
} 


/**
 * 得到部门
 */
function getEventType(){
    var parentId=$("#eventType").val();
	var zTree = $.fn.zTree.getZTreeObj("eventTypeTree");
	var nodes = zTree.transformToArray(zTree.getNodes());
	for (var i=0; i<nodes.length; i++) {
		var gName = nodes[i].name;
		var gId = nodes[i].id;
		if(gId=="gid_"+parentId){   
			zTree.selectNode(nodes[i]);
			if(parentId!=0){
				$("#groupType").val(gName);
			}
		}
	};
}


/**
 * 得到类型
 */
function getGroup(){
    var parentId=$("#parentId").val();
	var zTree = $.fn.zTree.getZTreeObj("groupTree");
	var nodes = zTree.transformToArray(zTree.getNodes());
	for (var i=0; i<nodes.length; i++) {
		var gName = nodes[i].name;
		var gId = nodes[i].id;
		if(gId=="gid_"+parentId){   
			zTree.selectNode(nodes[i]);
			if(parentId!=0){
				$("#groupSel").val(gName);
			}
		}
	};
}
