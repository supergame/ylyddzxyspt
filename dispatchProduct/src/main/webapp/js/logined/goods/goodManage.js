$(function(){
	var treeHeight = $(window).height()-38;
	$(".center_content").height(treeHeight);
	getGoodsGroupTree("gid_0");
	//加载存放位置下拉选
	// 捕获回车事件
    $("html").die().live("keydown", function(event) {
        if (event.keyCode == 13) {
        	_checkedIds="";
        	getTable();
        	
        }
    });
    getTable();
	
	// 头部全选复选框
	$("#goodTable").delegate("#total", "click", function(event) {
		checkTotal();
		event.stopPropagation();
	});
	// 第一列复选按钮
	$("#goodTable").delegate("input:checkbox[name='chk']", "click",
			function(event) {
				check();
				event.stopPropagation();
			});
	
	
	$("#serchList").click(function(){
		_checkedIds="";
		getTable();
	})
	
	
});




/**
 * 头部全选记录
 */
function checkTotal() {
	var isTotalChecked = $("input:checkbox[id='total']").prop("checked");
	var listCheckbox = $("input:checkbox[name='chk']");
	if (isTotalChecked) {
		listCheckbox.prop("checked", function(i, val) {
					if (!$(this).prop("disabled")) {
						return true;
					}
				});
	} else {
		listCheckbox.prop("checked", false);
	}
}

/**
 * 选择记录
 */
function check() {
	var checkTotalNum = $("input:checkbox[name='chk']");
	var checkNum = 0;
	var checkNumAll = true;
	checkTotalNum.each(function(i, val) {
				if ($(checkTotalNum[i]).prop("checked")) {
					checkNum++;
				} else {
					checkNumAll = false;
				}
			});
	if (!checkNumAll) {
		$("#total").prop("checked", false);
	} else {
		$("#total").prop("checked", true);
	}
}

/**
 * 删除人员
 */
function deleteGood() {
	// 获取选择管理员id
	var chkbox = document.getElementsByName("chk");
	var ids = _checkedIds;
	var selLen = _checkedIds.length;
	if (selLen <= 1) {
		qytx.app.dialog.alert("请至少选择一项");
		return;
	}
	var paramData = {
		'ids' : ids
	};
	// 删除管理员
	qytx.app.dialog.confirm('确定要删除吗？', function() {
		    qytx.app.ajax({
							url : basePath + "goods/deleteGoods.action",
							type : "post",
							data : paramData,
							success : function(data) {
								if (data == 1) {
										_checkedIds="";
										getTable();
								}else{qytx.app.dialog.alert("操作失败");}
							}
						});
			});
}




function toUpdateOrAdd(id){
	if(id){
		window.location.href = basePath + "logined/goods/goodAdd.jsp?goodId="+id;
	}else{
		window.location.href = basePath + "logined/goods/goodAdd.jsp";
	}
	return false;
}
/**
 * 弹出导入页面
 */
function toImport(){
	art.dialog.data("getTable",getTable);
	art.dialog.open(basePath +"logined/goods/importGood.jsp?",{
				id : 'import',
				title : '设备导入',
				lock :true,
				opacity: 0.08, 
				width : 600,
				height : 300,
				button : [{
					name : '导 入',
					callback : function() {
						var iframe = this.iframe.contentWindow;
						iframe.startAjaxFileUpload();
						return false;
					},
					focus:true
				},
				{
					name : '取 消',
					callback : function() {
						return true; 
					}
				}]
		});
}


/*

//搜索
function serch(){
	
}*/


function getTable(){
  var goodType=$("#goodType").val();	
  var goodNo=$("#goodNo").val();
  var goodName=$("#goodName").val();
  var groupId=$("#selectGroupId").val();
	$('#goodTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
						"name" : "name",
						"value" : goodName
					},{
						"name":"type",
						"value":goodType
					},{
						"name":"goodNo",
						"value":goodNo
					},{
						"name":"groupId",
						"value":groupId
					}
				);
		},
		"sAjaxSource" : basePath+"goods/findGoodListPage.action",// 获取管理员列表
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
				"mDataProp" : null
			     },{
					"mDataProp" : "no",
				}, {
					"mDataProp" : "name",
				    "sClass" : "longTxt"
				}, {
					"mDataProp" : "goodlNo",
					"sClass" : "longTxt"
				}, {
					"mDataProp" :"goodGroup"
				}, {
					"mDataProp" : "type"
				},{
					"mDataProp" : "specifications"
				},{
					"mDataProp" : "unit"
				},{
					"mDataProp" : "price"
				},{
					"mDataProp" : null
				}],
		"oLanguage" : {
			"sUrl" : basePath + "plugins/datatable/cn.txt" // 中文包
		},
		"fnDrawCallback" : function(oSettings) {
			$('#knowlegeTable tbody  tr td').each(function() {
				this.setAttribute('title', $(this).text());
			});
              
		},
		"fnInitComplete" : function() {
			// 重置iframe高度
		
		},
		"aoColumnDefs" : [{
			"aTargets" : [0],
			"fnRender" : function(oObj) {
				return '<input name="chk" value="'+ oObj.aData.id+'" type="checkbox" />';
			 }}, {
			"aTargets" : [9],
			"fnRender" : function(oObj) {
				var vid = oObj.aData.id;
				var html='<a href="javascript:void(0);" onclick="goodView('+vid+')">查看</a>';
				 html+='<a href="javascript:void(0);" onclick="toUpdateOrAdd('+vid+')">修改</a>';
				return html;
			}
			
		}]
	});
}	

	




	

/**
 * 查看资产详情
 */
function goodView(id) {
	var url = basePath+ "logined/goods/goodDetail.jsp";
	art.dialog.data("goodId", id);
	art.dialog.open(url, {
		title : '物资详情',
		width : 780,
		height :500,
		lock : true,
		opacity : 0.08,
		 button : [{
					name : '关闭',
					callback : function() {
						art.dialog.data("id", "");
						return true;
					}
				}]
	});

}


function getGoodsGroupTree(defaultSelectId){
	var url = basePath+"goods/selectGoodGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
			
		}
	});
}


function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	var type=$("#type").val();
	if(vid.substring(4)!="0"){
		$("#selectGroupId").val(vid.substring(4));
	}else{
		$("#selectGroupId").val("");
	}
	getTable();
}


/**
 * 弹出导入页面
 */
function toImport(){
	art.dialog.data("getTable",getTable);
	art.dialog.open(basePath +"logined/goods/importGood.jsp?selectGroupId="+selectGroupId,{
				id : 'import',
				title : '设备导入',
				lock :true,
				opacity: 0.08, 
				width : 600,
				height : 300,
				button : [{
					name : '导 入',
					callback : function() {
						var iframe = this.iframe.contentWindow;
						iframe.startAjaxFileUpload();
						return false;
					},
					focus:true
				},
				{
					name : '取 消',
					callback : function() {
						return true; 
					}
				}]
		});
}




