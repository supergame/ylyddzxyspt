$(document).ready(function() {
	// 获取设置信息
	queryAffairsManage();

	// 选择 允许事务提醒
	$("#myTable").delegate("input:checkbox[name='affairPvir']", "click", function() {
		if ($(this).attr("checked") == "checked") {
			if ($(":checkbox[name='affairPvir']:checked").length == $(":checkbox[name='affairPvir']").length) {
				$("#affairAll").attr("checked", "checked");
			}
		} else {
			$("#affairAll").removeAttr("checked");
		}
	});

	
	$("#myTable").delegate("input:checkbox[name='smsPvir']", "click", function() {
		if ($(this).attr("checked") == "checked") {
			if ($(":checkbox[name='smsPvir']:checked").length == $(":checkbox[name='smsPvir']").length) {
				$("#smsAll").attr("checked", "checked");
			}
		} else {
			$("#smsAll").removeAttr("checked");
		}
	});
	
	$("#myTable").delegate("input:checkbox[name='pushPvir']", "click", function() {
		if ($(this).attr("checked") == "checked") {
			if ($(":checkbox[name='pushPvir']:checked").length == $(":checkbox[name='pushPvir']").length) {
				$("#pushAll").attr("checked", "checked");
			}
		} else {
			$("#pushAll").removeAttr("checked");
		}
	});
	
	
});

function queryAffairsManage() {
	$('#myTable').dataTable(
			{
				"bDestroy" : true,
				"bProcessing" : false,
				'bServerSide' : true,
				'fnServerParams' : function(aoData) {
				},
				"sAjaxSource" : basePath + "affairSetting/getAffairSettingList.action",// 获取配置的事务提醒设置信息
				"sServerMethod" : "POST",
				"sPaginationType" : "full_numbers",
				"bPaginate" : false, // 翻页功能
				"bLengthChange" : false, // 改变每页显示数据数量
				"bFilter" : false, // 过滤功能
				"bSort" : false, // 排序功能
				"bInfo" : false,// 页脚信息
				"bAutoWidth" : false,// 自动宽度
				"iDisplayLength" : 20000, // 每页显示多少行
				"aoColumns" : [
				               {
				            	   "sTitle" : "序号",
				            	   "mDataProp" : "no"
				               },
				               {
				            	   "sTitle" : "模块名称",
				            	   "mDataProp" : "moduleName",
				            	   "sClass" : "longTxt"
				               },
				               {
				            	   "sTitle" : '<input name="" type="checkbox" value="" id="affairAll" onclick="selectAffairAll()" /> 在线消息',
				            	   "sWidth" : "100",
				            	   "mDataProp" : null
				               },
				               {
				            	   "sTitle" : '<input name="" type="checkbox" value="" id="smsAll" onclick="selectSmsAll()" /> 短信',
				            	   "sWidth" : "100",
				            	   "mDataProp" : null
				               },
				               {
				            	   "sTitle" : '<input name="" type="checkbox" value="" id="pushAll" onclick="selectPushAll()" /> 手机推送',
				            	   "sWidth" : "100",
				            	   "mDataProp" : null
				               }],
				               "oLanguage" : {
				            	   "sUrl" : basePath + "plugins/datatable/cn.txt" // 中文包
				               },
				               "fnDrawCallback" : function(oSettings) {
				            	   $('#myTable tbody  tr td[class="longTxt"]').each(function() {
			            				this.setAttribute('title', $(this).text());
			            			});
				            	   if ($(":checkbox[name='affairPvir']:checked").length == $(":checkbox[name='affairPvir']").length && $(":checkbox[name='affairPvir']").length > 0) {
				            		   $("#affairAll").attr("checked", "checked");
				            	   }
				            	   if ($(":checkbox[name='smsPvir']:checked").length == $(":checkbox[name='smsPvir']").length
				            			   && $(":checkbox[name='smsPvir']").length > 0) {
				            		   $("#smsAll").attr("checked", "checked");
				            	   }
				            	   if ($(":checkbox[name='pushPvir']:checked").length == $(":checkbox[name='pushPvir']").length
				            			   && $(":checkbox[name='pushPvir']").length > 0) {
				            		   $("#pushAll").attr("checked", "checked");
				            	   }
				               },
				               "fnInitComplete" : function() {
				               },
				               "aoColumnDefs" : [
				                                 {
				                                	 "aTargets" : [ 2 ],// 覆盖第2列
				                                	 "fnRender" : function(oObj) {
				                                		 // 是否允许事务提醒
				                                		 var affairPriv = oObj.aData.affairPriv;
				                                		 var id = oObj.aData.id;
				                                		 if (null != affairPriv && "" != affairPriv) {
				                                			 if (affairPriv == '1') {
				                                				 return '<input name="affairPvir" type="checkbox" checked="checked" value="'+id+'"/>';
				                                			 }
				                                		 }
				                                		 return '<input name="affairPvir" type="checkbox" value="'+id+'"/>';
				                                	 }
				                                 },
				                                 {
				                                	 "aTargets" : [ 3 ],// 覆盖第3列
				                                	 "fnRender" : function(oObj) {
				                                		 // 是否发送短信提醒
				                                		 var smsPriv = oObj.aData.smsPriv;
				                                		 var id = oObj.aData.id;
				                                		 if (null != smsPriv && "" != smsPriv) {
				                                			
				                                			 if (smsPriv == '1') {
				                                				 return '<input name="smsPvir" type="checkbox" checked="checked" value="1"/>';
				                                			 }
				                                		 }
				                                		 return '<input name="smsPvir" type="checkbox" value="0"/>';
				                                	 }
				                                 },
				                                 {
				                                	 "aTargets" : [ 4 ],// 覆盖第5列
				                                	 "fnRender" : function(oObj) {
				                                		 // 是否手机推送提醒
				                                		 var pushPriv = oObj.aData.pushPriv;
				                                		 var id = oObj.aData.id;
				                                		 if (null != pushPriv && "" != pushPriv) {
				                                			
				                                			 if (pushPriv == '1') {
				                                				 return '<input name="pushPvir" type="checkbox" checked="checked" value="1"/>';
				                                			 }
				                                		 }
				                                		 return '<input name="pushPvir" type="checkbox" value="0"/>';
				                                	 }
				                                 } ]
			});
}

function selectAffairAll() {
	var isTotalCheck = $("input:checkbox[id='affairAll']").prop("checked");
	var checkNum = 0;
	if (isTotalCheck) {
		$("input:checkbox[name='affairPvir']").prop("checked", function(i, val) {
			checkNum = checkNum + 1;
			return true;
		});
	} else {
		$("input:checkbox[name='affairPvir']").prop("checked", false);
	}
}

function selectSmsAll() {
	var isTotalCheck = $("input:checkbox[id='smsAll']").prop("checked");
	var checkNum = 0;
	if (isTotalCheck) {
		$("input:checkbox[name='smsPvir']").prop("checked", function(i, val) {
			checkNum = checkNum + 1;
			return true;
		});
	} else {
		$("input:checkbox[name='smsPvir']").prop("checked", false);
	}
}

function selectPushAll() {
	var isTotalCheck = $("input:checkbox[id='pushAll']").prop("checked");
	var checkNum = 0;
	if (isTotalCheck) {
		$("input:checkbox[name='pushPvir']").prop("checked", function(i, val) {
			checkNum = checkNum + 1;
			return true;
		});
	} else {
		$("input:checkbox[name='pushPvir']").prop("checked", false);
	}
}

/**
 * 保存配置信息
 */
function updateAffairManage(id, groupName) {
	art.dialog.confirm("您确定要修改设置信息吗？",function(){
		// 获取 允许事务提醒
		var sb = "";
		$.each( $("#myTable tbody  tr"), function(index, tr){
			sb+=$(tr).children().eq(2).children().val() + ",";
			if($(tr).children().eq(2).children().attr("checked")){
				sb+="1";
			}else{
				sb+="0";
			}
			sb+=",";
			if($(tr).children().eq(3).children().attr("checked")){
				sb+="1";
			}else{
				sb+="0";
			}
			sb+=",";
			if($(tr).children().eq(4).children().attr("checked")){
				sb+="1";
			}else{
				sb+="0";
			}
			sb+="_";
		});

		var paramData={
				'settings':sb
		};
		$.ajax({
			url : basePath + "/affairSetting/update.action",
			type : "post",
			data: paramData,
			dataType : "text",
			 beforeSend:function(){
	             $("body").lock();
	         },
	         complete:function(){
	             $("body").unlock();
	         },
			success : function(data) {
				if ("0" == data) {
					art.dialog({
						   title:"消息",
						   content:sprintf("affairs.msg_configuration_success"),
						   width : 317,
						   height : 109,
						   icon:"succeed",
						   opacity:0.08,
						   lock:true,
						   ok:function(){},
						   close:function(){
							   queryAffairsManage();
						   }
						});
				} else {
					art.dialog.alert(sprintf("affairs.msg_configuration_error"));
				}
			},
			 error:function(){
	             art.dialog.alert("系统异常，请稍后再试");
	         }
		});
	},function(){});
	
}