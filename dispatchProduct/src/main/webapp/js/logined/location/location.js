$(function(){
	
	getLocationTree("gid_0");
	$("#id").val("");
	
});


function getLocationTree(defaultSelectId){
	var url = basePath+"location/location_capitalGroupTreeList.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
			
		}
	});
}

function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	if(vid!="gid_0"){//修改地点
		$("#operationType").val(2);
		$("#operationTitle").html("修改地点");
		$("#formPage").show();
		$("#noData").hide();
		$("#locationName").val(name);
		$("#id").val(vid.substring(4));
		if(index_code!=null&&index_code!=""){
			$("#orderIndex").val(index_code.split("{1}")[0]);
		}
	}else{
		$("#id").val("");
		$("#formPage").hide();
		$("#noData").show();
	}
}


//新增
function addLocation(){
	var id = $("#id").val();
	if(id!=undefined){
		$("#operationType").val(1);
		$("#operationTitle").html("新增地点");
		$("#orderIndex").val("");
		$("#locationName").val("");
		$("#orderIndex").val("");
		$("#formPage").show();
		$("#noData").hide();
	}else{
		art.dialog.alert("请选择想要添加到的位置!");
		$("#formPage").hide();
		$("#noData").show();
	}
}
//删除
function delLocation(){
	var groupId = $("#id").val();
	if(groupId!=undefined&&groupId!=""){
		art.dialog.confirm("确定要删除吗?",function(){
			param = {"location.id":groupId};
			$.ajax({
				url:basePath+"location/location_deleteLocation.action",
				type:"POST",
				data:param,
				dataType:"text",
				success:function(result){
					if(result=="0"){
						art.dialog.tips("删除成功!");
						$("#id").val("");
						$(".formPage").hide();
						$("#noData").show();
						getLocationTree("");
					}else if(result=="1"){
						art.dialog.alert("该地点下存在子地点,不能删除!");
					}else{
						art.dialog.alert("操作失败,请稍后重试!");
					}
				}
			});
		});
	}else{
		art.dialog.alert("请选择要删除的地点!");
	}
};
function saveOrUpdate(){
	var location = {};
	var param;
	var locationName = $("#locationName").val();
	var orderIndex = $("#orderIndex").val();
	var operationType = $("#operationType").val();
	var id = $("#id").val();
	if(!locationName){
		art.dialog.alert("地点名称不能为空!");
		return false;
	}
	if(!orderIndex){
		art.dialog.alert("排序号不能为空!");
		return false;
	}
	
	if(operationType==1){//添加
		if(id==""){
			id = 0;
		}
		param = {
				"location.parentId":id,
				"location.orderIndex":orderIndex,
				"location.locationName":locationName
		}
	}else{
		param = {
				"location.id":id,
				"location.orderIndex":orderIndex,
				"location.locationName":locationName
		}
	}

	$.ajax({
		url:basePath+"location/location_saveOrUpdateLocation.action",
		type:"POST",
		data:param,
		dataType:"text",
		success:function(result){
			if(result!=null&&result.indexOf("0_")>-1){
				art.dialog.tips("添加成功!");
				getLocationTree("gid_"+result.split("_")[1]);
			}else if(result=="1"){
				art.dialog.tips("修改成功!");
				getLocationTree("gid_"+id);
			}else if(result=="2"){
				art.dialog.alert("同层地点名称不能重复!");
			}else{
				art.dialog.alert("操作失败,请稍后重试!");
			}
		}
	});
};
