$(function(){
	
	
	getTable();
	$(document).keyup(function(event){
		  if(event.keyCode ==13){
			  getTable();
		  }
		});
	
});


function searchList(){
	getTable();
}

//导出
function export_list(){
	var businessType=$("#businessType").val();
	var status=$("#status").val();
	var company=$("#company").val();
	var createTime_begin=$("#startTime").val();
	var createTime_end=$("#endTime").val();
    var url = basePath + "cardispatch/cardispatch_exportList.action?carOrderVo.orderType=1&carOrderVo.businessType="+businessType+"&carOrderVo.status="+status+"&carOrderVo.company="+company+"&carOrderVo.createTime_begin="+createTime_begin+"&carOrderVo.createTime_end="+createTime_end;	
	url=encodeURI(encodeURI(url));
	window.open(url);
}





/**
 * 获得医联体工单列表
 */
function getTable(){
	var businessType=$("#businessType").val();
	var status=$("#status").val();
	var company=$("#company").val();
	var createTime_begin=$("#startTime").val();
	var createTime_end=$("#endTime").val();
	$('#myTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
				"name" : "carOrderVo.orderType",
				"value" : 1
			},{
				"name" : "carOrderVo.businessType",
				"value" : businessType
			},{
				"name" : "carOrderVo.status",
				"value" : status
			},{
				"name" : "carOrderVo.company",
				"value" : company
			},{
				"name" : "carOrderVo.createTime_begin",
				"value" : createTime_begin
			},{
				"name" : "carOrderVo.createTime_end",
				"value" : createTime_end
			});
		},
		"sAjaxSource" : basePath + "cardispatch/cardispatch_findConjoinedOrder.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bStateSave" : false, // 状态保存
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,//用于当要在同一个元素上履行新的dataTable绑订时，将之前的那个数据对象清除掉，换以新的对象设置
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [{
					"mDataProp" : "no", //序号
				}, {
					"mDataProp" : "instanceId" //工单编号
				}, {
					"mDataProp" : "businessType", //派遣类型
				}, {
					"mDataProp" : "patientName" //患者姓名
				}, {
					"mDataProp" : "company" //转诊单位
				}, {
					"mDataProp" : "createTime" //登记时间
				}, {
					"mDataProp" : "stateStr" //当前状态
				}, {
					"mDataProp" : null //当前状态
				}],
		"oLanguage": {
			   "sUrl": basePath+"plugins/datatable/cn.txt" //中文包
		   },
		"fnDrawCallback": function (oSettings) {
			   $('#myTable tbody  tr td,#myTable tbody  tr td a').each(function() {
					this.setAttribute('title', $(this).text());
			   });
		   },
		"aoColumnDefs" : [{
			"aTargets" : [7], //操作
			"fnRender" : function(oObj) {
				var status = oObj.aData.status;
				var vid=oObj.aData.instanceId;
				var confirmFactory =oObj.aData.confirmFactory;
				var html= '<a href="javascript:void(0);" onclick="showView('+vid+');">查看</a> ';
				if(status>1 && confirmFactory==0){
					html+='<a href="javascript:void(0);" onclick="confirm('+vid+');">确认到科</a> ';
				}
				return html;
			}
		}]
	}); 
}

/**
 * 跳转详情页面
 */
function showView(instanceId){
	window.location.href = basePath + "logined/carDispatch/view.jsp?viewType=1&instanceId=" + instanceId;
}


/**
 * 确认到厂
 * @returns
 */
function confirm(instanceId){
	art.dialog.confirm("是否要确认到场", function () {
		$.ajax({
			url:basePath+"cardispatch/approve.action",
		    data:{"instanceId":instanceId,"taskName":5},
		    type:'post',
		    dataType:'text',
		    success:function(data){
		    	if(data==1){
		    		art.dialog.tips("操作成功!");
		    		getTable();
		    	}else{
		    		art.dialog.alert("操作失败,请稍候重试!");
		    	}
		    }
		 })
        
    })
}

