$(document).ready(function() {
	showView();
});

function showView(){
	var paramData = {
			'instanceId':$("#instanceId").val()
		};
		$.ajax({
			url : basePath + "cardispatch/cardispatch_carDispatchView.action",
			type : "post",
			dataType :'json',
			data:paramData,
			success : function(data) {
				if(data != "") {
					if($.trim(data.orderType) == "医联体工单"){
						$("#yltView").show();
						$("#carDispatchView").hide();
					}else{
						$("#yltView").hide();
						$("#carDispatchView").show();
					}
					fillData(data);
					//流程处理
					var html="";
					if(data.historyList.length>0){
						for(var i=0;i<data.historyList.length;i++){
							html+="<li><div class='info-box'><p class='title'><span class='time'>"+data.historyList[i].endTimeStr+"</span>";
							html+="<span class='style name-number'>"+data.historyList[i].processerName+"【"+data.historyList[i].workNo+"】"+"</span>" ;
							html+="<span class='style'>"+data.historyList[i].statusName+"</span>" ;
							if(data.historyList[i].statusName=="已派遣"){
								var carNo="--";
								var carTypeStr="--";
								if(data.historyList[i].carNo){
									carNo=data.historyList[i].carNo;
								};
								if(data.historyList[i].carTypeStr){
									carTypeStr=data.historyList[i].carTypeStr;
								}
								html+="<span class='style'> 车牌号："+carNo+"("+data.historyList[i].carPhone+")</span>" ;
								html+="<span class='style'>车辆类型："+carTypeStr+"</span>" ;
								html+="<span class='style'>驾驶人："+data.historyList[i].driverName+"【"+data.historyList[i].driverWorkNo+"】</span>" ;
								html+="<span class='style'>驾驶人电话："+(data.historyList[i].driverPhone?data.historyList[i].driverPhone:"--")+"</span>" ;
							}
							html+="</p></div></li>";
						}
						
					}
					$("#history").html(html);
				}
			}
		});
}

function fillData(data) {
//	alert(arr)
//	alert(JSON.stringify(data));
    //根据传入的id显示输入
//    for(var i = 0;i<data.length;i++){
//    	$("#"+data[i]).html(data[arr[i]]);
//    	
//    }
    for(var key in data){
    	$("#"+key).html(data[key]);
   }

}

function backToList(){
	var viewType=$("#viewType").val();
	if(viewType==1){//返回医联体工单列表
		window.location.href = basePath + "logined/carDispatch/conjoined/conjoined.jsp";
	}else{
		window.location.href = basePath + "logined/carDispatch/carDispatch.jsp?currentTab=" + $("#currentTab").val();
	}
}