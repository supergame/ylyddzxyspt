$(document).ready(function() {
	//点击保存按钮
	$("#saveCar").click(function(){
		saveCar();
//	     return false;
	});
	//单击取消按钮
	$("#cancelCar").click(function(){
		 artDialog.close();
//	     return false;
	});
});
//点击保存
function saveCar() {
	//车牌号
	var carNo=$.trim($("#carNo").val());
	if (carNo == "") {
		art.dialog.alert('请输入车牌号');
		return;
	}
	//手机号
    var carPhone=$.trim($("#carPhone").val());
    if(carPhone){
    	if(!(/^1[34578]\d{9}$/.test(carPhone))){ 
    		art.dialog.alert("手机号码有误，请重填");  
            return; 
        } 
    }
    //车辆类别
    var carType = $('input:radio[name="carType"]:checked').val();
    //是否有担架
    var havaStretcher = $('input:radio[name="havaStretcher"]:checked').val();
    //车辆座位数
    var seatNum=$.trim($("#seatNum").val());
    var paramData = {
	    'carVo.carNo':carNo,
		//'carVo.carBrand':carBrand,
		'carVo.carPhone':carPhone,
		'carVo.carType':carType,
		'carVo.havaStretcher':havaStretcher,
		'carVo.seatNum':seatNum
	};
    $.ajax({
		url : basePath+"cars/addCar.action",
		type : "post",
		dataType :'json',
		data:paramData,
		success : function(data) {
			if(data == 0) {
				var getDataTable=art.dialog.data('getDataTable');
				getDataTable();
				art.dialog.close();
				art.dialog.tips('保存车辆成功!',3000);
            }else if(data == 1){
            	art.dialog.alert("车辆号重复!");
            } 
			else {
            	art.dialog.alert('保存车辆失败！');
            }
	}});
}