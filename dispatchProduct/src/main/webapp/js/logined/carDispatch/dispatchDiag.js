var parent = art.dialog.parent;
var api = art.dialog.open.api;
var carNo ;
var carBrand;
var carPhone ;
var carType ;
var carStatus;
var carId;
var driverName;
var driverPhone;
var driverId;
var driverStatus;
$(document).ready(function() {
	var carType=$("#carTypes").val();
	var radio=$("input[name='carType']");
	for(var i=0;i<radio.length;i++){
		if(radio[i].value==carType){
			radio[i].checked=true;
            break;
		}
	}
	initButton();
});

//司机列表
function openDriverList(){
//	art.dialog.data("instanceId",instanceId);
	var url=basePath+"logined/carDispatch/driverList.jsp";
	var artWindow=art.dialog.open(url,{
		title : "司机选择",
		width : 800,
		height : 500,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		button : [{
			name : '确定',
			focus:true,
			callback : function() {
				var iframe = this.iframe.contentWindow;
				var driver=iframe.toParentParams();
				driverName = driver.userName;
				driverPhone = driver.phone;
				driverId = driver.userId;
				driverStatus = driver.status;
				//回显
				$("#driverName").val(driverName);
				//$("#phone").val(driverPhone);
				return true;
			}
		}, {
			name : '取消',
			callback : function() {
				return true;
			}
		}]
	});
  art.dialog.data("artWindow",artWindow);
}

/**
 * 初始化按钮
 */
function initButton(){
	api.button(
			{
				name: '确定',
				callback: function () {
					 dispatch();
					return false ;
				}
			},
			{
				name: '取消',
				callback:function(){
					return true;
				}
			}
		);
}

//车辆列表
function openCarList(){
	var carType=$("input[name='carType']:checked").val();
	var url=basePath+"logined/carDispatch/carList.jsp?carType="+carType;
	var artWindow=art.dialog.open(url,{
		title : "车辆选择",
		width : 800,
		height : 440,
		lock : true,
		drag : true,
		opacity : 0.08,//透明度
		button : [{
			name : '确定',
			focus:true,
			callback : function() {
				var iframe = this.iframe.contentWindow;
				var car=iframe.toParentParams();
				carNo = car.carNo;
				carBrand = car.carBrand;
				carPhone = car.carPhone;
				carType = car.carType;
				carStatus = car.status;
				carId = car.id;
				if(carPhone && carPhone!='--'){
					$("#phone").val(carPhone);
				}else{
					$("#phone").val("");
				}
				$("#carNo").val(carNo);
				 //$("#carType option[value='"+carType+"']").attr("selected","selected");
				var radio=$("input[name='carType']");
				for(var i=0;i<radio.length;i++){
					if(radio[i].value==carType){
						radio[i].checked=true;
			            break;
					}
				}	
				return true;
			}
		}, {
			name : '取消',
			callback : function() {
				return true;
			}
		}]
	});
}

var isSbumit=0;

/**
 * 派遣
 */
function dispatch(){
	if(isSbumit==1){
		return;
	}
	isSbumit=1;
	if(!carId){
		art.dialog.alert("请选择车辆");
		isSbumit=0;
		return;
	}
	if(!driverId){
		art.dialog.alert("驾驶员不存在");
		isSbumit=0;
		return;
	}
	var paramData = {
			'instanceId':$("#instanceId").val(),
			'taskName':1,
			'nextUserId':driverId,
			'driver.userId':driverId,
			'driver.userName':driverName,
			'driver.phone':driverPhone,
//			'driver.status':driverStatus,
			'car.id':carId,
			'car.carNo':carNo,
			'car.status':carStatus,
			'car.carType':carType,
			'car.carBrand':carBrand,
			'car.carPhone':carPhone
		};
		$.ajax({
			url : basePath + "cardispatch/approve.action",
			type : "post",
			dataType :'text',
			data:paramData,
			success : function(data) {
				var waitDispatchList=art.dialog.data("waitDispatchList");
				if(data == 1) {
					isSbumit=0;
					art.dialog.tips("派遣成功",1);
					waitDispatchList();
					art.dialog.close();
				} else{
					isSbumit=0;
					art.dialog.close();
					waitDispatchList();
					art.dialog.alert(data);
				}
			}
		});
}
/**
 * 获得司机
 */
function findDriver(){
	var driverName =$("#driverName").val();
	var param={
			"serachKey":driverName
	};
	$.ajax({
		url : basePath + "cardispatch/cardispatch_findDriverNameList.action",
		type : "post",
		dataType :'json',
		data:param,
		success : function(data) {
			if(data){
				$("#driverUl").show();
				var html="";
				for(var i=0;i<data.length;i++){
					html+="<li style='height: 25px;line-height: 22px;padding:0 5px;cursor: pointer' class='selectLocation'selectPhone=\""+data[i].phone+"\" selectUserName=\""+data[i].userName+"\" selectUserId="+data[i].userId+"  onclick='selectDriver(this)'  >" ;
                    html+="<a  href='javascript:;'style='color: #333;font-size: 14px;'  >"+data[i].userName+"</a></li>"
				}
				$("#driverUl").html(html);
				
			}else{
				$("#driverUl").hide();
			}
		}
	})
}






function selectDriver(obj){
	var userName=$(obj).attr("selectUserName");
	var userId=$(obj).attr("selectUserId");
	var phone =$(obj).attr("selectPhone");
	$("#driverName").val(userName);
	driverId=userId;
	driverName=userName;
	if(phone){
		driverPhone=phone;
	}
	$("#driverUl").hide();
}
