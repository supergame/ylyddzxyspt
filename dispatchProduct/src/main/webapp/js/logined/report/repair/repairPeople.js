$(document).ready(function() {
	    findRepairPeople();
	//查询列表
	$("#search_repairPeople").click(function(){
		findRepairPeople();
		
	});
	//导出
	$("#report_repairPeople").click(
			function(){
				report_repairPeople();
			}
	);
});
//导出
function report_repairPeople(){
	var beginTime=$("#begDate_repairPeople").val()?$("#begDate_repairPeople").val():"";
	var endTime=$("#endDate_repairPeople").val()?$("#endDate_repairPeople").val():"";
	var userName=$("#employee").val()?$("#employee").val():"";
	var departmentName=$("#department").val()?$("#department").val():"";
    var url = basePath + "report/repairPeople_exportPeopleWork.action?beginTime="+beginTime+"&endTime="+endTime+"&userName="+userName+"&departmentName="+departmentName;	
	url=encodeURI(encodeURI(url));
	window.open(url);
}

/**
 * 
 * 
 */
function findRepairPeople(){
	var param={
			'beginTime':$("#begDate_repairPeople").val(),
            'endTime':$("#endDate_repairPeople").val(),
            'userName':$("#employee").val(),
            'departmentName':$("#department").val()
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/repairPeople_findRepairPeopleList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
				var html="";
				if(data.length>0){
					for(var i=0;i<data.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data[i].no+"</td>" ;
						html+="<td>"+data[i].name+"</td>";
						html+="<td>"+data[i].departmentName+"</td>";
						html+="<td>"+data[i].receiveNum+"</td>";
						html+="<td>"+data[i].finishNum+"</td>";
						html+="<td>"+(data[i].noFinishNum<0?0:data[i].noFinishNum)+"</td>";
						html+="<td>"+data[i].longTimeSum+"</td>";
						html+="<td>"+data[i].avgTime+"</td>";
						html+="<td>"+data[i].workTimeSum+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='9'>暂无数据</td></tr>";
				}
				$("#repairPeopleList").html(html);
		}
	});
}


