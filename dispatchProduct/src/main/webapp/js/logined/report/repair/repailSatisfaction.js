$(document).ready(function() {
	findRepairSatisfaction(1);
	//查询列表
	$("#search_Satisfaction").click(function(){
		if($(".IllegalsNum").hasClass("hide")){
			findRepairSatisfaction(1);
		}else{
			findRepairSatisfaction(2);
		}
	});
	//导出
	$("#report_Satisfaction").click(
			function(){
				report_Satisfaction();
			}
	);
	//查看视图
	$("#showView_Satisfaction").click(
			function(){
				if($("#showList_Satisfaction").hasClass("hide")){
					$(".list_IllegalsNum").addClass("hide");
                    $(".IllegalsNum").removeClass("hide");
					$("#showList_Satisfaction").removeClass("hide");
					$("#showView_Satisfaction").addClass("hide");
					findRepairSatisfaction(2);
				}
			});
	//查看列表
	$("#showList_Satisfaction").click(
			function(){
				if($("#showView_Satisfaction").hasClass("hide")){
					$(".IllegalsNum").addClass("hide");
					$("#showList_Satisfaction").addClass("hide");
					$(".list_IllegalsNum").removeClass("hide");
					$("#showView_Satisfaction").removeClass("hide");
					findRepairSatisfaction(1);
				}
});
    
});
//导出
function report_Satisfaction(){
	var beginTime=$("#begDate_Satisfaction").val();
	var endTime=$("#endDate_Satisfaction").val();
	var employeeName=$("#employeeName").val();
    var url = basePath + "report/repairSatisfied_exportRepairSatisfied.action?beginTime="+beginTime+"&endTime="+endTime+"&employeeName="+employeeName;	
	url=encodeURI(encodeURI(url));
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findRepairSatisfaction(type){
	var param={
			'beginTime':$("#begDate_Satisfaction").val(),
            'endTime':$("#endDate_Satisfaction").val(),
            'employeeName':$("#employeeName").val()
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/repairSatisfied_findRepairSatisfiedList.action",
		data : param,
		dataType : 'json',
		success : function(data) {
			if(type==1){
				var html="";
				if(data.aData.length>0){
					for(var i=0;i<data.aData.length;i++){
						if(i%2==0){
							html+="<tr class='odd'>";	
						}else{
							html+="<tr class='even'>";
						}
						html+="<td>"+data.aData[i].no+"</td>" ;
						html+="<td>"+data.aData[i].name+"</td>";
						html+="<td>"+data.aData[i].departmentName+"</td>";
						/*html+="<td>"+data.aData[i].verySatisfied+"</td>";*/
						html+="<td>"+data.aData[i].satisfied+"</td>";
						/*html+="<td>"+data.aData[i].kind+"</td>";*/
						html+="<td>"+data.aData[i].dissatisfied+"</td>";
						html+="</tr>";
					}
				}else{
					html ="<tr><td colspan='5'>暂无数据</td></tr>";
				}
				$("#SatisfactionList").html(html);
			}else{
				eachartSatisfaction(data);
			}
		}
	});
}
//加载柱状图
function eachartSatisfaction(data){
	 var myChart = echarts.init(document.getElementById("main_Satisfaction"));
	option = {
			tooltip : {
		        trigger: 'axis',
		        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		        }
		    },
		    legend: {
		        data:data.typeName
		    },
		    grid: { 
		        x: 30,
		        x2: 10,
		        y2: 80,
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : data.nameArray
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : [
		        {
		            name:'非常满意',
		            type:'bar',
		            data:data.verySatisfiedArray
		        },
		        {
		            name:'满意',
		            type:'bar',
		            data:data.satisfiedArray
		        },{
		            name:'一般',
		            type:'bar',
		            data:data.kindArray
		        },{
		            name:'不满意',
		            type:'bar',
		            data:data.dissatisfiedArray
		        }
		    ]
		};
	
	 myChart.setOption(option);
}

