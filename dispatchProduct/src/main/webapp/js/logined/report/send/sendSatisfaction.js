$(document).ready(function() {
	findRepairSatisfaction();
	//查询列表
	$("#search_Satisfaction").click(function(){
		if($(".IllegalsNum").hasClass("hide")){
			findRepairSatisfaction();
		}else{
			findChartSatisfaction();
		}
	});
	//导出
	$("#report_Satisfaction").click(
			function(){
				report_Satisfaction();
			}
	);
	//查看视图
	$("#showView_Satisfaction").click(
			function(){
				if($("#showList_Satisfaction").hasClass("hide")){
					$(".list_IllegalsNum").addClass("hide");
                    $(".IllegalsNum").removeClass("hide");
					$("#showList_Satisfaction").removeClass("hide");
					$("#showView_Satisfaction").addClass("hide");
					findChartSatisfaction();
				}
			});
	//查看列表
	$("#showList_Satisfaction").click(
			function(){
				if($("#showView_Satisfaction").hasClass("hide")){
					$(".IllegalsNum").addClass("hide");
					$("#showList_Satisfaction").addClass("hide");
					$(".list_IllegalsNum").removeClass("hide");
					$("#showView_Satisfaction").removeClass("hide");
					findRepairSatisfaction();
				}
});
    
});
//导出
function report_Satisfaction(){
	var beginTime=$("#begDate_Satisfaction").val();
	var endTime=$("#endDate_Satisfaction").val();
	var employeeName=$("#employeeName").val();
    var url = basePath + "report/export.action?beginTime="+beginTime+"&endTime="+endTime+"&userName="+employeeName;
    url=encodeURI(encodeURI(url));
	window.open(url);
}

/**
 * type:1 列表 2 视图
 * 获得视图
 */
function findRepairSatisfaction(){
	var param={
			'beginTime':$("#begDate_Satisfaction").val(),
            'endTime':$("#endDate_Satisfaction").val(),
            'userName':$("#employeeName").val(),
            'type':1
	}
	$.ajax({
		type : 'post',
		url : basePath + "report/satis_findSatisfaction.action",
		data : param,
		dataType : 'json',
		success : function(data) {
			var html="";
			if(data.length>0){
				for(var i=0;i<data.length;i++){
					if(i%2==0){
						html+="<tr class='odd'>";	
					}else{
						html+="<tr class='even'>";
					}
					html+="<td>"+data[i].no+"</td>" ;
                    html+="<td>"+data[i].name+"</td>";
                    html+="<td>"+data[i].groupName+"</td>";
                    html+="<td>"+data[i].my+"</td>";
                    if(data[i].no!="合计"){
                    	html+="<td><a onClick='bmyList("+data[i].userId+")' style='cursor:pointer;text-decoration:none'>"+data[i].bmy+"</a></td>";
                    }else{
                    	html+="<td>"+data[i].bmy+"</td>";
                    }
                    html+="</tr>";
				}
			}else{
				html ="<tr><td colspan='5'>暂无数据</td></tr>";
			}
			$("#SatisfactionList").html(html);
		}
	});
}
/**
 * 获得视图
 */
function findChartSatisfaction(){
    var param={
        'beginTime':$("#begDate_Satisfaction").val(),
        'endTime':$("#endDate_Satisfaction").val(),
        "type":1,
        'userName':$("#employeeName").val()
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/satis_findStaffChart.action",
        data : param,
        dataType : 'json',
        success : function(data) {
        	eachartSatisfaction(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = {};

            data.name = [];
            data.fcmy = [];
            data.my = [];
            data.yb = [];
            data.bmy = [];
            data.completeName = ["满意","不满意"];
            eachartSatisfaction(data);
        }
    });
}
//加载柱状图
function eachartSatisfaction(data){
	 var myChart = echarts.init(document.getElementById("main_Satisfaction"));
	option = {
			tooltip : {
		        trigger: 'axis',
		        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		        }
		    },
		    legend: {
		        data:data.completeName
		    },
		    grid: { 
		        x: 30,
		        x2: 10,
		        y2: 80,
		    },
		    xAxis : [
		        {
		            type : 'category',
		            data : data.name
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : [
		       
		        {
		            name:'满意',
		            type:'bar',
		            data:data.my
		        },{
		            name:'不满意',
		            type:'bar',
		            data:data.bmy
		        }
		    ]
		};
	
	 myChart.setOption(option);
}

var win;
function bmyList(userId){
	if(win){
		win.close();
	}
	var startTime = $("#begDate_Satisfaction").val();
	var endTime = $("#endDate_Satisfaction").val();
	var url = basePath+"logined/defect/mydList.jsp?type=1&userId="+userId+"&startTime="+encodeURI(encodeURI(startTime))+"&endTime="+encodeURI(encodeURI(endTime));
	win = window.open(url,"_blank");
}

