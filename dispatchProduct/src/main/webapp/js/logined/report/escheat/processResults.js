$(document).ready(function() {

    getTable_process();
    $("#list_button_p").hide();
    $("#view_process").hide();
});

//查询
function query_process() {
    //判定图表是否隐藏
    if($("#view_process").is(":hidden")) {
        getTable_process()
    }else {
        findChart_process();
    }
}

//切换
function change_process(obj,type) {
    // 0 图表
    $(obj).hide();
    if(type == 0){
        $(obj).next().show();
        $("#list_process").hide();
        $("#view_process").show();
        findChart_process();
    }else {
        $(obj).prev().show()
        $("#view_process").hide();
        $("#list_process").show();

    }
}

//导出
function report_export_process(){
    var beginTime=$("#begDate_process").val();
    var endTime=$("#begDate_process").val();
    var className = $("#className").val();
    var url = basePath + "report/export.action?beginTime="+beginTime+"&endTime="+endTime+"&className="+className;
    url=encodeURI(url);
    window.open(url);
}


function getTable_process(){
    var param={
        'beginTime':$("#begDate_process").val(),
        'endTime':$("#begDate_process").val(),
        "type":2,
        'userName':""
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/satis_findSatisfaction.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            var html="";
            if(data.length>0){
                for(var i=0;i<data.length;i++){
                    if(i%2==0){
                        html+="<tr class='odd'>";
                    }else{
                        html+="<tr class='even'>";
                    }
                    html+="<td>"+data[i].no+"</td>" ;
                    html+="<td>"+data[i].name+"</td>";
                    html+="<td>"+data[i].groupName+"</td>";
                    /*html+="<td>"+data[i].fcmy+"</td>";*/
                    html+="<td>"+data[i].my+"</td>";
                   /* html+="<td>"+data[i].yb+"</td>";*/
                    if(data[i].no!="合计"){
                    	html+="<td><a onClick='bmyList("+data[i].userId+")' style='cursor:pointer;text-decoration:none'>"+data[i].bmy+"</a></td>";
                    }else{
                    	html+="<td>"+data[i].bmy+"</td>";
                    }
                    html+="</tr>";
                }
            }else{
                html ="<tr><td colspan='5'>暂无数据</td></tr>";
            }
            $("#tbody_process").html(html);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            var html ="<tr><td colspan='5'>暂无数据</td></tr>";
            $("#tbody_process").html(html);
        }
    });
}
/**
 * 获得视图
 */
function findChart_process(){
    var param={
        'beginTime':$("#begDate_process").val(),
        'endTime':$("#endDate_process").val(),
        "type":2,
        'className':$("#className").val()
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/satis_findStaffChart.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            eachart_process(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = {};

            data.name = [];
            data.fcmy = [];
            data.my = [];
            data.yb = [];
            data.bmy = [];
            data.completeName = ["满意","不满意"];
            eachart_process(data);
        }
    });
}
//加载柱状图
function eachart_process(data){
    var myChart = echarts.init(document.getElementById("view_process"));
    option = {
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data:data.completeName
        },
        grid: {
            x: 30,
            x2: 10,
            y2: 80,
        },
        xAxis : [
            {
                type : 'category',
                data : data.name
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            
            {
                name:'满意',
                type:'bar',
                data:data.my
            },
            {
                name:'不满意',
                type:'bar',
                data:data.bmy
            }
        ]
    };

    myChart.setOption(option);
}

var win;
function bmyList(userId){
	if(win){
		win.close();
	}
	var startTime = $("#begDate_process").val();
	var endTime = $("#endDate_process").val();
	var url = basePath+"logined/defect/mydList.jsp?type=2&userId="+userId+"&startTime="+encodeURI(encodeURI(startTime))+"&endTime="+encodeURI(encodeURI(endTime));
	win = window.open(url,"_blank");
}