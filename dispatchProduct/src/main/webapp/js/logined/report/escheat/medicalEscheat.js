$(document).ready(function() {

    getTable_medical();
});

//查询
function query_medical() {
   /* //判定图表是否隐藏
    if($("#view_process").is(":hidden")) {
        getTable_process()
    }else {
        findChart_process();
    }*/
    getTable_medical()
}

/*//切换
function change_process(obj,type) {
    // 0 图表
    $(obj).hide();
    if(type == 0){
        $(obj).next().show();
        $("#list_process").hide();
        $("#view_process").show();
        findChart_process();
    }else {
        $(obj).prev().show()
        $("#view_process").hide();
        $("#list_process").show();

    }
}*/

//导出
function report_export_medical(){
    var beginTime=$("#begDate_medical").val();
    var endTime=$("#begDate_medical").val();
    var className = $("#className").val();
    var url = basePath + "report/department_export.action?beginTime="+beginTime+"&endTime="+endTime+"&departmentName="+className+"&type=2";
    url=encodeURI(url);
    window.open(url);
}


function getTable_medical(){
    var param={
        'beginTime':$("#begDate_medical").val(),
        'endTime':$("#begDate_medical").val(),
        'departmentName':$("#className").val(),
        'type':2
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/department_findMedicalDepartmenList.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            var html="";
            if(data.length>0){
                for(var i=0;i<data.length;i++){
                    if(i%2==0){
                        html+="<tr class='odd'>";
                    }else{
                        html+="<tr class='even'>";
                    }
                    html+="<td>"+data[i].no+"</td>" ;
					html+="<td>"+data[i].name+"</td>";
					html+="<td>"+data[i].reportNum+"</td>";
					html+="<td>"+data[i].finishNum+"</td>";
					html+="<td>"+data[i].noFinishNum+"</td>";
                    html+="</tr>";
                }
            }else{
                html ="<tr><td colspan='5'>暂无数据</td></tr>";
            }
            $("#tbody_medical").html(html);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            var html ="<tr><td colspan='5'>暂无数据</td></tr>";
            $("#tbody_medical").html(html);
        }
    });
}

/*
/!**
 * 获得视图
 *!/
function findChart_medical(){
    var param={
        'beginTime':$("#begDate_medical").val(),
        'endTime':$("#endDate_medical").val(),
        'className':$("#className").val()
    }
    $.ajax({
        type : 'post',
        url : basePath + "report/shitu.action",
        data : param,
        dataType : 'json',
        success : function(data) {
            eachart_medical(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            var data = {};

            data.name = [];
            data.fcmy = [];
            data.my = [];
            data.yb = [];
            data.bmy = [];
            data.completeName = ["非常满意","满意","一般","不满意"];
            eachart_medical(data);
        }
    });
}
//加载柱状图
function eachart_medical(data){
    var myChart = echarts.init(document.getElementById("view_medical"));
    option = {
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data:data.completeName
        },
        grid: {
            x: 30,
            x2: 10,
            y2: 80,
        },
        xAxis : [
            {
                type : 'category',
                data : data.name
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'非常满意',
                type:'bar',
                data:data.fcmy
            },
            {
                name:'满意',
                type:'bar',
                data:data.my
            },{
                name:'一般',
                type:'bar',
                data:data.yb
            },
            {
                name:'不满意',
                type:'bar',
                data:data.bmy
            }
        ]
    };

    myChart.setOption(option);
}
*/

