package cn.com.qytx.hemei.report.goodReport.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.dao.BaseDao;

@Repository
public class GoodReportDao extends BaseDao<DefectApply, Integer> {
     /**
      * 科室使用物资统计
      * @return
      */
	public List<Object[]> findDepartmentUseGoods(String beginTime,String endTime,String groupName,String goodName,String goodGroupName){
		String sql= "SELECT g.update_time,g.company_id,g.instance_id,g.name,g.num,g.money,g.good_group_name, gi.group_name,g.price";
		sql+=" FROM tb_defect_apply d INNER JOIN tb_group_info gi ON gi.group_id = d.create_group_id";
		sql+=" INNER JOIN View_use_goods g ON d.instance_id = g.instance_id WHERE d.is_delete=0";
		if(StringUtils.isNoneBlank(beginTime)){
			beginTime=beginTime+" 00:00:00";
			sql+=" and g.update_time>='"+beginTime+"'";
		}
		if(StringUtils.isNoneBlank(endTime)){
			endTime=endTime+" 23:59:59";
			sql+=" and g.update_time<='"+endTime+"'";
		}
		if(StringUtils.isNoneBlank(groupName)){
			sql+=" and gi.group_name like '%"+groupName+"%'";
		}
		
		if(StringUtils.isNoneBlank(goodName)){
			sql+=" and g.name like '%"+goodName+"%'";
		}
		if(StringUtils.isNoneBlank(goodGroupName)){
			sql+=" and g.good_group_name like '%"+goodGroupName+"%'";
		}
		Query query = super.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
	
	/**
	 * 维修人员使用物资
	 * @return
	 */
	public List<Object[]> findRepairPeopleUseGoods(String beginTime,String endTime,String repairTeam,String name,String goodName,String goodGroupName){
		String sql=" select processer_name,group_name,name,good_group_name,num,price,money,update_time";
		sql+=" from View_repair_use_goods where 1=1";
		if(StringUtils.isNoneBlank(beginTime)){
			beginTime=beginTime+" 00:00:00";
			sql+=" and update_time>='"+beginTime+"'";
		}
		if(StringUtils.isNoneBlank(endTime)){
			endTime=endTime+" 23:59:59";
			sql+=" and update_time<='"+endTime+"'";
		}
		if(StringUtils.isNoneBlank(repairTeam)){
			sql+=" and group_name like '%"+repairTeam+"%'";
		}
		
		if(StringUtils.isNoneBlank(goodName)){
			sql+=" and name like '%"+goodName+"%'";
		}
		
		if(StringUtils.isNoneBlank(name)){
			sql+=" and processer_name like '%"+name+"%'";
		}
		if(StringUtils.isNoneBlank(goodGroupName)){
			sql+=" and good_group_name like '%"+goodGroupName+"%'";
		}
		
		Query query = super.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
	
	
}
