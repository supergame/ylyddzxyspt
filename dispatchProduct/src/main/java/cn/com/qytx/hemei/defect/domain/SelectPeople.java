package cn.com.qytx.hemei.defect.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.org.domain.UserInfo;

@Entity
@Table(name="tb_cbb_people_select")
public class SelectPeople extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 597476095646235525L;
  
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	
	@Column(name="instance_id")
	private String instanceId;//申请实例 规则 userId+yyyyMMddHHmmss
	
	
	/**
	 * 创建人
	 */
	@Column(name="create_user_id")
	private Integer createUserId;
	
	/**
	 * 创建日期
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	

	/**
	 * 资产组Id
	 */
	@JoinColumn(name="user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo userInfo;
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getInstanceId() {
		return instanceId;
	}


	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}


	public Integer getCreateUserId() {
		return createUserId;
	}


	public Timestamp getCreateTime() {
		return createTime;
	}


	public UserInfo getUserInfo() {
		return userInfo;
	}


	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}


	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}


	

	
	
	
	
}
