/**
 * 
 */
package cn.com.qytx.hemei.defect.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.dao.DefaultDepartmentDao;
import cn.com.qytx.hemei.defect.domain.DefaultDepartment;
import cn.com.qytx.hemei.defect.service.IDefaultDepartment;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月20日
 * 修改日期: 2017年5月20日
 * 修改列表: 
 */
@Service
@Transactional
public class DefaultDepartmentImpl extends BaseServiceImpl<DefaultDepartment> implements IDefaultDepartment {

	@Autowired
	private DefaultDepartmentDao defaultDepartmentDao ;
	@Override
	public DefaultDepartment findModel(Integer type) {
		// TODO Auto-generated method stub
		return defaultDepartmentDao.findModel(type);
	}

}
