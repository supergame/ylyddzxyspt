package cn.com.qytx.hemei.report.participant.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.hemei.report.participant.dao.ParticipantReportDao;
import cn.com.qytx.hemei.report.participant.impl.service.IParticipantReport;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
@Service
@Transactional
public class ParticipantReportImpl extends BaseServiceImpl<SelectPeople> implements IParticipantReport {

	@Autowired
	private ParticipantReportDao participantReportDao;
	
	@Override
	public List<Object[]> findParticipantReportList(String groupName,String userName,String beginTime,String endTime) {
		// TODO Auto-generated method stub
		return participantReportDao.findParticipantReportList(groupName,userName, beginTime, endTime);
	}

}
