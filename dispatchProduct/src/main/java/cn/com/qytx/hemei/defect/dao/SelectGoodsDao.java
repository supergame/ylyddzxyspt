package cn.com.qytx.hemei.defect.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.SelectGoods;
import cn.com.qytx.platform.base.dao.BaseDao;
@Repository
public class SelectGoodsDao extends BaseDao<SelectGoods, Integer> {
   /**
    * 通过gooid 和InstanceId 查找记录
    * @param goodId
    * @param instanceId
    * @return
    */
	public SelectGoods findByGoodIdAndInstanceId(Integer goodId ,String instanceId){
		String hql = " isDelete=0  and good.id="+goodId+" and instanceId='"+instanceId+"'";
		return super.findOne(hql);
	}
	
	/**
	 * 通过流程Id 查询
	 * @return
	 */
	public List<SelectGoods> findList(String instanceId){
		String hql =" 1=1";
		if(StringUtils.isNoneBlank(instanceId)){
			hql += " and  instanceId='"+instanceId+"'";
		}
		return super.unDeleted().findAll(hql);
	}
	
	
	
}
