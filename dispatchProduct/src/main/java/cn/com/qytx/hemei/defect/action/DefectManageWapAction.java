package cn.com.qytx.hemei.defect.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.file.domain.Attachment;
import cn.com.qytx.cbb.file.service.IAttachment;
import cn.com.qytx.cbb.myapply.domain.MyProcessed;
import cn.com.qytx.cbb.myapply.service.IMyProcessed;
import cn.com.qytx.cbb.notify.service.IWapNotify;
import cn.com.qytx.hemei.cardispatch.service.ICarOrder;
import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.hemei.defect.domain.SearchVo;
import cn.com.qytx.hemei.defect.domain.UserCapital;
import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.IDefectApply;
import cn.com.qytx.hemei.defect.service.IUserCapital;
import cn.com.qytx.hemei.location.domain.GroupLocation;
import cn.com.qytx.hemei.location.domain.Location;
import cn.com.qytx.hemei.location.service.IGroupLocation;
import cn.com.qytx.hemei.location.service.ILocation;
import cn.com.qytx.hemei.util.EventForAddWorkOrderLog;
import cn.com.qytx.hemei.util.SystemConfig;
import cn.com.qytx.hemei.util.WorkOrderType;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.spring.SpringUtil;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 手机端Action
 * @ClassName: DefectManageWapAction   
 * @author: WANG
 * @Date: 2016年10月11日 上午11:21:37   
 *
 */
public class DefectManageWapAction extends BaseActionSupport{
	private static final long serialVersionUID = 1L;
	@Autowired
	private IDefectApply defectApplyImpl;
	@Autowired
	private IMyProcessed MyProcessImpl;
	@Resource(name="dictService")
	private IDict dictService;
	@Resource
	private IUser userService;
	@Resource
	private IGroup groupService;
	@Autowired
	private SystemConfig systemConfig;
	@Resource(name="attachmentService")
	private IAttachment attachmentService;
	
	@Resource(name="userCapitalService")
	private IUserCapital userCapitalService;

	@Resource(name = "groupLocationService")
	private IGroupLocation groupLocationService;
	
	@Resource(name="locationService")
	private ILocation locationService;
	
	@Resource
	private IWapNotify wapNotifyService;
	
	@Resource
	private ICarOrder carOrderService;
	
	private Integer userId;
	private DefectApply apply;
	private String instanceId;
	private String approveResult;
	private String advice;
	private String taskName;
	private Integer nextUserId;
	private String startTime;
	private String endTime;
	private String equipmentUnit;
	private String defectDepartment;
	private String defectProfessional;
	private SearchVo searchVo;
	/**
	 * 工号
	 */
	private String workNo;
	
	private String type;
	
	private Integer processType;
	
	private Integer achievement;
	
	private String evaluate;
	
	private String equipments;
	
	private Integer isForkGroup;//是否有部门权限 值为1 则只能查看本部门工单
	
	private String attachmentIds;
	/**
	 * 消缺上报
	 * @Title: apply   
	 */
	public void apply(){
		try{
			if(userId!=null){
				UserInfo userInfo = userService.findOne(userId);
				if(StringUtils.isNotBlank(apply.getInstanceId())){
					DefectApply defectApply=defectApplyImpl.findByInstanceId(apply.getInstanceId());
					GroupInfo createGroup = groupService.findOne(defectApply.getCreateGroupId());
					if(defectApply.getType()!=0){
						defectApply.setEquipmentName(apply.getEquipmentName());
					}
					defectApply.setAttachmentIds(apply.getAttachmentIds());
					defectApply.setAudioIds(apply.getAudioIds());
					defectApply.setDescribe(apply.getDescribe());
					defectApply.setAudioTime(apply.getAudioTime());
					defectApply.setGrade(apply.getGrade());
					defectApply.setDefectTime(apply.getDefectTime());
					defectApply.setMemo(apply.getMemo());
					defectApply.setEquipmentNumber(apply.getEquipmentNumber());
					defectApply.setCreateGroupId(apply.getCreateGroupId());
					/**
					 * 修改工单日志start
					 */
					WorkOrderLog workOrderLog = new WorkOrderLog();
					workOrderLog.setCompanyId(userInfo.getCompanyId());
					workOrderLog.setOperUser(userInfo);
					workOrderLog.setType(WorkOrderType.WORK_ORDER_UPDATE);
					workOrderLog.setInstanceId(defectApply.getInstanceId());
					workOrderLog.setContent("修改工单-手机端，原内容：[上报科室："+(createGroup!=null?createGroup.getGroupName():"--")+"，维修地点："+defectApply.getDescribe()+"，备注："+defectApply.getMemo()+"]");
					/**
					 * 添加工单日志end
					 */
					defectApplyImpl.saveOrUpdate(defectApply);
					SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
					ajax("100||操作成功");
				}else{
					
					Integer checkNum = defectApplyImpl.findWaitCheckNum(userId, apply.getCreateGroupId(), userInfo.getCompanyId());
					if(checkNum > 0 ){
						ajax("101||您还有未验收的工单,请验收后上报!");
					}else{
						apply.setTurnNum(0);
						apply.setReminderNum(0);
						List<Integer> capitalIdList = new ArrayList<Integer>();
						if(StringUtils.isNoneBlank(equipments)){
							Gson gson = new Gson();
							capitalIdList = gson.fromJson(equipments,new TypeToken<List<Integer>>(){}.getType());
						}
						apply=defectApplyImpl.apply(userId, apply,capitalIdList);
						ajax("100||"+apply.getInstanceId());
						WorkOrderLog workOrderLog = new WorkOrderLog();
						workOrderLog.setCompanyId(userInfo.getCompanyId());
						workOrderLog.setOperUser(userInfo);
						workOrderLog.setType(WorkOrderType.WORK_ORDER_ADD);
						workOrderLog.setInstanceId(apply.getInstanceId());
						workOrderLog.setContent("添加工单-手机端");
						SpringUtil.getApplicationContext().publishEvent(new EventForAddWorkOrderLog(workOrderLog) );
					}
				}
			}else{
				ajax("101||参数缺少");
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	/**
	 * 处理操作
	 * @Title: approve   
	 */
	public void approve(){
		try{
			if(userId!=null){
				DefectApply defectApply=defectApplyImpl.findByInstanceId(instanceId);
				if("2".equals(taskName)){
					if("1".equals(defectApply.getState())){
						ajax("101||任务已不存在");
						return;
					}
				}
				List<Integer> capitalIdList = new ArrayList<Integer>();
				if(defectApply.getType()!=0&&StringUtils.isNoneBlank(equipments)){
					Gson gson = new Gson();
					capitalIdList = gson.fromJson(equipments,new TypeToken<List<Integer>>(){}.getType());
				}
				int res=defectApplyImpl.approve(userId, instanceId, approveResult, advice, taskName, nextUserId,processType,achievement,evaluate,capitalIdList,attachmentIds);
				if(res==1){
					ajax("100||操作成功");
				}else{
					ajax("101||操作失败");
				}
			}else{
				ajax("101||参数缺少");
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	/**
	 * 再次评价
	 */
	public void updateEvaluate(){
		try{
			DefectApply defectApply=defectApplyImpl.findByInstanceId(instanceId);
			defectApply.setAchievement(achievement);
			defectApply.setEvaluate(evaluate);
			defectApplyImpl.saveOrUpdate(defectApply);
			ajax("100||操作成功");
		}catch(Exception e){
			e.printStackTrace();
			ajax("101||操作失败");
		}
	}
	
	/**
	 * 申请详情
	 * @Title: view   
	 */
	public void view(){
		try{
			if(userId!=null){
				UserInfo userInfo=userService.findOne(userId);
				if(userInfo!=null){
					Map<String, Map<String, String>> map= dictService.findAllDict();
					DefectApply apply=defectApplyImpl.findByInstanceId(instanceId);
					UserInfo applyUser = userService.findOne(apply.getCreateUserId());
					apply.setCreateUserName(applyUser!=null?applyUser.getUserName():"");//上报人
					apply.setCreateUserPhone(applyUser!=null?applyUser.getPhone():"");//上报人
					apply.setCreateUserWorkNo(applyUser!=null?applyUser.getWorkNo():"");//上报人
					if(apply.getCreateGroupId()!=null){
						GroupInfo applyGroup = groupService.findOne(apply.getCreateGroupId());
						apply.setDefectDepartmentVo(applyGroup!=null?applyGroup.getGroupName():"");
						apply.setCreateGroupName(applyGroup!=null?applyGroup.getGroupName():"");
					}else{
						GroupInfo applyGroup = groupService.findOne(applyUser!=null?applyUser.getGroupId():0);
						apply.setDefectDepartmentVo(applyGroup!=null?applyGroup.getGroupName():"");
						apply.setCreateGroupId(applyUser!=null?applyUser.getGroupId():null);
						apply.setCreateGroupName(applyGroup!=null?applyGroup.getGroupName():"");
					}
					
					SimpleDateFormat sdfYYYYMMDDHHMM = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
					apply.setDefectTimeStr(sdfYYYYMMDDHHMM.format(apply.getDefectTime()));
					apply.setCreateTimeStr(sdfYYYYMMDDHHMM.format(apply.getCreateTime()));
					Map<String,Object> resMap=new HashMap<String,Object>();
					resMap.put("longinUserName",userInfo.getUserName());
					resMap.put("longinUserId",userInfo.getUserId());
					if(apply!=null){
						Integer achievement=apply.getAchievement();
						if(achievement!=null){
							if(achievement==1){
								apply.setAchievementStr("非常满意");
							}else if(achievement==2){
								apply.setAchievementStr("满意");
							}else if(achievement==3){
								apply.setAchievementStr("一般");
							}else{
								apply.setAchievementStr("不满意");
							}
						}
						
						long hour = (apply.getDefectTime().getTime()-System.currentTimeMillis())/(1000*60*60);
						Integer outTimeState = 0;
						if(hour<0&&(!"3".equals(apply.getState())&&!"4".equals(apply.getState()))){
							hour = Math.abs(hour);
							outTimeState = 1;
						}
						if("3".equals(apply.getState())||"4".equals(apply.getState())){
							outTimeState = 2;
						}
						String timeStr = "";
						if(hour>24){
							timeStr = hour/24+"天";
						}else{
							timeStr = hour+"小时";
						}
						resMap.put("hour", timeStr);
						resMap.put("outTimeState", outTimeState);
						
						
						String processUserId = "";//处理人Id
						String processGroupName = "";//处理科室
						String processGroupPhone = "";//处理科室电话
						String processUserName = "";//处理人
						String processUserPhone = "";//处理人电话
						String processTime = "";
						String applyState=apply.getState();
						String state="";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							String delayState = "";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
							apply.setDelayStateVo(delayState);
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							//state="待归档";
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						apply.setStateVo(state);
						
						//添加设备记录
						if(apply.getType().intValue()!=0){
							List<UserCapital> ucList = userCapitalService.findUserCapitalListByInstanceId(instanceId);
							if(ucList!=null&&ucList.size()>0){
								List<Map<String,Object>> equipmentList = new ArrayList<Map<String,Object>>();
								for(UserCapital userCapital:ucList){
									Map<String,Object> equipmentMap = new HashMap<String, Object>();
									Capital capital = userCapital.getCapital();
									equipmentMap.put("id",capital.getId() );
									equipmentMap.put("name", capital.getName());
									equipmentMap.put("capitalNo", StringUtils.isNotBlank(capital.getCapitalNo())?capital.getCapitalNo():"");
									equipmentMap.put("capitalModel", StringUtils.isNotBlank(capital.getCapitalModel())?capital.getCapitalModel():"");
									equipmentMap.put("capitalType", 1);
									equipmentList.add(equipmentMap);
								}
								resMap.put("equipmentList", equipmentList);
							}
						}
						
						String grade=apply.getGrade();
						Map<String, String> gradeMap= map.get("grade");
						if(StringUtils.isNotBlank(grade)&&gradeMap!=null){
							String gradeVo= gradeMap.get(grade);
							if(StringUtils.isNotBlank(gradeVo)){
								apply.setGradeVo(gradeVo);
							}
						}
						//处理历史
						List<MyProcessed> historyList= MyProcessImpl.findByInstanceId(instanceId);
						for(MyProcessed myProcessed:historyList){
							String name="";
							UserInfo user=userService.findOne(myProcessed.getProcesserId());
							if(user!=null){
								myProcessed.setProcesserPhone(user.getPhone());
								GroupInfo processGroup = groupService.findOne(user.getGroupId());
								name=processGroup.getGroupName()+"  "+user.getUserName()+"（"+user.getWorkNo()+"）";
							}
							GroupInfo group=groupService.findOne(myProcessed.getProcesserId());
							if(group!=null){
								name=group.getGroupName();
							}
							myProcessed.setProcesserDealName(name);
							if(myProcessed.getNextProcesserId()!=null){
								Integer nextProcessType = myProcessed.getNextProcessType();
								if(nextProcessType!=null&&nextProcessType.intValue()==2){
									GroupInfo nextGroup = groupService.findOne(myProcessed.getNextProcesserId());
									if(nextGroup!=null){
										myProcessed.setNextProcesserName(nextGroup.getGroupName());
										myProcessed.setNextProcesserPhone(nextGroup.getPhone());
									}
								}else{
									UserInfo nextUser=userService.findOne(myProcessed.getNextProcesserId());
									if(nextUser!=null){
										myProcessed.setNextProcesserName(nextUser.getUserName());
										myProcessed.setNextProcesserPhone(nextUser.getPhone());
									}
								}
								
							}
							if("3".equals(myProcessed.getTaskName())&&(applyState.equals("2")||applyState.equals("8"))){
								UserInfo processUser = userService.findOne(myProcessed.getProcesserId());
								if(processUser!=null){
									processUserId = processUser.getUserId()+"";
									processUserName = processUser.getUserName();
									processUserPhone = processUser.getPhone();
									processTime = sdfYYYYMMDDHHMM.format(myProcessed.getEndTime());
									GroupInfo processUserGroup = groupService.findOne(processUser.getGroupId());
									if(processUserGroup!=null){
										processGroupName = processUserGroup.getGroupName();
										processGroupPhone = processUserGroup.getPhone();
									}
								}
							}
							if("3".equals(myProcessed.getTaskName())&&applyState.equals("3")){
								UserInfo processUser = userService.findOne(myProcessed.getProcesserId());
								if(processUser!=null){
									processUserName = processUser.getUserName();
									processUserPhone = processUser.getPhone();
									processTime = sdfYYYYMMDDHHMM.format(myProcessed.getEndTime());
									GroupInfo processUserGroup = groupService.findOne(processUser.getGroupId());
									if(processUserGroup!=null){
										processGroupName = processUserGroup.getGroupName();
										processGroupPhone = processUserGroup.getPhone();
									}
								}
							}
						}
						apply.setHistoryList(historyList);
						resMap.put("apply", apply);
						resMap.put("processUserId", processUserId);
						resMap.put("processUserName", processUserName);
						resMap.put("processUserPhone", processUserPhone);
						resMap.put("processGroupName", processGroupName);
						resMap.put("processGroupPhone", processGroupPhone);
						resMap.put("processTime", processTime);
						if(StringUtils.isNoneBlank(apply.getDispatchWorkNo())){ 
							UserInfo dispatchUser = userService.findOne(" workNo=?",apply.getDispatchWorkNo()); 
							resMap.put("dispatchUserName",dispatchUser!=null?dispatchUser.getUserName():"");
							resMap.put("dispatchWorkNo", apply.getDispatchWorkNo());
						}else{
							resMap.put("dispatchUserName","");
							resMap.put("dispatchWorkNo", "");
						}
						//系统配置
						if (systemConfig!=null){
							String phone=systemConfig.getTurnPeople();
							List<UserInfo> list=userService.findUsersByPhone(phone);
							resMap.put("turnUser", (list!=null&&list.size()>0)?list.get(0):userInfo);
							resMap.put("turnNum", systemConfig.getTurnNum());
							resMap.put("rejectNum", systemConfig.getRejectNum());
						}
						//待作废次数
						int count= defectApplyImpl.getUnApprove(userInfo.getUserId(),userInfo.getCompanyId(),apply.getInstanceId());
						if(count>=systemConfig.getRejectNum()){
							apply.setIsDraft(1);
						}
						//图片
						if(StringUtils.isNotBlank(apply.getAttachmentIds())){
							List<Attachment> attList= attachmentService.getAttachmentsByIds(apply.getAttachmentIds());
							resMap.put("attList", attList);
						}
						//处理图片
						if(StringUtils.isNotBlank(apply.getCheckAttachmentIds())){
							List<Attachment> attList= attachmentService.getAttachmentsByIds(apply.getCheckAttachmentIds());
							resMap.put("checkAttList", attList);
						}
						
					}
					Gson gson=new GsonBuilder().setDateFormat("yyyy年MM月dd日 HH:mm").create();
					ajax("100||"+gson.toJson(resMap));
				}
			}else{
				ajax("101||参数缺少");
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	
	public void getCurrUserName(){
		try{
			if(userId==null){
				ajax("101||参数缺少");
				return;
			}
			UserInfo userInfo = userService.findOne(userId);
			ajax("100||"+userInfo.getUserName());
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	/**
	 * 我的申请列表
	 * @Title: myStarted   
	 */
	public void myStarted(){
		try{
			//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
			Sort sort=new Sort(Direction.DESC,"createTime");
			Pageable pageable=this.getPageable(sort);
			UserInfo userInfo = userService.findOne(userId);
			if("view".equals(type)){
				userId = null;
			}
			if(isForkGroup!=null&&isForkGroup.intValue()==1){
				searchVo.setGroupId(userInfo.getGroupId());
				userId = null;
			}
			Page<DefectApply> page=defectApplyImpl.myStarted(pageable, userId,searchVo);
			List<DefectApply> list=page.getContent();
			List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
			Map<Integer,String> groupMap = groupService.findGroupMap(userInfo.getCompanyId(), 1);
			if(list!=null&&list.size()>0){	
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日");
				for(DefectApply apply:list){
					Map<String,Object> map=new HashMap<String,Object>();
					String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
					String grade=apply.getGrade()==null?"":apply.getGrade();
					String describe=apply.getDescribe()==null?"":apply.getDescribe();
					//UserInfo applyUser = userService.findOne(apply.getCreateUserId());
					String  defectDepartmentVo="暂无";
					if(apply.getCreateGroupId()!=null){
						defectDepartmentVo=groupMap.get(apply.getCreateGroupId());
					}
					String defectTime="";
					if(apply.getDefectTime()!=null){
						defectTime=sdf.format(apply.getDefectTime());
					}
					String isVoice = "0";
					if(StringUtils.isNotBlank(apply.getAudioIds())){
						isVoice = "1";
					}
					map.put("isVoice", isVoice);
					String isImage = "0";
					if(StringUtils.isNotBlank(apply.getAttachmentIds())){
						isImage = "1";
					}
					map.put("isImage", isImage);
					Integer type=apply.getType();
					map.put("type", type);
					String updateTime="";
					if(apply.getUpdateTime()!=null){
						updateTime=sdf.format(apply.getUpdateTime());
					}
					
					String createTime="";
					if(apply.getCreateTime()!=null){
						createTime=sdf.format(apply.getCreateTime());
					}
					
					String state="";//状态
					String processName=apply.getProcessName();//当前操作人
					String applyState=apply.getState();
					String delayState = "";
					if(applyState.equals("0")||applyState.equals("6")){
						state="待接收";
					}else if(applyState.equals("1")||applyState.equals("7")){
						state="待处理";
						if(apply.getIsDelay()!=null){
							if(apply.getIsDelay()==1){
								delayState = "(待延期)";
							}else if(apply.getIsDelay()==2){
								delayState = "(延期中)";
							}else if(apply.getIsDelay()==3){
								delayState = "(延期驳回)";
							}
						}
					}else if(applyState.equals("2")||applyState.equals("8")){
						state="待验收";
					}else if(applyState.equals("3")){
						state="已完结";
					}else if(applyState.equals("5")){
						state="待作废";
					}else if(applyState.equals("9")){
						state="已作废";
					}else if(applyState.equals("-1")){
						state="待调度";
					}
					
					String currOperName = "";
					if(apply.getProcessType()==2){
						GroupInfo groupInfo = groupService.findOne(apply.getProcessId());
						currOperName = groupInfo!=null?groupInfo.getGroupName():"--";
					}else{
						UserInfo tempUserInfo = userService.findOne(apply.getProcessId());
						currOperName = tempUserInfo!=null?tempUserInfo.getUserName():"--";
					}
					map.put("currOperName", currOperName);
					map.put("delayStateVo", delayState);
					map.put("instanceId", instanceId);
					map.put("grade", grade);
					map.put("describe", describe);
					map.put("defectTime", defectTime);
					map.put("updateTime", updateTime);
					map.put("createTime", createTime);
					map.put("state", state);
					map.put("processName", processName);
					map.put("defectDepartmentVo", defectDepartmentVo);
					map.put("equipmentName",StringUtils.isNotBlank(apply.getEquipmentName())?apply.getEquipmentName():"");
					map.put("reminderNum",apply.getReminderNum());
					mapList.add(map);
				}
			}
			Map<String, Object> jsonMap = new HashMap<String, Object>();
			jsonMap.put("iTotalRecords", page.getTotalElements());
			jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
			jsonMap.put("aaData", mapList);
			Gson gson = new Gson();
			ajax("100||"+gson.toJson(jsonMap));
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	/**
	 * 待我处理的
	 * @Title: myWaitProcess   
	 */
	public void myWaitProcess(){
		try{
			Map<String, Object> jsonMap = new HashMap<String, Object>();
			//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
			Pageable pageable=this.getPageable();
			UserInfo userInfo = userService.findOne(userId);
			if(userInfo!=null){
				if(searchVo==null){
					searchVo = new SearchVo();
				}
				if(isForkGroup!=null&&isForkGroup.intValue()==1){
					searchVo.setGroupId(userInfo.getGroupId());
					searchVo.setIsOut("1");
					userId = null;
				}
				searchVo.setGroupId(userInfo.getGroupId());
				Page<DefectApply> page=defectApplyImpl.myWaitProcess(pageable, userId,taskName,searchVo,null,null);
				List<DefectApply> list=page.getContent();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					Map<Integer,String> groupMap = groupService.findGroupMap(userInfo.getCompanyId(), 1);
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
					for(DefectApply apply:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
						String grade=apply.getGrade()==null?"":apply.getGrade();
						String describe=apply.getDescribe()==null?"":apply.getDescribe();
						String defectTime="";
						if(apply.getDefectTime()!=null){
							defectTime=sdf.format(apply.getDefectTime());
						}
						String state="";//状态
						String processName=apply.getProcessName();//当前操作人
						String applyState=apply.getState();
						String delayState = "";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						String isVoice = "0";
						if(StringUtils.isNotBlank(apply.getAudioIds())){
							isVoice = "1";
						}
						map.put("isVoice", isVoice);
						String isImage = "0";
						if(StringUtils.isNotBlank(apply.getAttachmentIds())){
							isImage = "1";
						}
						
						String updateTime="";
						if(apply.getUpdateTime()!=null){
							updateTime=sdf.format(apply.getUpdateTime());
						}
						
						String createTime="";
						if(apply.getCreateTime()!=null){
							createTime=sdf.format(apply.getCreateTime());
						}
						
						String currOperName = "";
						if(apply.getProcessId()!=null){
							if(apply.getProcessType()==2){
								GroupInfo groupInfo = groupService.findOne(apply.getProcessId());
								currOperName = groupInfo!=null?groupInfo.getGroupName():"--";
							}else{
								UserInfo tempUserInfo = userService.findOne(apply.getProcessId());
								currOperName = tempUserInfo!=null?tempUserInfo.getUserName():"--";
							}
						}
						map.put("defectDepartment", groupMap.containsKey(apply.getCreateGroupId())?groupMap.get(apply.getCreateGroupId()):"--");
						Integer type=apply.getType();
						map.put("type", type);
						map.put("currOperName", currOperName);
						map.put("isImage", isImage);
						map.put("instanceId", instanceId);
						map.put("grade", grade);
						map.put("describe", describe);
						map.put("defectTime", defectTime);
						map.put("updateTime", updateTime);
						map.put("createTime", createTime);
						map.put("state", state);
						map.put("processName", processName);
						map.put("delayStateVo", delayState);
						//String defectDepartment=apply.getDefectDepartment()==null?"":apply.getDefectDepartment();
						//String defectDepartmentVo = getNameByDictMap(dictMap,"defectDepartment",StringUtils.isNotBlank(defectDepartment)?Integer.valueOf(defectDepartment):null);
					   //map.put("cc", defectDepartmentVo);
						map.put("equipmentName",StringUtils.isNotBlank(apply.getEquipmentName())?apply.getEquipmentName():"");
						//String gradeVo = getNameByDictMap(dictMap,"grade",StringUtils.isNotBlank(grade)?Integer.valueOf(grade):null);
						//map.put("gradeVo",gradeVo);
						mapList.add(map);
					}
				}
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);	
			}
			Gson gson = new Gson();
			ajax("100||"+gson.toJson(jsonMap));
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	/**
	 * 	
	 * @Title: myProcessed   
	 */
	public void myProcessed(){
		try{
			UserInfo user=userService.findOne(userId);
			if(user!=null){
				//如果userId为空，则是领导查看全部申请，不为空则是查看我的申请
				Pageable pageable=this.getPageable();
				if(searchVo==null){
					searchVo = new SearchVo();
				}
				if(isForkGroup!=null&&isForkGroup.intValue()==1){
					searchVo.setGroupId(user.getGroupId());
					searchVo.setIsOut("1");
					userId = null;
				}
				Page<DefectApply> page=defectApplyImpl.myProcessed(pageable, userId,taskName,searchVo);
				List<DefectApply> list=page.getContent();
				List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
				if(list!=null&&list.size()>0){
					Map<Integer,String> groupMap = groupService.findGroupMap(user.getCompanyId(), 1);
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
					for(DefectApply apply:list){
						Map<String,Object> map=new HashMap<String,Object>();
						String instanceId=apply.getInstanceId()==null?"":apply.getInstanceId();
						String grade=apply.getGrade()==null?"":apply.getGrade();
						String describe=apply.getDescribe()==null?"":apply.getDescribe();
						String defectTime="";
						if(apply.getDefectTime()!=null){
							defectTime=sdf.format(apply.getDefectTime());
						}
						String state="";//状态
						String processName=apply.getProcessName();//当前操作人
						String applyState=apply.getState();
						String delayState = "";
						if(applyState.equals("0")||applyState.equals("6")){
							state="待接收";
						}else if(applyState.equals("1")||applyState.equals("7")){
							state="待处理";
							if(apply.getIsDelay()!=null){
								if(apply.getIsDelay()==1){
									delayState = "(待延期)";
								}else if(apply.getIsDelay()==2){
									delayState = "(延期中)";
								}else if(apply.getIsDelay()==3){
									delayState = "(延期驳回)";
								}
							}
						}else if(applyState.equals("2")||applyState.equals("8")){
							state="待验收";
						}else if(applyState.equals("3")){
							state="已完结";
						}else if(applyState.equals("4")){
							state="已归档";
						}else if(applyState.equals("5")){
							state="待作废";
						}else if(applyState.equals("9")){
							state="已作废";
						}else if(applyState.equals("-1")){
							state="待调度";
						}
						String isVoice = "0";
						if(StringUtils.isNotBlank(apply.getAudioIds())){
							isVoice = "1";
						}
						map.put("isVoice", isVoice);
						String isImage = "0";
						if(StringUtils.isNotBlank(apply.getAttachmentIds())){
							isImage = "1";
						}
						
						String updateTime="";
						if(apply.getUpdateTime()!=null){
							updateTime=sdf.format(apply.getUpdateTime());
						}
						
						String createTime="";
						if(apply.getCreateTime()!=null){
							createTime=sdf.format(apply.getCreateTime());
						}
						
						String currOperName = "";
						if(apply.getProcessId()!=null){
							if(apply.getProcessType()==2){
								GroupInfo groupInfo = groupService.findOne(apply.getProcessId());
								currOperName = groupInfo!=null?groupInfo.getGroupName():"--";
							}else{
								UserInfo tempUserInfo = userService.findOne(apply.getProcessId());
								currOperName = tempUserInfo!=null?tempUserInfo.getUserName():"--";
							}
						}
						map.put("defectDepartment", groupMap.containsKey(apply.getCreateGroupId())?groupMap.get(apply.getCreateGroupId()):"--");
						Integer type=apply.getType();
						map.put("type", type);
						map.put("currOperName", currOperName);
						map.put("isImage", isImage);
						map.put("instanceId", instanceId);
						map.put("grade", grade);
						map.put("describe", describe);
						map.put("defectTime", defectTime);
						map.put("createTime", createTime);
						map.put("updateTime", updateTime);
						map.put("state", state);
						map.put("processName", processName);
						map.put("delayStateVo", delayState);
						map.put("equipmentName",StringUtils.isNotBlank(apply.getEquipmentName())?apply.getEquipmentName():"");
						mapList.add(map);
					}
				}
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", page.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", page.getTotalElements());
				jsonMap.put("aaData", mapList);
				Gson gson = new Gson();
				ajax("100||"+gson.toJson(jsonMap));
			}
			
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	

	
	
	/**
	 * 统计报表
	 * @Title: report   
	 */
	public void report(){
		try{
			if(userId!=null){
				Map<String,Object> map=defectApplyImpl.getReport(startTime, endTime, equipmentUnit, defectDepartment,defectProfessional);
				if(map!=null){
					Gson gson=new Gson();
					ajax("100||"+gson.toJson(map));
				}else{
					ajax("101||获取失败");
				}
			}else{
				ajax("101||参数缺少");
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	/**
	 * 待处理数量
	 * @Title: delete   
	 */
	public void getUnHandleCount(){
		try{
			
			if(userId!=null){
				UserInfo user=userService.findOne(userId);
				if(user!=null){
					Map<String, Object> map=defectApplyImpl.getWaitHandle(user.getGroupId(),user.getUserId(), user.getCompanyId(),null,null);
					if(map!=null){
						Gson gson=new Gson();
						int myJoinNum = defectApplyImpl.waitProcessCount(user.getUserId(), "2");
						int notifyUnReadCount = wapNotifyService.getNotifyUnReadCount(35, user.getUserId(), user.getCompanyId());
						map.put("myJoinCount", myJoinNum);//我参与的 数量
						map.put("notifyUnReadCount", notifyUnReadCount);//公告未读数量
						Map<String,Integer> carOrderWaitMap = carOrderService.getCarOrderWaitNum(user.getCompanyId(), userId,user.getGroupId());
						map.put("waitDispatch", carOrderWaitMap.get("waitDispatch"));//待调度
						map.put("waitStartCar", carOrderWaitMap.get("waitStartCar"));//待发车
						map.put("waitConfirm", carOrderWaitMap.get("waitConfirm"));//待 接工
						map.put("confirmFactoryNum", carOrderWaitMap.get("confirmFactoryNum"));//待确认到场
						ajax("100||"+gson.toJson(map));
					}else{
						ajax("101||获取失败");
					}
				}
			}else{
				ajax("101||参数缺少");
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 待处理数量
	 * @Title: delete   
	 */
	public void getUnHandleCount_isOut(){
		try{
			Map<String, Object> map=defectApplyImpl.getWaitHandle_All(1);
			Gson gson = new Gson();
			ajax(gson.toJson(map));
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 功能：通过infoType和值获取 Name
	 * @param dictMap
	 * @param infoType
	 * @param value
	 * @return
	 */
	public static String getNameByDictMap(Map<String, Map<String, String>> dictMap,String infoType,Integer value){
		String result = "";
		if(value!=null&&dictMap!=null&&dictMap.containsKey(infoType)){
			Map<String,String> map = dictMap.get(infoType);
			for(Map.Entry<String, String> entry:map.entrySet()){
				String key = entry.getKey();
				if(key.equals(String.valueOf(value))){
					result = entry.getValue();
					break;
				}
			}
		}
		return result;
	}
	/**
	 * 获得当亲登录人部门和地点
	 */
	public  void findUserGroupAndLocation(){
		try {
			if(userId!=null){
				UserInfo user=userService.findOne(userId);
				if(user!=null){
                         GroupInfo group=groupService.findOne(user.getGroupId());
                         Map<String,Object> map =new HashMap<String, Object>();
                         if(group!=null){
                        	List<GroupLocation> glList = groupLocationService.findList(group.getGroupId());
                 			if(glList!=null&&glList.size()>0){
                 				List<Location> newLocationList = locationService.getLocationPathListByIds(glList.get(0).getLocationId()+"",user.getCompanyId());
                 				if(newLocationList!=null&&newLocationList.size()>0){
                 					Location location = newLocationList.get(0);
                 					map.put("location",location.getPathName());
                 					map.put("groupName",group.getGroupName() );
                 					map.put("groupId",group.getGroupId() );
                 					Gson json = new Gson();
                 					ajax("100||"+json.toJson(map));
                 				}
                 			}
                         }

				}
			}else{
				ajax("101||参数缺少");
			}
		} catch (Exception e) {
			ajax("102||程序异常");
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 功能：提醒操作
	 * @return
	 */
	public String pushReminder(){
		try{
			if(userId!=null){
				UserInfo userInfo = userService.findOne(userId);
				if(userInfo!=null){
					defectApplyImpl.pushReminder(userInfo,instanceId);
					ajax("100||操作成功");
				}
			}else{
				if(StringUtils.isNotBlank(workNo)){
					String reminderContent = "您有一条院内会诊工单,请及时处理!";
					List<Integer> userIds = new ArrayList<Integer>();
					if(workNo.contains(",")){
						if(workNo.startsWith(",")){
							workNo = workNo.substring(1);
						}
						if(workNo.endsWith(",")){
							workNo = workNo.substring(0, workNo.length()-1);
						}
						String workNoArr[] = workNo.split(",");
						for(String tempWorkNo : workNoArr){
							UserInfo userInfo = userService.findByWorkNo(tempWorkNo);
							userIds.add(userInfo.getUserId());
						}
					}else{
						UserInfo userInfo = userService.findByWorkNo(workNo);
						userIds.add(userInfo.getUserId());
					}
					defectApplyImpl.pushReminderNew(reminderContent,userIds);
					ajax("100||操作成功");
				}else{
					ajax("101||参数缺少");
				}
			}
		}catch(Exception e){
			ajax("102||程序异常");
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public DefectApply getApply() {
		return apply;
	}
	public void setApply(DefectApply apply) {
		this.apply = apply;
	}
	public IDefectApply getDefectApplyImpl() {
		return defectApplyImpl;
	}
	public void setDefectApplyImpl(IDefectApply defectApplyImpl) {
		this.defectApplyImpl = defectApplyImpl;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getApproveResult() {
		return approveResult;
	}
	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}
	public String getAdvice() {
		return advice;
	}
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Integer getNextUserId() {
		return nextUserId;
	}
	public void setNextUserId(Integer nextUserId) {
		this.nextUserId = nextUserId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getEquipmentUnit() {
		return equipmentUnit;
	}
	public void setEquipmentUnit(String equipmentUnit) {
		this.equipmentUnit = equipmentUnit;
	}
	public String getDefectDepartment() {
		return defectDepartment;
	}
	public void setDefectDepartment(String defectDepartment) {
		this.defectDepartment = defectDepartment;
	}
	public SearchVo getSearchVo() {
		return searchVo;
	}
	public void setSearchVo(SearchVo searchVo) {
		this.searchVo = searchVo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDefectProfessional() {
		return defectProfessional;
	}
	public void setDefectProfessional(String defectProfessional) {
		this.defectProfessional = defectProfessional;
	}
	public Integer getProcessType() {
		return processType;
	}
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}
	public Integer getAchievement() {
		return achievement;
	}
	public void setAchievement(Integer achievement) {
		this.achievement = achievement;
	}
	public String getEquipments() {
		return equipments;
	}
	public void setEquipments(String equipments) {
		this.equipments = equipments;
	}
	public Integer getIsForkGroup() {
		return isForkGroup;
	}
	public void setIsForkGroup(Integer isForkGroup) {
		this.isForkGroup = isForkGroup;
	}
	public String getAttachmentIds() {
		return attachmentIds;
	}
	public void setAttachmentIds(String attachmentIds) {
		this.attachmentIds = attachmentIds;
	}
	public String getWorkNo() {
		return workNo;
	}
	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}
	public String getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}
	
	
	
}
