package cn.com.qytx.hemei.cardispatch.domain;

import java.io.Serializable;


public class CarVo extends Car implements Serializable {

	/**
	 * 序列号
	 */
	private static final long serialVersionUID = 8059048983104101885L;

	//车辆类型
	private Integer carType;
	//车牌号
	private String carNo;
	//车辆品牌
	private String carPhone;
	//车辆型号
	private String carModel;
	//当前状态
	private Integer status;
	public Integer getCarType() {
		return carType;
	}
	public void setCarType(Integer carType) {
		this.carType = carType;
	}
	public String getCarNo() {
		return carNo;
	}
	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}
	
	public String getCarPhone() {
		return carPhone;
	}
	public void setCarPhone(String carPhone) {
		this.carPhone = carPhone;
	}
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
