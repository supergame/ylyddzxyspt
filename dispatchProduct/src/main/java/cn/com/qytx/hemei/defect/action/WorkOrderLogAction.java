package cn.com.qytx.hemei.defect.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.IWorkOrderLog;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

public class WorkOrderLogAction extends BaseActionSupport {
	private static final long serialVersionUID = -3819432110521482321L;
	
	private String instanceId;
	@Autowired
	private IWorkOrderLog workOrderLogService;
	
	public void workOrderLogList(){
		UserInfo user =  getLoginUser();
		if(user!=null){
			try {
				List<WorkOrderLog> list = workOrderLogService.findWorkOrderLogListByInstanceId(instanceId);
				List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
				if(list!=null && list.size()>0){
					SimpleDateFormat sdfYYYYMMDDHHMM = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					for(WorkOrderLog wol:list){
						Map<String,Object> map = new HashMap<String, Object>();
						UserInfo people = wol.getOperUser();
						map.put("userName",StringUtils.isNoneBlank(people.getUserName())?people.getUserName():"--");//姓名
						map.put("workNo",StringUtils.isNoneBlank(people.getWorkNo())?people.getWorkNo():"--");//工号
						map.put("content",wol.getContent());
						map.put("insertTime", sdfYYYYMMDDHHMM.format(wol.getInsertTime()));
						listMap.add(map);
					}
				}
				ajax(listMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
	
    
	
	
	
}
