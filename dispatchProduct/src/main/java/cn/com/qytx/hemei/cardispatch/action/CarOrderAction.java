package cn.com.qytx.hemei.cardispatch.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cn.com.qytx.cbb.myapply.domain.MyProcessed;
import cn.com.qytx.cbb.myapply.service.IMyProcessed;
import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarOrder;
import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.hemei.cardispatch.service.ICar;
import cn.com.qytx.hemei.cardispatch.service.ICarOrder;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;

/**
 * 功能：车辆调度工单
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月7日
 * 修改日期：2017年9月7日	
 */
public class CarOrderAction extends BaseActionSupport{
	private static final long serialVersionUID = 6443441678076641166L;
	
	private final static Logger logger = Logger.getLogger(CarOrderAction.class);
	
	
	@Autowired
	private IGroup groupService;
	@Autowired
	private IUser userService;
	
	@Autowired
	private ICarOrder  carOrderService;
	
	@Autowired
	private ICar carService;
	
	/**
	 * 已办业务接口
	 */
	@Autowired
	private IMyProcessed myProcessImpl;
	
	/**
	 * 车辆调度工单参数
	 */
	private String carOrderMsg;
	/**
	 * 流程Id
	 */
	private String instanceId;
	
	/**
	 * 1  接诊科室  2 车管科
	 */
	private Integer groupType;
	
	/**
	 * 部门名称
	 */
	private String groupName;
	
	
	private String approveResult;
	/**
	 * 审批意见
	 */
	private String advice;
	/**
	 * 当前操作状态
	 */
	private String taskName;
	
	/**
	 * 下一步处理人
	 */
	private Integer nextUserId;
	/**
	 * 处理类型
	 */
	private Integer processType;
	
	/**
	 * 司机实体
	 */
	private Driver driver;
	/**
	 * 车辆实体
	 */
	private Car car;
	
	/**
	 * 功能：添加车辆工单
	 * @return
	 */
	public String addCarOrder(){
		logger.info("创建接口调用参数：carOrderMsg="+carOrderMsg);
		try{
			if(StringUtils.isEmpty(carOrderMsg)){
				ajax("101||车辆工单信息不能为空");
				return null;
			}
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
			CarOrder carOrder = gson.fromJson(carOrderMsg, CarOrder.class);
			if(carOrder == null){
				ajax("101||车辆工单信息不能为空");
				return null;
			}
			if(carOrder.getOrderType()==null){
				ajax("101||车辆工单类别不能为空");
				return null;
			}
			if(carOrder.getOrderType().intValue()==2 && StringUtils.isEmpty(carOrder.getCarContent())){
				ajax("101||用车内容不能为空");
				return null;
			}
			/*if(carOrder.getOrderType().intValue()==1 && carOrder.getReferralDepartment() == null){
				ajax("101||接诊科室信息不能为空");
				return null;
			}*/
			if(carOrder.getProcessId() == null){
				ajax("101||处理科室信息不能为空");
				return null;
			}
			if(StringUtils.isEmpty(carOrder.getStartLocation())){
				ajax("101||出发地点信息不能为空");
				return null;
			}
			if(StringUtils.isEmpty(carOrder.getArriveLocation())){
				ajax("101||到达地点信息不能为空");
				return null;
			}
			
			if(StringUtils.isEmpty(carOrder.getDispatchWorkNo())){
				ajax("101||调度人工号信息不能为空");
				return null;
			}
			if(carOrder.getProcessId().intValue()==61){//直接派遣
				if(carOrder.getCarId() == null){
					ajax("101||车辆信息不能为空");
					return null;
				}
				if(carOrder.getDriverId() == null){
					ajax("101||驾驶员信息不能为空");
					return null;
				}
				carOrder.setProcessType(1);
				instanceId = carOrderService.dispatchAddCarOrder(carOrder);
			}else{
				carOrder.setProcessType(2);
				instanceId = carOrderService.addCarOrder(carOrder);
			}
			ajax("100||"+instanceId);
		}catch(Exception e){
			ajax("102||操作失败!");
			logger.info("车辆调度工单添加失败,失败信息:"+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 功能：添加车辆工单 直接派遣
	 * @return
	 */
	public String dispatchCarOrder(){
		logger.info("创建接口调用参数：carOrderMsg="+carOrderMsg);
		try{
			if(StringUtils.isEmpty(carOrderMsg)){
				ajax("101||车辆工单信息不能为空");
				return null;
			}
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
			CarOrder carOrder = gson.fromJson(carOrderMsg, CarOrder.class);
			if(carOrder == null){
				ajax("101||车辆工单信息不能为空");
				return null;
			}
			if(carOrder.getOrderType()==null){
				ajax("101||车辆工单类别不能为空");
				return null;
			}
			if(carOrder.getOrderType().intValue()==2 && StringUtils.isEmpty(carOrder.getCarContent())){
				ajax("101||用车内容不能为空");
				return null;
			}
			/*if(carOrder.getOrderType().intValue()==1 && carOrder.getReferralDepartment() == null){
				ajax("101||接诊科室信息不能为空");
				return null;
			}*/
			if(carOrder.getProcessId() == null){
				ajax("101||处理科室信息不能为空");
				return null;
			}
			
			if(carOrder.getCarId() == null){
				ajax("101||车辆信息不能为空");
				return null;
			}
			if(carOrder.getDriverId() == null){
				ajax("101||驾驶员信息不能为空");
				return null;
			}
			if(StringUtils.isEmpty(carOrder.getStartLocation())){
				ajax("101||出发地点信息不能为空");
				return null;
			}
			if(StringUtils.isEmpty(carOrder.getCarPhone())){
				ajax("101||车辆号码信息不能为空");
				return null;
			}
			if(StringUtils.isEmpty(carOrder.getArriveLocation())){
				ajax("101||到达地点信息不能为空");
				return null;
			}
			
			if(StringUtils.isEmpty(carOrder.getDispatchWorkNo())){
				ajax("101||调度人工号信息不能为空");
				return null;
			}
			carOrder.setProcessType(1);
			String instanceId = carOrderService.dispatchAddCarOrder(carOrder);
			ajax("100||"+instanceId);
		}catch(Exception e){
			ajax("102||操作失败!");
			logger.info("车辆调度工单添加失败,失败信息:"+e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 功能：获取科室接口  groupType    1 车管科 2 临床
	 * @return
	 */
	public String findGruopListByType(){
		logger.info("调度查询科室接口,查询类型：groupType:"+groupType);
		try{
			List<GroupInfo> groupList = new ArrayList<GroupInfo>();
			String hql = " isDelete=0 and companyId=1 and groupType=1";
			if(StringUtils.isNoneBlank(groupName)){
				hql += " and groupName like '%"+groupName+"%'";
			}
			if(groupType!=null&&groupType.intValue()==1){
				hql += " and branch='1'";
				GroupInfo dispatchGroup = groupService.findOne(61);
				groupList.add(dispatchGroup);
				List<GroupInfo> tempGroupList = groupService.findAll(hql);
				groupList.addAll(tempGroupList);
			}else{
				hql += " and extension='3'";
				groupList = groupService.findAll(hql);
				
			}
			Gson gson = new Gson();
			ajax(gson.toJson(groupList));
			
		}catch(Exception e){
			e.printStackTrace();
			logger.info("调度查询科室接口,失败原因:"+e.getMessage());
		}
		return null;
	}
	
	
	/**
	 * 功能：车辆调度工单流转信息
	 * @return
	 */
	public String findHistoryList(){
		logger.info("调用车辆流转信息接口,参数：instanceId="+instanceId);
		try{
			List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
			//处理历史
			List<MyProcessed> historyList= myProcessImpl.findByInstanceId(instanceId);
			String statusName = "";
			String carNo = "";
			String carPhone="";
			String driverName = "";
			String driverPhone = "";
			String driverWorkNo = "";
			SimpleDateFormat sdfFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			for(MyProcessed m:historyList){
				UserInfo userInfo=userService.findOne(m.getProcesserId());
				if(userInfo!=null){
					m.setWorkNo(StringUtils.isNoneBlank(userInfo.getWorkNo())?userInfo.getWorkNo():"--");
				}
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("processName", userInfo!=null?userInfo.getUserName():"--");//处理人名称
				map.put("workNo", userInfo!=null?userInfo.getWorkNo():"--");//处理人工号
				String taskName = m.getTaskName();
				if("0".equals(taskName)){
					statusName = "派车工单";
				}
				if("1".equals(taskName)){
					statusName = "已派遣";
					UserInfo driverUser=userService.findOne(m.getNextProcesserId());
					if(driverUser!=null){
						driverPhone = driverUser.getPhone();
						driverName = driverUser.getUserName();
						driverWorkNo = driverUser.getWorkNo();
					}
					if(m.getCarId()!=null){
						Car car=carService.findOne(m.getCarId());
						carNo = car!=null?car.getCarNo():"-";
						carPhone=car!=null?car.getCarPhone():"-";
					}
				}
				if("2".equals(taskName)){
					statusName = "已发车";
				}
				if("3".equals(taskName)){
					statusName = "已回场";
				}
				if("5".equals(taskName)){
					statusName = "确认到场";
				}
				if("6".equals(taskName)){
					statusName = "派遣撤销";
				}
				if("7".equals(taskName)){
					statusName = "已到达";
				}
				map.put("taskName", taskName);
				map.put("carNo", carNo);
				map.put("carPhone", carPhone);
				map.put("driverName", driverName);
				map.put("driverPhone", driverPhone);
				map.put("driverWorkNo", driverWorkNo);
				map.put("statusName", statusName);
				map.put("operTime", m.getEndTime()!=null?sdfFormat.format(m.getEndTime()):"-");
				mapList.add(map);
			}
			Gson gson = new Gson();
			ajax(gson.toJson(mapList));
		}catch(Exception e){
			logger.info("调用车辆流转信息接口失败,失败原因:"+e.getMessage());
		}
		return null;
	}
	
	
	/**
	 * 处理操作
	 * @Title: approve   
	 */
	public void approve(){
		logger.info("调度车辆处理工单接口,参数：instanceId:"+instanceId+",taskName:"+taskName+",nextUserId:"+nextUserId+",driver:"+driver+",car:"+car);
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				CarOrder carOrder=carOrderService.findByInstanceId(instanceId);
				if("1".equals(taskName)){
					if(car==null || car.getId()==null){
						ajax("车辆Id不能为空!");
						return;
					}
					if(driver==null || driver.getUserId()==null){
						ajax("司机Id不能为空!");
						return;
					}
					if(carOrder.getStatus()>=2){
						ajax("任务已派遣,不能重复派遣！");
						return;
					}
					
				}
				if("2".equals(taskName)){//发车
					if(carOrder.getStatus()>=3){
						ajax("任务已发车,不能重复发车！");
						return;
					}
					Driver driver = carOrder.getDriver();
					nextUserId = (driver!=null?driver.getUserId():userInfo.getUserId());
				}
				
				/**
				 * 到达
				 */
				if("7".equals(taskName)){//到达
					if(carOrder.getStatus()==7 ||carOrder.getStatus()==4){
						ajax("任务已到达,不能重复到达！");
						return;
					}
					nextUserId = (driver!=null?driver.getUserId():userInfo.getUserId());
				}
				
				
				/**
				 * 回场
				 */
				if("3".equals(taskName)){//回场
					if(carOrder.getStatus()==4){
						ajax("任务已回场,不能重复回场！");
						return;
					}
				}
				/**
				 * 接收工单,不需要走流程
				 */
				if("4".equals(taskName)){
					carOrder.setIsAccept(1);
					carOrderService.saveOrUpdate(carOrder);
					ajax("1");
					return;
				}
				if("6".equals(taskName)){
					if(carOrder.getStatus()==1){
						ajax("任务已撤销,不能重复撤销！");
						return;
					}
				}
				int res=carOrderService.approve(userInfo.getUserId(), instanceId, approveResult, advice, taskName, nextUserId,processType,driver,car);
				if(res==1){
					ajax("1");
				}else{
					ajax("系统繁忙,请稍后重试!");
				}
			}
		}catch(Exception e){
			logger.info("车辆调度工单处理失败,失败原因:"+e.getMessage());
			e.printStackTrace();
			ajax("系统繁忙,请稍后重试!");
		}
	}

	public IUser getUserService() {
		return userService;
	}

	public String getCarOrderMsg() {
		return carOrderMsg;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public Integer getGroupType() {
		return groupType;
	}

	public String getApproveResult() {
		return approveResult;
	}

	public String getAdvice() {
		return advice;
	}

	public String getTaskName() {
		return taskName;
	}

	public Integer getNextUserId() {
		return nextUserId;
	}

	public Integer getProcessType() {
		return processType;
	}

	public Driver getDriver() {
		return driver;
	}

	public Car getCar() {
		return car;
	}

	public void setUserService(IUser userService) {
		this.userService = userService;
	}

	public void setCarOrderMsg(String carOrderMsg) {
		this.carOrderMsg = carOrderMsg;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public void setGroupType(Integer groupType) {
		this.groupType = groupType;
	}

	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public void setNextUserId(Integer nextUserId) {
		this.nextUserId = nextUserId;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	
	
}



