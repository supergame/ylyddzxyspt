package cn.com.qytx.hemei.location.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;

@Entity
@Table(name="tb_location")
public class Location extends BaseDomain{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	/**
	 * 地点名称
	 */
	@Column(name="location_name")
	private String locationName;
	
	/**
	 * 级别
	 */
	@Column(name="grade")
	private Integer grade;
	
	
	/**
	 * 父ID
	 */
	@Column(name="parent_id")
	private Integer parentId;
		
	/**
	 * 排序
	 */
	@Column(name="order_index")
	private Integer orderIndex;
	
	
	@DeleteState
	@Column(name="is_delete")
	private Integer isDelete;
	
	
	@Column(name="path")
	private String path;
	
	
	@Column(name="create_user_id")
	private Integer createUserId;
	
	@Column(name="create_time")
	private Timestamp createTime=new Timestamp(System.currentTimeMillis());
	
	@Column(name="update_time")
	private Timestamp updateTime=new Timestamp(System.currentTimeMillis());
	
	@Column(name="update_user_id")
	private Integer updateUserId;

	@Transient
	private String pathName;
	
	public Integer getId() {
		return id;
	}

	public String getLocationName() {
		return locationName;
	}

	public Integer getGrade() {
		return grade;
	}

	public Integer getParentId() {
		return parentId;
	}

	public Integer getOrderIndex() {
		return orderIndex;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public String getPath() {
		return path;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}
	
	
	
	
}
