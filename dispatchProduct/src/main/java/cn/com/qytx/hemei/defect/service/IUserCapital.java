package cn.com.qytx.hemei.defect.service;

import java.util.List;

import cn.com.qytx.hemei.defect.domain.UserCapital;
import cn.com.qytx.platform.base.service.BaseService;

public interface IUserCapital extends BaseService<UserCapital>{

	
	public List<UserCapital> findUserCapitalListByInstanceId(String instanceId);
	
}
