package cn.com.qytx.hemei.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import cn.com.qytx.platform.org.domain.CompanyInfo;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.ModuleInfo;
import cn.com.qytx.platform.org.domain.RoleInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.ICompany;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IModule;
import cn.com.qytx.platform.org.service.IRole;
import cn.com.qytx.platform.org.service.IUser;

public class UserDetailServiceImpl implements UserDetailsService {

	@Resource
	private IUser userService;
	@Resource
	private ICompany companyService;
	@Resource
	private IRole roleService;
	@Resource
	private IModule moduleService;
	
	@Resource
	private IGroup groupService;
	
	@Override
	public UserDetails loadUserByUsername(String loginName)
			throws UsernameNotFoundException {
        ServletRequestAttributes attr = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request =attr.getRequest();
        request.getSession().setAttribute("loginFailure",null);
        
        if (loginName == null) {
            request.getSession().setAttribute("loginFailure","failure");
        }else{
        	Cookie[] cookies = request.getCookies();
        	Integer companyId = null;
        	if(cookies!=null){
				for(Cookie cookie : cookies){
	                 if(cookie.getName().equals("companyId")){
	                	 companyId = Integer.valueOf(cookie.getValue());
	                	 break;
	                 }
	             }
			}
        	if(companyId == null){
        		companyId = 1;
        	}
            UserInfo user=userService.findOne(" loginName=? and companyId=? and isDelete=0", loginName,companyId);
            List<UserInfo> list = userService.findAll(" loginName=? and companyId=? and isDelete=0", loginName,companyId);
            
            if(user==null){
            	request.getSession().setAttribute("loginFailure","failure");
            }else if (list!=null&&list.size()>1) {
            	request.getSession().setAttribute("loginFailure","loginNameRepeat");
			}else if(user.getUserState() == null || user.getUserState()>0){
            	request.getSession().setAttribute("loginFailure","loginForbid");
            }else{
                CompanyInfo companyInfo=companyService.findOne(user.getCompanyId());
                request.getSession().setAttribute("companyInfo",companyInfo);
                String roleIdArr="";
                List<RoleInfo> roleList =roleService.getRoleByUser(user.getUserId()); //根据人员Id获取角色列表
                if(roleList!=null)
                {
                    roleIdArr= getRoleIds(roleList);
                }
                
                //add by jiayq,如果是超级管理员用户，则有所有的菜单权限
                List<ModuleInfo> moduleList = null;
                if(user.getIsDefault()!=null&&user.getIsDefault() == 0 ){
                	moduleList = moduleService.findAll();
                }else{
                	moduleList =moduleService.getModuleByRole(roleIdArr);//获取模块列表
                }
                if(moduleList!=null&&!moduleList.isEmpty()){
                	List<ModuleInfo> newModuleList = new ArrayList<ModuleInfo>();
                	for(ModuleInfo moduleInfo:moduleList){
                		if("dispatch".equals(moduleInfo.getSysName())){
                			newModuleList.add(moduleInfo);
                		}
                	}
                	moduleList = newModuleList;
                }
                request.getSession().setAttribute("moduleList",moduleList);
                request.getSession().setAttribute("adminUser",user);
                GroupInfo groupInfo = groupService.findOne(user.getGroupId());
                request.getSession().setAttribute("groupInfo",groupInfo);
                userService.updateLastLoginTime(user.getUserId());
                Collection<SimpleGrantedAuthority> clist = new ArrayList<SimpleGrantedAuthority>();
                clist.add(new SimpleGrantedAuthority("LOGINUSER"));
                User userdetail = new User(
                        user.getLoginName(), user.getLoginPass(), clist);
                return userdetail;
            }
        }
        return new org.springframework.security.core.userdetails.User(
        		loginName, "", true, true, true, true, new ArrayList<GrantedAuthority>());
	}
    /**
     * 获取角色id列表
     * @param roleList
     * @return
     */
    private String getRoleIds(final List<RoleInfo> roleList)
    {
        StringBuffer roleIdArr= new StringBuffer();
        String result = "";
        for(RoleInfo role:roleList)
        {
            roleIdArr.append(role.getRoleId()+",");
        }
        if(roleIdArr.toString().endsWith(","))
        {
            result=roleIdArr.substring(0,roleIdArr.length()-1);
        }
        return result;
    }
}
