package cn.com.qytx.hemei.report.send.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.service.ISendReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能: 班组工作量统计
 * 版本: 1.0
 * 开发人员: 
 * 创建日期: 2017年5月15日
 * 修改日期: 2017年5月15日
 * 修改列表: 
 */
public class SendReportAction extends BaseActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3877820495033775999L;

	@Autowired
	private ISendReport sendReportService;
	
	private String beginTime;//开始时间
	private String endTime;//结束时间
	private Integer type;// 1:配送 2:归还
	
	/**
	 * 获得部门统计
	 */
	public void findGroupSend(){
		UserInfo user= getLoginUser();
		if(user!=null){
			List<Object[]> list = sendReportService.findTakeWorkList(beginTime,endTime,type);
			Map<String,Integer> takeWorkMap=new HashMap<String, Integer>();
			String instanceIds="";
			if(list!=null && list.size()>0){
				int i=1;
				for(Object[] obj:list){
					instanceIds+="'"+(String)obj[0]+"',";
					Integer groupId=(Integer)obj[1];
					String groupName=(String)obj[2];
					if(takeWorkMap.containsKey(String.valueOf(groupId)+"_"+groupName)){
						Integer num=takeWorkMap.get(String.valueOf(groupId)+"_"+groupName);
						takeWorkMap.put(String.valueOf(groupId)+"_"+groupName, num+1);
					}else{
						takeWorkMap.put(String.valueOf(groupId)+"_"+groupName, i);
					}
				}
				instanceIds=instanceIds.substring(0, instanceIds.length()-1);
			}
			list.clear();
			for (Map.Entry<String, Integer> entry : takeWorkMap.entrySet()) {  
				Object[] array=new Object[3];
				String[] keyArray=entry.getKey().split("_");
				array[0]=keyArray[1];
				array[1]=entry.getValue();
				array[2]=Integer.valueOf(keyArray[0]);
				list.add(array);
			} 
			Map<Integer, String> map = sendReportService.findMap(beginTime, endTime,type,instanceIds);
			List<Map<String,Object>> listMap= analyzeResult(list,map);
			ajax(listMap);
		}
		  
	}
	
	/**
     * 
     * @param 
     * @return
     */
    private List<Map<String, Object>>  analyzeResult(List<Object[]> list,Map<Integer, String> lisMap){
    	List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
    	DecimalFormat df =new DecimalFormat("0.0");
    	if(list!=null&&list.size()>0){
    		//合计
			Integer sumFinish=0;
			Integer sumNoFinish=0;
			Integer sumn=0;
			Integer sumTakeWorkNum=0;
			Integer sumEquipmentNum = 0;
			int i = 1;
			for(Object[] obj:list){
				Map<String,Object> map = new HashMap<String, Object>();
				map.put("no",i);
				map.put("name",obj[0].toString());
				map.put("sumTakeWorkNum",obj[1]==null?0:Integer.valueOf(obj[1].toString()));//应接工量
				Integer sumNum =0;
				Integer finish =0;
				Integer equipmentNum = 0;
				Integer groupId=(Integer)obj[2];
				if(lisMap!=null && !lisMap.isEmpty()){
					if(lisMap.containsKey(groupId)){
						String num=lisMap.get(groupId);
						String arr[] = num.split("-");
						sumNum =Integer.valueOf(arr[0]);
						finish =Integer.valueOf(arr[1]);
						equipmentNum =Integer.valueOf(arr[2]);
					}
				}
				map.put("sumNum",sumNum);//接工量
				map.put("finish",finish);//完工量
				map.put("noFinish",sumNum-finish);
				String finishPer="0.0";
				if(sumNum!=0){
					finishPer=df.format((double)finish/sumNum*100 );
				}
				map.put("finishPer",finishPer);//完工率
				map.put("equipmentNumber",equipmentNum);//接工量
				mapList.add(map);
				sumn+=sumNum;
				sumNoFinish+=(sumNum-finish);
				sumFinish+=finish;
				sumEquipmentNum+=equipmentNum;
				sumTakeWorkNum+=obj[1]==null?0:Integer.valueOf(obj[1].toString());
				i++;
			}
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("no","合计");
			map.put("name","");
			map.put("sumNum",sumn);
			map.put("finish",sumFinish);
			map.put("equipmentNumber",sumEquipmentNum);
			map.put("noFinish",sumNoFinish);
			map.put("sumTakeWorkNum",sumTakeWorkNum);//应接工量
			String finishPer="0.0";
			if(sumn!=0){
				finishPer=df.format((double)sumFinish/sumn*100 );
			}
			map.put("finishPer",finishPer);//完工率
			mapList.add(map);
    	}
    	return mapList;
    }
	
	/**
	 * 获得图表数据
	 */
	public void findGroupChart(){
		UserInfo user = getLoginUser();
		if(user!=null){
			List<Object[]> list = sendReportService.findTakeWorkList(beginTime,endTime,type);
			Map<String,Integer> takeWorkMap=new HashMap<String, Integer>();
			String instanceIds="";
			if(list!=null && list.size()>0){
				int i=1;
				for(Object[] obj:list){
					instanceIds+="'"+(String)obj[0]+"',";
					Integer groupId=(Integer)obj[1];
					String groupName=(String)obj[2];
					if(takeWorkMap.containsKey(String.valueOf(groupId)+"_"+groupName)){
						Integer num=takeWorkMap.get(String.valueOf(groupId)+"_"+groupName);
						takeWorkMap.put(String.valueOf(groupId)+"_"+groupName, num+1);
					}else{
						takeWorkMap.put(String.valueOf(groupId)+"_"+groupName, i);
					}
				}
				instanceIds=instanceIds.substring(0, instanceIds.length()-1);
			}
			list.clear();
			for (Map.Entry<String, Integer> entry : takeWorkMap.entrySet()) {  
				Object[] array=new Object[3];
				String[] keyArray=entry.getKey().split("_");
				array[0]=keyArray[1];
				array[1]=entry.getValue();
				array[2]=Integer.valueOf(keyArray[0]);
				list.add(array);
			} 
			Map<Integer, String> listMap = sendReportService.findMap(beginTime, endTime,type,instanceIds);
			List<String> xname=new ArrayList<String>();
			List<Integer> finish=new ArrayList<Integer>();
			List<Integer> nofinsh=new ArrayList<Integer>();
			List<Integer> sum=new ArrayList<Integer>();
			List<Integer> takeWork=new ArrayList<Integer>();
			String[] completeName={"应接工量","接工量","完工量","未完工量"};
			if(list!=null&& list.size()>0){
				for(Object[] obj:list){
					xname.add(obj[0].toString());
					takeWork.add(obj[1]==null?0:Integer.valueOf(obj[1].toString()));//应接工数
					Integer groupId=(Integer)obj[2];
					if(listMap!=null && !listMap.isEmpty()){
						if(listMap.containsKey(groupId)){
							String num=listMap.get(groupId);
							sum.add(Integer.valueOf(num.split("-")[0]));
							finish.add(Integer.valueOf(num.split("-")[1]));
							nofinsh.add(Integer.valueOf(num.split("-")[0])-Integer.valueOf(num.split("-")[1]));
						}
					}
				}
			}
			String[] xnameArray= new String[xname.size()];
			xname.toArray(xnameArray);
			Integer[]finishArray= new Integer[finish.size()];
			finish.toArray(finishArray);
			Integer[]noFinishArray= new Integer[nofinsh.size()];
			nofinsh.toArray(noFinishArray);
			Integer[]sumArray= new Integer[sum.size()];
			sum.toArray(sumArray);
			Integer[]takeWorkArray= new Integer[takeWork.size()];
			takeWork.toArray(takeWorkArray);
			Map<String,Object> map =new HashMap<String, Object>();
			map.put("name", xnameArray);
			map.put("completeName", completeName);
			map.put("finishArray", finishArray);
			map.put("noFinishArray", noFinishArray);
			map.put("takeWorkArray", takeWorkArray);
			map.put("sumArray", sumArray);
			ajax(map);
		}
	}
	
	/*
	 * 导出班组工作量
	 */
	public void export(){
		HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				String excelName = "";
				if(type==1){
					excelName = "班组配送工作量";
				}else if(type==2){
					excelName = "班组归还工作量";
				}
				List<Object[]> list = sendReportService.findTakeWorkList(beginTime,endTime,type);
				Map<String,Integer> takeWorkMap=new HashMap<String, Integer>();
				String instanceIds="";
				if(list!=null && list.size()>0){
					int i=1;
					for(Object[] obj:list){
						instanceIds+="'"+(String)obj[0]+"',";
						Integer groupId=(Integer)obj[1];
						String groupName=(String)obj[2];
						if(takeWorkMap.containsKey(String.valueOf(groupId)+"_"+groupName)){
							Integer num=takeWorkMap.get(String.valueOf(groupId)+"_"+groupName);
							takeWorkMap.put(String.valueOf(groupId)+"_"+groupName, num+1);
						}else{
							takeWorkMap.put(String.valueOf(groupId)+"_"+groupName, i);
						}
					}
					instanceIds=instanceIds.substring(0, instanceIds.length()-1);
				}
				list.clear();
				for (Map.Entry<String, Integer> entry : takeWorkMap.entrySet()) {  
					Object[] array=new Object[3];
					String[] keyArray=entry.getKey().split("_");
					array[0]=keyArray[1];
					array[1]=entry.getValue();
					array[2]=Integer.valueOf(keyArray[0]);
					list.add(array);
				} 
				Map<Integer, String> map = sendReportService.findMap(beginTime, endTime,type,instanceIds);
				List<Map<String,Object>> listMap= analyzeResult(list,map);
				String fileName = URLEncoder.encode(excelName+".xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName(excelName);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}		
		
		private List<String> getExportHeadList(){
	        List<String> headList = new ArrayList<String>();
	        headList.add("序号");
	        headList.add("班组名称");
	        headList.add("接工量");
	        headList.add("完工量");
	        headList.add("配送设备量");
	        headList.add("未完工量");
	        headList.add("完工率");
	        return headList;
	    }
	    
	    private List<String> getExportKeyList(){
	        List<String> headList = new ArrayList<String>();
	        headList.add("no");
	        headList.add("name");
	        headList.add("sumNum");
	        headList.add("finish");
	        headList.add("equipmentNumber");
	        headList.add("noFinish");
	        headList.add("finishPer");
	        return headList;
	    }
	  
		public String getBeginTime() {
			return beginTime;
		}
		public void setBeginTime(String beginTime) {
			this.beginTime = beginTime;
		}
		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}

		public Integer getType() {
			return type;
		}

		public void setType(Integer type) {
			this.type = type;
		}
	    
}
	
	