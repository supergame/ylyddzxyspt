package cn.com.qytx.hemei.util;


import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.hemei.defect.service.IWorkOrderLog;
import cn.com.qytx.platform.log.domain.Log;
import cn.com.qytx.platform.log.service.ILog;
/**
 * 功能:保存日志
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月16日
 * 修改日期: 2016年8月16日
 * 修改列表: 
 */
@Component
@Transactional
public class AddWorkOrderLogListener implements ApplicationListener<EventForAddWorkOrderLog>{

	@Autowired
	private IWorkOrderLog workOrderLogService;
	
	@Override
	public void onApplicationEvent(EventForAddWorkOrderLog event) {
		if(event.getSource() instanceof WorkOrderLog){
			WorkOrderLog log = (WorkOrderLog)event.getSource();
			log.setInsertTime(new Timestamp(System.currentTimeMillis()));
			log.setIsDelete(0);
			workOrderLogService.saveOrUpdate(log);
		}
	}

}
