package cn.com.qytx.hemei.location.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.hemei.location.dao.LocationDao;
import cn.com.qytx.hemei.location.domain.Location;
import cn.com.qytx.hemei.location.service.ILocation;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能:资产组接口实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
@Service("locationService")
@Transactional
public class LocationImpl extends BaseServiceImpl<Location> implements ILocation{

	@Resource(name="locationDao")
	private LocationDao locationDao;
	
	@Override
	public List<Location> findLocationList(Integer companyId) {
		return locationDao.findLocationList(companyId);
	}

	@Override
	public List<TreeNode> getTreeLocationList(String path,Integer companyId,Integer isOpen) {
		List<Location> cgList = this.findLocationList(companyId);
		Map<Integer,String> locationMap = new HashMap<Integer, String>();
		if(cgList!=null&&!cgList.isEmpty()){
			for(Location location:cgList){
				locationMap.put(location.getId(), location.getLocationName());
			}
			
		}
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("地点维护");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen((isOpen!=null&&isOpen.intValue()==1)?true:false);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(Location cg:cgList){
				TreeNode treeNode = new TreeNode();
				treeNode.setId("gid_"+cg.getId());
				treeNode.setName(cg.getLocationName());
				treeNode.setPId("gid_"+cg.getParentId());
				//构造全路径
				String tempPath = cg.getPath();
				if(tempPath.startsWith(",")){
					tempPath = tempPath.substring(1);
				}
				if(tempPath.endsWith(",")){
					tempPath = tempPath.substring(0,tempPath.length()-1);
				}
				String pathArr[] = tempPath.split(",");
				String pathName = "";
				for(String pathId:pathArr){
					Integer pathIdInt = Integer.valueOf(pathId);
					if(locationMap.containsKey(pathIdInt)){
						pathName += locationMap.get(pathIdInt);
					}
				}
				//String pathName = locationDao.getLocationPathNameById(cg.getId());
				treeNode.setObj(cg.getOrderIndex()+"{1}"+pathName);// 排序号
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	private Map<Integer,Integer> getSubListCountMap(List<Location> cgList){
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		for(Location cg:cgList){
			Integer parentId = cg.getParentId();
			if(map.containsKey(parentId)){
				map.put(parentId, map.get(parentId)+1);
			}else{
				map.put(parentId, 1);
			}
		}
		
		return map;
	}
	

	@Override
	public List<TreeNode> getTreeLocationList_Select(String path,
			Integer companyId) {
		List<Location> cgList = this.findLocationList(companyId);
		Map<Integer,String> locationMap = new HashMap<Integer, String>();
		if(cgList!=null&&!cgList.isEmpty()){
			for(Location location:cgList){
				locationMap.put(location.getId(), location.getLocationName());
			}
			
		}
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("地点维护");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen(true);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(Location cg:cgList){
				TreeNode treeNode = new TreeNode();
				treeNode.setId("gid_"+cg.getId());
				treeNode.setName(cg.getLocationName());
				treeNode.setPId("gid_"+cg.getParentId());
				//构造全路径
				String tempPath = cg.getPath();
				if(tempPath.startsWith(",")){
					tempPath = tempPath.substring(1);
				}
				if(tempPath.endsWith(",")){
					tempPath = tempPath.substring(0,tempPath.length()-1);
				}
				String pathArr[] = tempPath.split(",");
				String pathName = "";
				for(String pathId:pathArr){
					Integer pathIdInt = Integer.valueOf(pathId);
					if(locationMap.containsKey(pathIdInt)){
						pathName += locationMap.get(pathIdInt);
					}
				}
				//String pathName = locationDao.getLocationPathNameById(cg.getId());
				treeNode.setObj(cg.getOrderIndex()+"{1}"+pathName);// 排序号
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	@Override
	public boolean isHasSameLocationName(Integer parentId, String groupName,
			int companyId) {
		return locationDao.isHasSameLocationName(parentId, groupName, companyId);
	}


	@Override
	public boolean isHasChildLocation(Integer id, int companyId) {
		return locationDao.isHasChildLocation(id,companyId);
	}

	@Override
	public List<Location> getLocationListByIds(String Ids) {
		return locationDao.getLocationListByIds(Ids);
	}

	@Override
	public List<Location> getLocationPathListByIds(String Ids,Integer companyId) {
		List<Location> newLocationList = new ArrayList<Location>();
		if(StringUtils.isNotBlank(Ids)){
			List<Location> allLocation = locationDao.findLocationList(companyId);
			Map<Integer,Location> locationMap = new HashMap<Integer, Location>();
			if(allLocation!=null&&!allLocation.isEmpty()){
				for(Location location:allLocation){
					locationMap.put(location.getId(), location);
				}
				
			}
			String idsArr[] = Ids.split(",");
			Location location = null;
			for(String id:idsArr){
				Integer locationId = Integer.parseInt(id);
				if(locationMap.containsKey(locationId)){
					location = locationMap.get(locationId);
					String path = location.getPath();
					if(path.startsWith(",")){
						path = path.substring(1);
					}
					if(path.endsWith(",")){
						path = path.substring(0,path.length()-1);
					}
					String pathArr[] = path.split(",");
					String pathName = "";
					for(String pathId:pathArr){
						Integer pathIdInt = Integer.valueOf(pathId);
						if(locationMap.containsKey(pathIdInt)){
							pathName += locationMap.get(pathIdInt).getLocationName();
						}
					}
					location.setPathName(pathName);
					newLocationList.add(location);
				}
			}
		}
		return newLocationList;
	}

}
