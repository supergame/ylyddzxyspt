package cn.com.qytx.hemei.duty.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.dict.domain.Dict;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.duty.domain.WorkDuty;
import cn.com.qytx.hemei.duty.service.IWorkDutyService;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * 功能:值班 版本: 1.0 开发人员: 潘博  创建日期: 2016年5月17日 修改日期: 2016年5月17日 修改列表:
 */
public class WorkDutyWapAction extends BaseActionSupport {
	/**
	 * 描述含义
	 */
	private static final long serialVersionUID = 1L;
	/** 值班impl */
	@Autowired
	IWorkDutyService workDutyImpl;
	
	@Autowired
	private IUser userService;
	
	@Autowired
	private IDict dictService;
	
	private String dutyDate;
	
	private Integer userId;//用户Id
	
	private Integer dutyDepartmentId;

	public String getDutyList() {
		try {
			if(userId!=null){
				UserInfo userInfo = userService.findOne(userId);
				Date workDate=new Date();
				if(StringUtils.isNotBlank(dutyDate)){
					workDate=StrToDate(dutyDate);
				}
				List<Dict> dictList = dictService.findListByInfoType("dutyDepartment");
				List<Map<String,Object>> mapList = new ArrayList<Map<String,Object>>();
				/** 得到结果 */
				List<WorkDuty> workDutyList = workDutyImpl.getWorkDutyByWorkDate(workDate);
				Map<Integer,List<WorkDuty>> workDutyMap = new HashMap<Integer, List<WorkDuty>>();
				if(workDutyList!=null&&!workDutyList.isEmpty()){
					List<WorkDuty> wdList = null;
					for(WorkDuty duty:workDutyList){
						Integer dutyDepId = Integer.parseInt(duty.getDutyPost());
						if(workDutyMap.containsKey(dutyDepId)){
							wdList = workDutyMap.get(dutyDepId);
						}else{
							wdList = new ArrayList<WorkDuty>();
						}
						UserInfo dutyUser = duty.getCreateUser();
						if(dutyUser!=null){
							duty.setDutyPersons(dutyUser.getUserName());
							duty.setDutyPhone(dutyUser.getPhone());
							duty.setIsOwn(dutyUser.getUserId().intValue()==userInfo.getUserId().intValue()?1:0);
							//duty.setCreateUser(null);
						}
						wdList.add(duty);
						workDutyMap.put(dutyDepId, wdList);
					}
				}
				for(Dict dict:dictList){
					Map<String,Object> subMap = new HashMap<String, Object>();
					subMap.put("dutyDepartment", dict.getName());
					Integer dictVal = dict.getValue();
					if(workDutyMap.containsKey(dictVal)){
						subMap.put("dutyList", workDutyMap.get(dictVal));
					}
					mapList.add(subMap);
				}
				Gson gson=new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd").create();
				ajax("100||"+gson.toJson(mapList));
			}else{
				ajax("101||参数缺少");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
		return null;
	}

	
	public String todayWorkDuty(){
		try{
			if(userId!=null){
				Map<String,Object> map = new HashMap<String, Object>();
				WorkDuty todayWd = workDutyImpl.getWorkDutyByUserId(new Date(), userId);
				String userName = "";
				map.put("isDuty", todayWd!=null?"1":"0");
				if(todayWd==null){
					UserInfo userInfo = userService.findOne(userId);
					if(userInfo!=null){
						userName = userInfo.getUserName();
					}
				}else{
					userName = todayWd.getCreateUser().getUserName();
				}
				map.put("userName", userName);
				Gson gson = new Gson();
				ajax("100||"+gson.toJson(map));
			}else{
				ajax("101||参数缺少");
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
		return null;
	}
	
	/**
	 * 功能：值班
	 * @return
	 */
	public String saveWorkDuty(){
		try{
			if(userId == null){
				ajax("101||参数异常");
				return null;
			}
			if(dutyDepartmentId == null){
				ajax("101||参数异常");
				return null;
			}
			UserInfo userInfo = userService.findOne(userId);
			WorkDuty wd = new WorkDuty();
			wd.setCompanyId(userInfo.getCompanyId());
			wd.setCreateUser(userInfo);
			wd.setCreateTime(new Timestamp(System.currentTimeMillis()));
			wd.setDutyDate(new Date());
			wd.setDutyPost(String.valueOf(dutyDepartmentId));
			workDutyImpl.saveOrUpdate(wd);
			ajax("100||0");
		}catch(Exception e){
			ajax("102||程序异常");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 功能：值班
	 * @return
	 */
	public String deleteWorkDuty(){
		try{
			if(userId == null){
				ajax("101||参数异常");
				return null;
			}
			WorkDuty todayWd = workDutyImpl.getWorkDutyByUserId(new Date(), userId);
			workDutyImpl.delete(todayWd, true);
			ajax("100||0");
		}catch(Exception e){
			ajax("102||程序异常");
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 字符串转换成日期
	 * 
	 * @param str
	 * @return date
	 */
	public static Date StrToDate(String str) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 日期转换成字符串
	 * 
	 * @param str
	 * @return date
	 */
	public static String DateToStr(Date date) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String str = format.format(date);
		return str;
	}



	public String getDutyDate() {
		return dutyDate;
	}



	public void setDutyDate(String dutyDate) {
		this.dutyDate = dutyDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public Integer getDutyDepartmentId() {
		return dutyDepartmentId;
	}


	public void setDutyDepartmentId(Integer dutyDepartmentId) {
		this.dutyDepartmentId = dutyDepartmentId;
	}

	

}
