package cn.com.qytx.hemei.cardispatch.service;

import java.util.List;

import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;

public interface IDriver extends BaseService<Driver> {

	/**
	 * 查询司机
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	public Page<Driver>  findDriver(Pageable pageable, Integer companyId);

	/**
	 * 查询司机
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	public List<Driver>  findDriverName(Integer companyId,String serachKey);
	
}
