package cn.com.qytx.hemei.cardispatch.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;

/**
 * 功能：车辆调度工单表
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年9月7日
 * 修改日期：2017年9月7日	
 */
@Entity
@Table(name="tb_dispatch_car_order")
public class CarOrder extends BaseDomain implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 597476095646235525L;
  
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	/**
	 * 申请实例 规则 userId+yyyyMMddHHmmss
	 */
	@Column(name="instance_id")
	private String instanceId;
	
	/**
	 * 工单状态（1 待派遣（已受理） 2 待发车（已派遣） 3 待到达（已发车） 4 已回场（完结）    7已到达（待回场）
	 * 3 待到达（已发车）
	 */
	@Column(name="status")
	private Integer status;
	
	
	/**
	 * 是否确认到场  0 未确认 1 已确认
	 */
	@Column(name="confirm_factory")
	private Integer confirmFactory;
	
	/**
	 * 是否接收  0 未接收  1 接收
	 */
	@Column(name="is_accept")
	private Integer isAccept;
	
	/**
	 * 车辆类型（1 转诊车 2 急诊车） 
	 */
	@Column(name="car_type")
	private Integer carType;
	
	/**
	 * 处理科室Id
	 */
	@Column(name="process_department_id")
	private Integer processDepartmentId;
	
	/**
	 * 当前处理Id
	 */
	@Column(name="process_id")
	private Integer processId;
	
	@Column(name="process_name")
	private String processName;
	
	/**
	 * 处理权限（ 1 人员 2 部门 3 角色）
	 */
	@Column(name="process_type")
	private Integer processType;
	
	/**
	 * 上一步处理人Id
	 */
	@Column(name="previous_process_id")
	private Integer previousProcessId;
	
	/**
	 * 出发地点
	 */
	@Column(name="start_location")
	private String startLocation;
	
	
	/**
	 * 到达地点
	 */
	@Column(name="arrive_location")
	private String arriveLocation;
	
	/**
	 * 计划发车时间
	 */
	@Column(name="plan_start_time")
	private Timestamp planStartTime;
	
	/**
	 * 实际发车时间
	 */
	@Column(name="actual_start_time")
	private Timestamp actualStartTime;
	
	
	/**
	 * 工单类别 （1 医联体工单 2车辆派遣 ）
	 */
	@Column(name="order_type")
	private Integer orderType;
	
	/**
	 * 派遣类型(业务类型) 
	 * 1上转住院
	 * 2上转检查
	 * 3上转门诊
	 * 4下转住院
	 * 5急危重抢救
	 * 6标本收取 
	 * 7 转运病人 
	 * 8  会诊安排 
	 * 9物资调配
	 * 10 其他
	 */
	@Column(name="business_type")
	private Integer businessType;
	
	
	/**
	 * 用车内容
	 */
	@Column(name="car_content")
	private String carContent;
	
	/**
	 * 转诊单位
	 */
	@Column(name="company")
	private String company;
	
	/**
	 * 诊断
	 */
	@Column(name="diagnosis")
	private String diagnosis;
	
	
	/**
	 * 转诊科室
	 */
	@Column(name="referral_department")
	private Integer referralDepartment;

	
	/**
	 * 检查项目
	 */
	@Column(name="check_project")
	private String checkProject;
	
	
	/**
	 * 转诊医师
	 */
	@Column(name="referral_doctor")
	private String referralDoctor;
	
	
	/**
	 * 联系方式
	 */
	@Column(name="doctor_phone")
	private String doctorPhone;
	
	/**
	 * 转诊目的
	 */
	@Column(name="referral_purpose")
	private String referralPurpose;
	
	/**
	 * 入院时间
	 */
	@Column(name="inhospital_time")
	private Timestamp inhospitalTime;
	
	/**
	 * 转诊原因
	 */
	@Column(name="referral_reason")
	private String referralReason;
	
	
	/**
	 * 患者姓名
	 */
	@Column(name="patient_name")
	private String patientName;
	
	/**
	 * 病人体位  躺  / 坐
	 */
	@Column(name="position")
	private String position;
	
	
	/**
	 * 就诊时间
	 */
	@Column(name="visit_time")
	private Timestamp visitTime;
	
	
	/**
	 * 责任人
	 */
	@Column(name="person_liable")
	private String personLiable;
	

	/**
	 * 性别  0女 1 男
	 */
	@Column(name="patient_sex")
	private Integer patientSex;
	
	/**
	 * 年龄
	 */
	@Column(name="patient_age")
	private Integer patientAge;
	
	
	/**
	 * 身份证号
	 */
	@Column(name="patient_id_number")
	private String patientIdNumber;
	
	/**
	 * 车辆
	 */
	
	@JoinColumn(name="car_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private Car car;
	
	/**
	 * 司机
	 */
	@JoinColumn(name="driver_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private Driver driver;
	
	/**
	 * 调度人工号
	 */
	@Column(name="dispatch_work_no")
	private String dispatchWorkNo;
	
	
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete=0;
	
	
	@Column(name="update_user_id")
	private Integer updateUserId;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time")
	private Timestamp updateTime;
	
	/**
	 * 创建人
	 */
	@Column(name="create_user_id")
	private Integer createUserId;
	
	/**
	 * 创建日期
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	/**
	 * 回厂时间
	 */
	@Column(name="back_factory_time")
	private Timestamp backFactoryTime;
	
	/**
	 * 派遣时间
	 */
	@Column(name="dispatch_time")
	private Timestamp dispatchTime;
	/**
	 * 到达时间
	 */
	@Column(name="arrive_time")
	private Timestamp arriveTime;
	
	@Transient
	private Integer carId;
	
	@Transient
	private Integer driverId;
	
	@Transient
	private String carPhone;
	@Transient
	private Integer restSeatNum;  //车辆剩余数
	@Transient
	private Integer occupy;       //车辆担架是否被占用
	@Transient
	private Timestamp departureTime; //车辆发车时间
	

	public Integer getId() {
		return id;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public Integer getStatus() {
		return status;
	}

	public Integer getCarType() {
		return carType;
	}

	public Integer getProcessId() {
		return processId;
	}

	public String getProcessName() {
		return processName;
	}

	public Integer getProcessType() {
		return processType;
	}

	public Integer getPreviousProcessId() {
		return previousProcessId;
	}

	public String getStartLocation() {
		return startLocation;
	}

	public String getArriveLocation() {
		return arriveLocation;
	}

	public Timestamp getPlanStartTime() {
		return planStartTime;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public Integer getBusinessType() {
		return businessType;
	}

	public String getCompany() {
		return company;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public Integer getReferralDepartment() {
		return referralDepartment;
	}

	public String getCheckProject() {
		return checkProject;
	}

	public String getReferralDoctor() {
		return referralDoctor;
	}

	public String getDoctorPhone() {
		return doctorPhone;
	}

	public String getReferralPurpose() {
		return referralPurpose;
	}

	public Timestamp getInhospitalTime() {
		return inhospitalTime;
	}

	public String getReferralReason() {
		return referralReason;
	}

	public String getPatientName() {
		return patientName;
	}

	public Timestamp getVisitTime() {
		return visitTime;
	}

	public String getPersonLiable() {
		return personLiable;
	}

	public Integer getPatientAge() {
		return patientAge;
	}

	public String getPatientIdNumber() {
		return patientIdNumber;
	}


	public String getDispatchWorkNo() {
		return dispatchWorkNo;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setCarType(Integer carType) {
		this.carType = carType;
	}

	public void setProcessId(Integer processId) {
		this.processId = processId;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	public void setPreviousProcessId(Integer previousProcessId) {
		this.previousProcessId = previousProcessId;
	}

	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}

	public void setArriveLocation(String arriveLocation) {
		this.arriveLocation = arriveLocation;
	}

	public void setPlanStartTime(Timestamp planStartTime) {
		this.planStartTime = planStartTime;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public void setBusinessType(Integer businessType) {
		this.businessType = businessType;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public void setReferralDepartment(Integer referralDepartment) {
		this.referralDepartment = referralDepartment;
	}

	public void setCheckProject(String checkProject) {
		this.checkProject = checkProject;
	}

	public void setReferralDoctor(String referralDoctor) {
		this.referralDoctor = referralDoctor;
	}

	public void setDoctorPhone(String doctorPhone) {
		this.doctorPhone = doctorPhone;
	}

	public void setReferralPurpose(String referralPurpose) {
		this.referralPurpose = referralPurpose;
	}

	public void setInhospitalTime(Timestamp inhospitalTime) {
		this.inhospitalTime = inhospitalTime;
	}

	public void setReferralReason(String referralReason) {
		this.referralReason = referralReason;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public void setVisitTime(Timestamp visitTime) {
		this.visitTime = visitTime;
	}

	public void setPersonLiable(String personLiable) {
		this.personLiable = personLiable;
	}

	public void setPatientAge(Integer patientAge) {
		this.patientAge = patientAge;
	}

	public void setPatientIdNumber(String patientIdNumber) {
		this.patientIdNumber = patientIdNumber;
	}



	public void setDispatchWorkNo(String dispatchWorkNo) {
		this.dispatchWorkNo = dispatchWorkNo;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Car getCar() {
		return car;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}


	public Timestamp getBackFactoryTime() {
		return backFactoryTime;
	}

	public void setBackFactoryTime(Timestamp backFactoryTime) {
		this.backFactoryTime = backFactoryTime;
	}

	public Timestamp getActualStartTime() {
		return actualStartTime;
	}

	public void setActualStartTime(Timestamp actualStartTime) {
		this.actualStartTime = actualStartTime;
	}

	public Timestamp getDispatchTime() {
		return dispatchTime;
	}

	public void setDispatchTime(Timestamp dispatchTime) {
		this.dispatchTime = dispatchTime;
	}

	public Integer getIsAccept() {
		return isAccept;
	}

	public void setIsAccept(Integer isAccept) {
		this.isAccept = isAccept;
	}

	public Integer getProcessDepartmentId() {
		return processDepartmentId;
	}

	public Integer getPatientSex() {
		return patientSex;
	}

	public void setProcessDepartmentId(Integer processDepartmentId) {
		this.processDepartmentId = processDepartmentId;
	}

	public void setPatientSex(Integer patientSex) {
		this.patientSex = patientSex;
	}

	public Integer getConfirmFactory() {
		return confirmFactory;
	}

	public void setConfirmFactory(Integer confirmFactory) {
		this.confirmFactory = confirmFactory;
	}

	public String getCarContent() {
		return carContent;
	}

	public void setCarContent(String carContent) {
		this.carContent = carContent;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Timestamp getArriveTime() {
		return arriveTime;
	}

	public void setArriveTime(Timestamp arriveTime) {
		this.arriveTime = arriveTime;
	}

	public Integer getCarId() {
		return carId;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public String getCarPhone() {
		return carPhone;
	}

	public void setCarId(Integer carId) {
		this.carId = carId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public void setCarPhone(String carPhone) {
		this.carPhone = carPhone;
	}

	public Integer getRestSeatNum() {
		return restSeatNum;
	}

	public void setRestSeatNum(Integer restSeatNum) {
		this.restSeatNum = restSeatNum;
	}

	public Integer getOccupy() {
		return occupy;
	}

	public void setOccupy(Integer occupy) {
		this.occupy = occupy;
	}

	public Timestamp getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Timestamp departureTime) {
		this.departureTime = departureTime;
	}
	
	
	
	
}
