package cn.com.qytx.hemei.util.proxy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

/**
 * 
 * 单点登录的代理类
 * 
 * @author 严家俊
 *
 */
public class SinglePointLoginProxy
{
	
	/**
	 * 解密AES加密的数据
	 * @param tData    数据
	 * @param tKey     解密数据的KEY
	 * @return
	 */
	public static String decodeAESData(String tData,String tKey)
	{
		try
		{
			KeyGenerator pKeyGenerator = KeyGenerator.getInstance("AES");  
			pKeyGenerator.init(128);  
			Cipher        pCipher=Cipher.getInstance("AES/ECB/PKCS5Padding");
			pCipher.init(Cipher.DECRYPT_MODE,new SecretKeySpec(tKey.getBytes(), "AES"));  
			byte[]                bpBuffer =getHexBytes(tData);
			bpBuffer=pCipher.doFinal(bpBuffer);
			tData=new String(bpBuffer,"utf-8");
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
		return tData;
	}
	
	/**
	 * 使用AES加密数据
	 * @param tData  数据
	 * @param tKey   加密数据的KEY
	 * @return
	 */
	public static String encodeAESData(String tData,String tKey)
	{
		try
		{
			KeyGenerator pKeyGenerator = KeyGenerator.getInstance("AES");  
			pKeyGenerator.init(128);  
			Cipher        pCipher=Cipher.getInstance("AES/ECB/PKCS5Padding");
			pCipher.init(Cipher.ENCRYPT_MODE,new SecretKeySpec(tKey.getBytes(), "AES"));  
			byte[]        bpBuffer=pCipher.doFinal(tData.getBytes("utf-8"));
			tData=toHexString(bpBuffer);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		} 
		return tData;
	}
	
	/**
	 * 将Hex字符串转化为数组
	 * @param tHexString
	 * @return
	 */
	public static byte[] getHexBytes(String tHexString)
	{
		byte[] bpBuffer=new byte[tHexString.length()/2];
		for(int iCyc=0;iCyc<tHexString.length();iCyc++)
		{
			byte bValue=0;
			switch(tHexString.charAt(iCyc))
			{
				case '0':
					bValue=0x00; 
					break;
				case '1':
					bValue=0x01; 
					break;
				case '2':
					bValue=0x02; 
					break;
				case '3':
					bValue=0x03;
					break;
				case '4':
					bValue=0x04;
					break;
				case '5':
					bValue=0x05;
					break;
				case '6':
					bValue=0x06;
					break;
				case '7':
					bValue=0x07;
					break;
				case '8':
					bValue=0x08;
					break;
				case '9':
					bValue=0x09;
					break;
				case 'a':
					bValue=0x0a;
					break;
				case 'b':
					bValue=0x0b;
					break;
				case 'c':
					bValue=0x0c;
					break;
				case 'd':
					bValue=0x0d;
					break;
				case 'e':
					bValue=0x0e;
					break;
				case 'f':
					bValue=0x0f;
					break;
			}
			if(iCyc%2==0)
			{
				bpBuffer[iCyc/2]=0;
				bpBuffer[iCyc/2]=(byte) (bValue<<4);
			}
			else
			{
				bpBuffer[iCyc/2]=(byte) (bpBuffer[iCyc/2]|bValue);
			}
		}
		return bpBuffer;
	}
	
	/**
	 * 将byte[]转换为Hex类型
	 * @param bpBuffer
	 * @return
	 */
	public static String toHexString(byte[] bpBuffer)
	{  
        StringBuilder pStringBuilder = new StringBuilder();  
        for (int iCyc=0; iCyc<bpBuffer.length; iCyc++)
        {  
        	int iValue1=(bpBuffer[iCyc] & 0xFF) >> 4;
        	int iValue2= bpBuffer[iCyc] & 0x0F;
        	switch(iValue1)
			{
				case 0:
					pStringBuilder.append('0'); 
					break;
				case 1:
					pStringBuilder.append('1');
					break;
				case 2:
					pStringBuilder.append('2');
					break;
				case 3:
					pStringBuilder.append('3');
					break;
				case 4:
					pStringBuilder.append('4');
					break;
				case 5:
					pStringBuilder.append('5');
					break;
				case 6:
					pStringBuilder.append('6');
					break;
				case 7:
					pStringBuilder.append('7');
					break;
				case 8:
					pStringBuilder.append('8');
					break;
				case 9:
					pStringBuilder.append('9');
					break;
				case 10:
					pStringBuilder.append('a');
					break;
				case 11:
					pStringBuilder.append('b');
					break;
				case 12:
					pStringBuilder.append('c');
					break;
				case 13:
					pStringBuilder.append('d');
					break;
				case 14:
					pStringBuilder.append('e');
					break;
				case 15:
					pStringBuilder.append('f');
					break;
			}
        	switch(iValue2)
			{
				case 0:
					pStringBuilder.append('0'); 
					break;
				case 1:
					pStringBuilder.append('1');
					break;
				case 2:
					pStringBuilder.append('2');
					break;
				case 3:
					pStringBuilder.append('3');
					break;
				case 4:
					pStringBuilder.append('4');
					break;
				case 5:
					pStringBuilder.append('5');
					break;
				case 6:
					pStringBuilder.append('6');
					break;
				case 7:
					pStringBuilder.append('7');
					break;
				case 8:
					pStringBuilder.append('8');
					break;
				case 9:
					pStringBuilder.append('9');
					break;
				case 10:
					pStringBuilder.append('a');
					break;
				case 11:
					pStringBuilder.append('b');
					break;
				case 12:
					pStringBuilder.append('c');
					break;
				case 13:
					pStringBuilder.append('d');
					break;
				case 14:
					pStringBuilder.append('e');
					break;
				case 15:
					pStringBuilder.append('f');
					break;
			}
        }  
        return pStringBuilder.toString();
    }  
	
	
	/**
	 * 获取令牌
	 * @param tSystemUrl      远程服务器的URL地址
	 * @param tAccessKey      数据加密用的KEY
	 * @param tUserName       登录名（工号）
	 * @param tRealName       真实姓名
	 * @param tDepartName     部门名称
	 * @param pMapAttribute   附加属性
	 * @return
	 */
	public static TokenResult requireToken(String tSystemUrl,
										   String tAccessKey,
										   String tUserName,String tRealName,String tDepartName,Map<String,Object> pMapAttribute)
	{
		Gson gson = new Gson();
		TokenResult pReturn=null;
		HttpClient pClient=new HttpClient();
		//构建访问的URL地址
		String     tUrl=tSystemUrl.trim();
		if(tUrl.charAt(tUrl.length()-1)!='/'&&tUrl.charAt(tUrl.length()-1)!='\\')
		{
			tUrl+="/";
		}
		tUrl+="systemServiceController.do?requireLoginToken";
		//进行HTTP调用
	    PostMethod      pMethod = new PostMethod(tUrl);
	    List<NameValuePair> pValueList= new ArrayList<NameValuePair>();
	    NameValuePair pParam=null;
	    //--用户名
	    pParam=new NameValuePair();
	    pParam.setName("username");
	    pParam.setValue(encodeAESData(tUserName,tAccessKey));
	    pValueList.add(pParam);
	    //--姓名
	    pParam=new NameValuePair();
	    pParam.setName("realname");
	    pParam.setValue(encodeAESData(tRealName,tAccessKey));
	    pValueList.add(pParam);
	    //--部门
	    pParam=new NameValuePair();
	    pParam.setName("depart");
	    pParam.setValue(encodeAESData(tDepartName,tAccessKey));
	    pValueList.add(pParam);
	    //--其它附加属性
	    if(pMapAttribute!=null&&pMapAttribute.size()>0)
	    {
	    	pParam=new NameValuePair();
		    pParam.setName("parameters");
		    pParam.setValue(encodeAESData(gson.toJson(pMapAttribute),tAccessKey));
		    pValueList.add(pParam);
	    }
	    pMethod.setRequestBody(pValueList.toArray(new NameValuePair[]{}));
	    try
		{
	    	int iStatus = pClient.executeMethod(pMethod);
			if(iStatus==200)
			{
				byte[] 	   bpBuffer=pMethod.getResponseBody();
				String 	   tContent=decodeAESData(new String(bpBuffer,"utf-8"),tAccessKey);
				JSONObject pObject=JSONObject.parseObject(tContent);
				int        iCode=pObject.getIntValue("respCode");
				String     tMessage=pObject.getString("respMsg")!=null?pObject.getString("respMsg"):"";
				String     tOpenUrl=pObject.getString("obj")!=null?pObject.getString("obj"):"";
				if(iCode==200)
				{
					pReturn=new TokenResult(true, "成功",0,tOpenUrl);
				}
				else
				{
					pReturn=new TokenResult(false,tMessage,iCode,"");
				}
			}
			else
			{
				pReturn=new TokenResult(false, "HTTP返回异常", iStatus, "");
			}
		} 
	    catch (IOException e)
		{
	    	pReturn=new TokenResult(false, "IO异常："+e.getMessage(), 900, "");
			e.printStackTrace();
		}
	    finally
	    {
	    	;
	    }
		return pReturn;
	}
}
