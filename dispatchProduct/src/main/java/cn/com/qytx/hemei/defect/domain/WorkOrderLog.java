package cn.com.qytx.hemei.defect.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.base.domain.DeleteState;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能：工单操作日志
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年7月18日
 * 修改日期：2017年7月18日	
 */
@Entity
@Table(name="tb_work_order_log")
public class WorkOrderLog extends BaseDomain{
	private static final long serialVersionUID = 6855253120741510058L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@JoinColumn(name = "user_id")
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	private UserInfo operUser;//操作人
	
	@Column(name="insert_time")
	private Timestamp insertTime;
	
	@Column(name="content")
	private String content;
	
	@Column(name="type")
	private Integer type;//操作类型   
	
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete;
	
	@Column(name="instance_id")
	private String instanceId;

	public Integer getId() {
		return id;
	}

	public UserInfo getOperUser() {
		return operUser;
	}

	public Timestamp getInsertTime() {
		return insertTime;
	}

	public String getContent() {
		return content;
	}

	public Integer getType() {
		return type;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setOperUser(UserInfo operUser) {
		this.operUser = operUser;
	}

	public void setInsertTime(Timestamp insertTime) {
		this.insertTime = insertTime;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
	
	
	
}
