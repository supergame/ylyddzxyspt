package cn.com.qytx.hemei.event.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.google.gson.Gson;

import cn.com.qytx.hemei.event.domain.Event;
import cn.com.qytx.hemei.event.service.IEvent;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;
import cn.com.qytx.platform.org.service.IUser;

/**
 * 功能：事件手机端接口
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年4月20日
 * 修改日期：2017年4月20日	
 */
public class EventWapAction extends BaseActionSupport{
	private static final long serialVersionUID = 5003983293039800806L;
	
	@Resource(name="eventService")
	private IEvent eventService;
	
	@Resource(name="groupService")
	private IGroup groupService;
	
	@Resource(name="userService")
	private IUser userService;
	
	private Event event;
	
	private Integer userId;
	
	/**
	 * 功能：事件列表
	 * @return
	 */
	public String eventList(){
		try{
			 if(userId==null){
		        	ajax("101||参数不能为空!");
		        	return null;
	        }
			if(event==null){
				event = new Event();
			}
			UserInfo userInfo = userService.findOne(userId);
			List<GroupInfo> groupList = groupService.getGroupList(userInfo.getCompanyId(), 1);
			Map<Integer,String> groupMap = new HashMap<Integer, String>();
			if(groupList!=null&&groupList.size()>0){
				for(GroupInfo groupInfo:groupList){
					groupMap.put(groupInfo.getGroupId(), groupInfo.getGroupName());
				}
			}
			List<Event> eventList = eventService.findEventList(event);
			if(eventList!=null&&eventList.size()>0){
				for(Event event:eventList){
					Integer groupId = event.getGroupId();
					if(groupMap.containsKey(groupId)){
						event.setGroupName(groupMap.get(groupId));
					}
				}
			}
			Gson gson = new Gson();
			String gsonStr = gson.toJson(eventList);
			ajax("100||"+gsonStr);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
}
