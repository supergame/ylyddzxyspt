/**
 * 
 */
package cn.com.qytx.hemei.report.service;

import java.util.List;
import java.util.Map;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月9日
 * 修改日期: 2017年5月9日
 * 修改列表: 
 */
public interface IRepairReport extends BaseService<DefectApply>{
	 
	 /**
	  * 维修事项前十
	  * @return
	  */
	 public List<Object[]> findRepairTopTen(String beginTime,String endTime);
	 
	 
	 /**
	  * 维修事项前十
	  * @return
	  */
	 public List<Object[]> findRepairEventType(String beginTime,String endTime);
	 
	 /**
	  *维修人员满意度统计
	  * @return
	  */
	 
	public List<Object[]> findSatisfactionList(String beginTime,String endTime,String employeeName);
	
	
	 /**
	  *医务科室报修统计
	  * @return
	  */
	 
	public List<Object[]> findMedicalDepartmenList(String beginTime,String endTime,String departmentName);
	 
	/**
	 * 后勤员工工作量统计
	 * @param beginTime
	 * @param endTime
	 * @param userName
	 * @param departmentName
	 * @return
	 */
	public List<Object[]> findRepairPeople(String beginTime,String endTime,String userName,String departmentName,String instanceIds);

	
	 /**
      * 查询后勤班组应接工量
      * @return
      */
	 public List<Object[]> findTakeWorkList(String beginTime,String endTime);
	 

		public Map<Integer, String> finshMap(String beginTime, String endTime,String instanceIds);
		
		


}
