/**
 * 
 */
package cn.com.qytx.hemei.report.dao;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.dao.BaseDao;
/**
 * 功能: 维修报表
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月9日
 * 修改日期: 2017年5月9日
 * 修改列表: 
 */
@Repository
public class RepairReportDao extends BaseDao<DefectApply, Integer> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1865665011690464471L;
	
	
	
	
	
	
	/**
	 * 计算应接工
	 * @return
	 */
	public List<Object[]> findTakeWorkList(String beginTime,String endTime){
		 String sql="SELECT e.instance_id,g.group_id,g.group_name FROM tb_group_info g ";
		 sql+=" left JOIN [View_repair_event] e ON e.next_processer_id=g.group_id where e.type=0  AND g.extension=2 and ((e.task_name =1 and e.approveResult=-1) or (e.task_name =0 and e.approveResult=4))";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and e.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and e.end_time <='"+endTime+" 23:59:59'";
		}
		 Query query = super.entityManager.createNativeQuery(sql);
		 return query.getResultList();
	}
	
	
	
	
     /**
      * 查询后勤班组工作量
      * @return
      */
	 public Map<Integer,String> findListMap(String beginTime,String endTime,String instanceIds){
		 String sql="SELECT  SUM( CASE WHEN a.task_name =2 and a.approveResult=1 THEN 1 ELSE 0 END)as receive, SUM( CASE WHEN a.task_name =3 and a.approveResult=1 THEN 1 ELSE 0 END)as finshNum ,a.group_id";
		 sql+=" FROM (SELECT e.task_name,e.approveResult,g.group_id,g.group_name FROM View_repair_group g ";
		 sql+=" left JOIN [View_repair_event] e ON g.user_id = e.processer_id where e.type=0  AND g.extension=2";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and e.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and e.end_time <='"+endTime+" 23:59:59'";
		}
		if(StringUtils.isNoneBlank(instanceIds)){
			sql+=" and e.instance_id in("+instanceIds+")";
		}
		 sql+=" ) a GROUP BY group_id";
		 Query query = super.entityManager.createNativeQuery(sql);
		 List<Object[]> list=query.getResultList();
		 Map<Integer,String> map =new HashMap<Integer, String>();
		 if(list!=null && list.size()>0){
			 for(Object[] obj:list){
				 Integer groupId=(Integer)obj[2];
				 Integer receive=obj[0]==null?0:(Integer)obj[0];
				 Integer finshNum=obj[1]==null?0:(Integer)obj[1];
				 map.put(groupId,receive+"-"+finshNum);
			 }
		 }
		 return map;
	 }
	 
	 
	/* *//**
      * 查询后勤班组应接工量
      * @return
      *//*
	 public List<Object[]> findTakeWorkList(String beginTime,String endTime){
		 String sql="SELECT a.group_name, SUM( CASE WHEN (a.task_name =1 and a.approveResult=-1)or(a.task_name =0 and a.approveResult=4) THEN 1 ELSE 0 END)as takeWork,a.group_id";
		 sql+=" FROM (SELECT e.task_name,e.approveResult,g.group_id,g.group_name FROM tb_group_info g ";
		 sql+=" left JOIN [View_repair_event] e ON e.next_processer_id=g.group_id where e.type=0  AND g.extension=2";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and e.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and e.end_time <='"+endTime+" 23:59:59'";
		}
		 sql+=" ) a GROUP BY group_id,group_name";
		 Query query = super.entityManager.createNativeQuery(sql);
		 return query.getResultList();
	 }*/
	 
	 
	 
	
	 /**
	  * 维修事项前十
	  * @return
	  */
	 public List<Object[]> findRepairTopTen(String beginTime,String endTime){
		 
		 String sql=" SELECT TOP 10 equipment_name ,num FROM";
		 sql+=" (select equipment_name,count(*) as num from  tb_defect_apply WHERE type=0 ";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and create_time >='"+beginTime+" 00:00:00'";
			}
			if(StringUtils.isNoneBlank(endTime)){
				sql+=" and create_time <='"+endTime+" 23:59:59'";
			}
		 sql+=" AND is_delete=0 GROUP BY equipment_name) d ORDER BY d.num desc ";
		 Query query = super.entityManager.createNativeQuery(sql);
		 return query.getResultList();
	 }
	 
	 /**
	  * 维修事项类型
	  * @return
	  */
	 public List<Object[]> findRepairEventType(String beginTime,String endTime){
		 String sql="select count(*) as num, c.type_name from(";
		 sql+=" SELECT e.type_name FROM	tb_defect_apply d INNER JOIN (";
		 sql+=" SELECT	t.type_name,e.event_name FROM	tb_event e	INNER JOIN tb_event_type t";
		 sql+=" ON e.event_type = t.id AND e.is_delete = 0 ) e ON e.event_name = d.equipment_name";
		 if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and d.create_time >='"+beginTime+" 00:00:00'";
			}
			if(StringUtils.isNoneBlank(endTime)){
				sql+=" and d.create_time <='"+endTime+" 23:59:59'";
			}
		 sql+="  AND d.is_delete=0) c GROUP BY type_name ORDER BY num DESC";
		 Query query = super.entityManager.createNativeQuery(sql);
		 return query.getResultList();
	 }
	 /**
	  *维修人员满意度统计
	  * @return
	  */
	 
	public List<Object[]> findSatisfactionList(String beginTime,String endTime,String employeeName){
		String sql="SELECT d.processer_name,g.group_name,d.verySatisfied,d.satisfied,d.kind,d.dissatisfied FROM";
	    sql+="(SELECT p.processer_name,p.processer_id,SUM (CASE WHEN d.achievement = 1 THEN 1 ELSE 0 END) AS verySatisfied,";
	    sql+="SUM (CASE WHEN d.achievement = 2 THEN 1 ELSE 0 END) AS satisfied,";
	    sql+="SUM (CASE WHEN d.achievement = 3 THEN	1 ELSE 0 END) AS kind,";
	    sql+="SUM (CASE WHEN d.achievement = 4 THEN 1 ELSE 0 END) AS dissatisfied FROM tb_defect_apply d";
	    sql+=" INNER JOIN tb_cbb_my_processed p ON d.instance_id = p.instance_id WHERE p.task_name = 3";
	    if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and p.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and p.end_time <='"+endTime+" 23:59:59'";
		}
	    if(StringUtils.isNoneBlank(employeeName)){
		    	sql+=" and p.processer_name like '%"+employeeName+"%'";
		}
		    
	    sql+=" AND d.type = 0 AND d.is_delete = 0 GROUP BY processer_name,processer_id) d";
	    sql+=" LEFT JOIN (SELECT u.user_id,g.group_name FROM tb_user_info u	INNER JOIN tb_group_info g";
	    sql+=" ON u.group_id = g.group_id) g ON d.processer_id=g.user_id";
	   
	    Query query = super.entityManager.createNativeQuery(sql);
	    return query.getResultList();
	}
	 
	 /**
	  *医务科室报修统计
	  * @return
	  */
	 
	public List<Object[]> findMedicalDepartmenList(String beginTime,String endTime,String departmentName){
		
		String  sql="SELECT	a.group_name, SUM (CASE WHEN a.task_name = 1 and a.approveResult=-1 THEN 1 ELSE 0 END)as report,";
		sql+="SUM (CASE WHEN a.task_name = 4 and a.approveResult=1 THEN 1 ELSE 0 END ) as finshNum,sum(isnull(a.time_long,0)) as timeLong,sum(isnull(a.work_hour,0)) as workTime";
		sql+=" FROM(SELECT e.task_name,g.group_name,e.time_long,e.work_hour,e.approveResult FROM View_repair_group g";
		sql+=" 	LEFT JOIN (SELECT	r.*, e.work_hour  FROM [View_repair_event] r LEFT JOIN tb_event e ON e.event_name = r.equipment_name where 1=1";
		if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and r.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and r.end_time <='"+endTime+" 23:59:59'";
		}
		sql+=" 	) e ON g.user_id = e.create_user_id WHERE g.extension=3) a where 1=1 ";
		if(StringUtils.isNoneBlank(departmentName)){
			sql+=" and a.group_name like '%"+departmentName+"%'";
		}
		sql+=" GROUP  BY group_name";
		Query query = super.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
	/**
	 * 后勤员工工作量统计
	 * @param beginTime
	 * @param endTime
	 * @param userName
	 * @param departmentName
	 * @return
	 */
	public List<Object[]> findRepairPeople(String beginTime,String endTime,String userName,String departmentName,String instanceIds){
	    String sql="select c.group_name,c.user_name,sum(CASE WHEN c.task_name=2 and c.approveResult=1 THEN 1 ELSE 0 END)as receive,sum(CASE WHEN c.task_name=3 and c.approveResult=1 THEN 1 ELSE 0 END)as finsh";
	    sql+=",sum(isnull(c.time_long,0)) as timeLong,sum(isnull(workHour,0))as workHour from(SELECT * FROM View_repair_group a";
	    sql+=" LEFT JOIN (SELECT v.*, ISNULL(e.work_hour, 0) workHour FROM	[View_repair_event] v";
	    sql+=" LEFT JOIN tb_event e ON v.equipment_name = e.event_name WHERE v.type=0";
	    if(StringUtils.isNoneBlank(beginTime)){
			 sql+=" and v.end_time >='"+beginTime+" 00:00:00'";
			}
		if(StringUtils.isNoneBlank(endTime)){
			sql+=" and v.end_time <='"+endTime+" 23:59:59'";
		}
		if(StringUtils.isNoneBlank(instanceIds)){
			sql+=" and v.instance_id in("+instanceIds+")";
		}
	    sql+=" ) b ON a.user_id = b.processer_id WHERE a.extension=2";
	    sql+=" )c  where 1=1";
	    if(StringUtils.isNoneBlank(userName)){
	    	sql+=" and c.user_name like '%"+userName+"%'";
	    }
	    if(StringUtils.isNoneBlank(departmentName)){
	    	sql+=" and c.group_name like '%"+departmentName+"%'";
	    }
		sql+="  GROUP BY group_name,user_name ";
		Query query = super.entityManager.createNativeQuery(sql);
		return query.getResultList();
	}
	
}
