package cn.com.qytx.hemei.defect.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.hemei.defect.dao.SelectGoodsDao;
import cn.com.qytx.hemei.defect.domain.SelectGoods;
import cn.com.qytx.hemei.defect.service.ISelectGoods;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

@Service
@Transactional
public class SelectGoodsImpl extends BaseServiceImpl<SelectGoods> implements ISelectGoods {

	@Autowired
	private SelectGoodsDao selectGoodsDao;
	
	@Override
	public SelectGoods findByGoodIdAndInstanceId(Integer goodId,
			String instanceId) {
		// TODO Auto-generated method stub
		return selectGoodsDao.findByGoodIdAndInstanceId(goodId, instanceId);
	}

	@Override
	public List<SelectGoods> findList(String instanceId) {
		// TODO Auto-generated method stub
		return selectGoodsDao.findList(instanceId);
	}

}
