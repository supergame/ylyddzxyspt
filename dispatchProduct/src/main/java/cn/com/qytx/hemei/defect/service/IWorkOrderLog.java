package cn.com.qytx.hemei.defect.service;

import java.util.List;

import cn.com.qytx.hemei.defect.domain.WorkOrderLog;
import cn.com.qytx.platform.base.service.BaseService;

public interface IWorkOrderLog extends BaseService<WorkOrderLog>{

	
	public List<WorkOrderLog> findWorkOrderLogListByInstanceId(String instanceId);
	
}
