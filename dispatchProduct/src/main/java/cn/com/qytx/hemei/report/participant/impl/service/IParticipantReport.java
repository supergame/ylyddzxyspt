package cn.com.qytx.hemei.report.participant.impl.service;

import java.util.List;

import cn.com.qytx.hemei.defect.domain.SelectPeople;
import cn.com.qytx.platform.base.service.BaseService;

public interface IParticipantReport extends BaseService<SelectPeople>  {
	/**
	 * 获得参与人报表数据
	 * @return
	 */
	public List<Object[]> findParticipantReportList(String groupName,String userName,String beginTime,String endTime);
}
