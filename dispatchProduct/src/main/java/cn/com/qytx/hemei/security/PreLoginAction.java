package cn.com.qytx.hemei.security;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.CompanyInfo;
import cn.com.qytx.platform.org.service.ICompany;

public class PreLoginAction extends BaseActionSupport{
	private static final long serialVersionUID = 2574447848362493629L;
	Logger logger = Logger.getLogger(PreLoginAction.class);
	
	private String companyCode;
	
	@Autowired
	private ICompany companyService;
	
	
	public String preLogin(){
		HttpServletRequest request = this.getRequest();
		HttpServletResponse response = this.getResponse();
		CompanyInfo companyInfo = null;
		if(StringUtils.isNotBlank(companyCode)){
			companyInfo = companyService.findOne(" companyCode=?", companyCode);
		}
		logger.info("有人登陆了companyCode="+companyCode);
		Cookie[] cookies = request.getCookies();
		if(companyInfo != null){
			boolean isExistCookie = false;
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("companyCode")){
                	isExistCookie = true;
                    System.out.println("原值为:"+cookie.getValue());
                    cookie.setValue(companyCode);
                    cookie.setPath("/");
                    cookie.setMaxAge(30 * 60);// 设置为30min
                    logger.info("被修改的cookie名字为:"+cookie.getName()+",新值为:"+cookie.getValue());
                    response.addCookie(cookie);
                }
                if(cookie.getName().equals("companyId")){
                    System.out.println("原值为:"+cookie.getValue());
                    cookie.setValue(String.valueOf(companyInfo.getCompanyId()));
                    cookie.setPath("/");
                    cookie.setMaxAge(30 * 60);// 设置为30min
                    logger.info("被修改的cookie名字为:"+cookie.getName()+",新值为:"+cookie.getValue());
                    response.addCookie(cookie);
                }
            }
            if(!isExistCookie){
            	Cookie cCode = new Cookie("companyCode",companyCode);
				cCode.setMaxAge(Integer.MAX_VALUE);
				cCode.setPath(request.getContextPath());
				Cookie cCId = new Cookie("companyId",companyInfo.getCompanyId()+"");
				cCId.setMaxAge(Integer.MAX_VALUE);
				cCId.setPath(request.getContextPath());
				response.addCookie(cCode);
			    response.addCookie(cCId);
			    logger.info("记录cookie：companyCode="+companyCode+";;;companyId="+companyInfo.getCompanyId());
            }
		}else{
			if(cookies!=null){
				for(Cookie cookie : cookies){
	                 if(cookie.getName().equals("companyId")){
	                	 companyInfo = companyService.findOne(cookie.getValue());
	                	 break;
	                 }
	             }
			}
		}
		request.setAttribute("companyInfo", companyInfo);
		return SUCCESS;
	}


	public String getCompanyCode() {
		return companyCode;
	}


	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	
}
