package cn.com.qytx.hemei.report.equipment.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.hemei.report.equipment.dao.EquipmentDao;
import cn.com.qytx.hemei.report.equipment.impl.service.IEquipment;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
@Service
@Transactional
public class EquipmentImpl extends BaseServiceImpl<Capital> implements IEquipment {

	@Autowired
	private EquipmentDao equipmentDao;
	
	
	
	@Override
	public List<Object[]> findReturnEquipmentList(String groupName,
			String userName, String beginTime, String endTime,Integer type) {
		// TODO Auto-generated method stub
		return equipmentDao.findReturnEquipmentList(groupName, userName, beginTime, endTime, type);
	}



	@Override
	public List<Object[]> findUseEquipmentList(String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return equipmentDao.findUseEquipmentList(beginTime, endTime);
	}

	
	public Map<String,Integer> findEquipmentNum(String beginTime,
			String endTime){
		List<Object[]> list=equipmentDao.findReturnEquipmentNum(beginTime, endTime);
		Map<String,Integer> map =new HashMap<String, Integer>();
		if(list!=null && list.size()>0){
			for(Object[] obj:list){
				Integer equipmentGroupId=(Integer)obj[0];
				Integer groupId=(Integer)obj[1];
				String key=equipmentGroupId+"_"+groupId;
				Integer value=(Integer)obj[2];
				map.put(key, value);
			}
		}
		return map;
		
	}
	
	
	
	
	
	
}
