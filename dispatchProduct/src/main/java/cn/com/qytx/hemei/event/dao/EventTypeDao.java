package cn.com.qytx.hemei.event.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.event.domain.EventType;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;

/**
 * 功能:事件类型持久层 版本: 1.0 开发人员: panbo 创建日期: 2016年8月2日 修改日期: 2016年8月2日 修改列表:
 */
@Repository("eventTypeDao")
public class EventTypeDao extends BaseDao<EventType, Integer> {

	public List<EventType> findEventTypeList(Integer companyId) {
		String hql = "isDelete=0 and companyId=?";
		return super.findAll(hql, new Sort(Direction.ASC, "orderIndex"),
				companyId);
	}

	/**
	 * 在指定ID下面是否有相同的事件类型名称
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameEventTypeName(Integer parentId,String EventTypeName,int companyId){
		String sql="parentId=? and typeName=?  and isDelete=0  and companyId = ?";
		Object object=super.findOne(sql, parentId,EventTypeName,companyId);
		if(object==null){
			return false;
		}else{
			return true;
		}
	}



	/**
	 * 功能：判断是否存在子事件类型
	 * @param id
	 * @param companyId
	 * @return
	 */
	public boolean isHasChildEventType(Integer id, int companyId) {
		String hql = "isDelete=0 and parentId=? and companyId=?";
		List list = super.findAll(hql, id,companyId);
		if(list!=null&&list.size()>0){
			return true;
		}
		return false;
	}

	public List<EventType> getEventTypeListByIds(String ids) {
		String hql = "1=0";
		if(StringUtils.isNotBlank(ids)){
			if(ids.startsWith(",")){
				ids = ids.substring(1);
			}
			if(ids.endsWith(",")){
				ids = ids.substring(0,ids.length()-1);
			}
			hql += "or id in("+ids+")";
		}
		return super.findAll(hql,new Sort(Direction.ASC,"id"));
	}
	
	
}
