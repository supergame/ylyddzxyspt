package cn.com.qytx.hemei.cardispatch.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.hemei.cardispatch.domain.Car;
import cn.com.qytx.hemei.cardispatch.domain.CarOrder;
import cn.com.qytx.hemei.cardispatch.domain.Driver;
import cn.com.qytx.hemei.cardispatch.service.ICarOrder;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;

public class CarOrderWapAction extends BaseActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3308110690384376337L;
	private final static Logger logger = Logger.getLogger(CarOrderAction.class);
	
	
	@Autowired
	private ICarOrder  carOrderService;
	
	@Autowired
	private IUser  userService;
	
	/**
	 * 流程Id
	 */
	private String instanceId;
	private String approveResult;
	private String advice;
	private String taskName;
	private Integer nextUserId;
	private Integer processType;
	
	private Driver driver;
	
	private Car car;
	
	private Integer userId;
	
	
	/**
	 * 处理操作
	 * @Title: approve   
	 */
	public void approve(){
		logger.info("调度车辆处理工单接口,参数：instanceId:"+instanceId+",taskName:"+taskName+",nextUserId:"+nextUserId+",driver:"+driver+",car:"+car);
		try{
			if(userId!=null){
				UserInfo userInfo=userService.findOne(userId);
				if(userInfo!=null){
					CarOrder carOrder=carOrderService.findByInstanceId(instanceId);
					if("1".equals(taskName)){
						if(car==null || car.getId()==null){
							ajax("101||车辆Id不能为空");
							return;
						}
						if(driver==null || driver.getUserId()==null){
							ajax("101||司机Id不能为空");
							return;
						}
						if(carOrder.getStatus()>=2){
							ajax("101||此任务已派遣");
							return;
						}
					}
					/**
					 * 发车
					 */
					if("2".equals(taskName)){
						if(carOrder.getStatus()>=3){
							ajax("101||此任务已发车");
							return;
						}
					}
					
					
					/**
					 * 到达
					 */
					if("7".equals(taskName)){//到达
						if(carOrder.getStatus()==7||carOrder.getStatus()==4){
							ajax("101||此任务已到达");
							return;
						}
						nextUserId = userInfo.getUserId();
					}
					
					
					/**
					 * 回场
					 */
					if("3".equals(taskName)){//回场
						if(carOrder.getStatus()==4){
							ajax("101||此任务已回场");
							return;
						}
					}
					
					/**
					 * 接收工单,不需要走流程
					 */
					if("4".equals(taskName)){
						carOrder.setIsAccept(1);
						carOrderService.saveOrUpdate(carOrder);
						ajax("100||1");
						return;
					}
					int res=carOrderService.approve(userInfo.getUserId(), instanceId, approveResult, advice, taskName, nextUserId,processType,driver,car);
					if(res==1){
						ajax("100||1");
					}else{
						ajax("102||程序异常");
					}
				}
			}else{
				ajax("101||参数缺少");
			}
			
		}catch(Exception e){
			logger.info("车辆调度工单处理失败,失败原因:"+e.getMessage());
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}


	public String getInstanceId() {
		return instanceId;
	}


	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}


	public String getApproveResult() {
		return approveResult;
	}


	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}


	public String getAdvice() {
		return advice;
	}


	public void setAdvice(String advice) {
		this.advice = advice;
	}


	public String getTaskName() {
		return taskName;
	}


	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}


	public Integer getNextUserId() {
		return nextUserId;
	}


	public void setNextUserId(Integer nextUserId) {
		this.nextUserId = nextUserId;
	}


	public Integer getProcessType() {
		return processType;
	}


	public void setProcessType(Integer processType) {
		this.processType = processType;
	}


	public Driver getDriver() {
		return driver;
	}


	public void setDriver(Driver driver) {
		this.driver = driver;
	}


	public Car getCar() {
		return car;
	}


	public void setCar(Car car) {
		this.car = car;
	}


	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	

}
