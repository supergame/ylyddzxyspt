package cn.com.qytx.hemei.defect.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.platform.base.domain.BaseDomain;
import cn.com.qytx.platform.org.domain.UserInfo;

@Table(name="tb_user_capital")
@Entity
public class UserCapital extends BaseDomain implements java.io.Serializable {
	private static final long serialVersionUID = 6808459378106670198L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	/**
	 * 创建人
	 */
	@JoinColumn(name="create_user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo createUser;
	
	@Column(name="create_time")
	private Timestamp createTime=new Timestamp(System.currentTimeMillis());
	
	/**
	 * 所属资产
	 */
	@JoinColumn(name="capital_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private Capital capital;
	
	/**
	 * 所属工单
	 */
	@JoinColumn(name="defect_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private DefectApply defectApply;

	public Integer getId() {
		return id;
	}

	public UserInfo getCreateUser() {
		return createUser;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public Capital getCapital() {
		return capital;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCreateUser(UserInfo createUser) {
		this.createUser = createUser;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public void setCapital(Capital capital) {
		this.capital = capital;
	}

	public DefectApply getDefectApply() {
		return defectApply;
	}

	public void setDefectApply(DefectApply defectApply) {
		this.defectApply = defectApply;
	}
	
	
	
	
	
}
