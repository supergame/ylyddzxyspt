/**
 * 
 */
package cn.com.qytx.hemei.defect.action;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.hemei.defect.domain.DefaultDepartment;
import cn.com.qytx.hemei.defect.service.IDefaultDepartment;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能: 设置默认部门
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年5月20日
 * 修改日期: 2017年5月20日
 * 修改列表: 
 */
public class DefaultDepartmentAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8181853204628990418L;

	@Autowired
	private IDefaultDepartment defaultDepartmentService;
	
	private DefaultDepartment  defaultDepartment; 
	
	
	/**
	 * 设置或修改默认部门
	 */
	public void addOrUpdateDepartment(){
		UserInfo user = getLoginUser();
		if(user!=null){
			try {
				if(defaultDepartment.getId()!=null){
					DefaultDepartment d=defaultDepartmentService.findOne(defaultDepartment.getId());
					d.setGroupId(defaultDepartment.getGroupId());
					d.setGroupName(defaultDepartment.getGroupName());
					defaultDepartmentService.saveOrUpdate(d);
				}else{
					defaultDepartment.setCompanyId(user.getCompanyId());
					defaultDepartmentService.saveOrUpdate(defaultDepartment);
				}
				ajax(0);
			} catch (Exception e) {
				e.printStackTrace();
				ajax(1);
			}
			
		}
	}
	/**
	 * 通過type查找对应的部门
	 */
	public void findDepartment(){
		UserInfo user= getLoginUser();
		if(user!=null){
			Map<String,Object> map= new HashMap<String, Object>();
			if(defaultDepartment!=null&&defaultDepartment.getType()!=null){
				DefaultDepartment d=defaultDepartmentService.findModel(defaultDepartment.getType());
				if(d!=null){
			    	map.put("groupId", d.getGroupId());
			    	map.put("groupName", d.getGroupName());
			    	map.put("id", d.getId());
			    }
			}
			ajax(map);
		}
	}
	public DefaultDepartment getDefaultDepartment() {
		return defaultDepartment;
	}
	public void setDefaultDepartment(DefaultDepartment defaultDepartment) {
		this.defaultDepartment = defaultDepartment;
	}
	
	
}
