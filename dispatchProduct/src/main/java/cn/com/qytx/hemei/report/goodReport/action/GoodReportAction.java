package cn.com.qytx.hemei.report.goodReport.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.hemei.report.goodReport.service.IGoodReport;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

public class GoodReportAction extends BaseActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private IGoodReport goodReportService;
	
	private String beginTime;
	
	
	private String endTime;
	
	private String repairTeam;
	
	private String goodName;
	
	
	private String groupName;
	
	private String name;
	
	/**
     * 科室使用物资统计
     * @return
     */
	public void findDepartmentUseGoods(){
		UserInfo user=getLoginUser();
		try {
			if(user!=null){
				List<Object[]> list=goodReportService.findDepartmentUseGoods(beginTime, endTime, repairTeam, goodName, groupName);
				List<Map<String,Object>> listMap=findDepartmentUseGoodsListMap(list);
				ajax(listMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	public List<Map<String,Object>> findDepartmentUseGoodsListMap(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		int i=1;
		if(list!=null && list.size()>0){
			for(Object[] obj:list){
				Map<String,Object> map= new HashMap<String, Object>();
				map.put("no", i);
				map.put("groupName",obj[7]==null?"--":obj[7].toString());
				map.put("goodName",obj[3]==null?"--":obj[3].toString());
				map.put("goodGroupName",obj[6]==null?"--":obj[6].toString());
				map.put("num",obj[4]==null?0:Integer.valueOf(obj[4].toString()));
				map.put("price",obj[8]==null?0:obj[8]);
				map.put("money",obj[5]==null?0:obj[5] );
				listMap.add(map);
				i++;
			}
		}
		return listMap;
		
	}
	
	
	  
    /**
     * 导出上报部门使用物资情况
     */
    public  void exportGroupUseGoods(){
    	HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				repairTeam = URLDecoder.decode(repairTeam, "utf-8");
				goodName = URLDecoder.decode(goodName, "utf-8");
				groupName = URLDecoder.decode(groupName, "utf-8");
				List<Object[]> list=goodReportService.findDepartmentUseGoods(beginTime, endTime, repairTeam, goodName, groupName);
				List<Map<String,Object>> listMap=findDepartmentUseGoodsListMap(list);
				String fileName = URLEncoder.encode("报修科室物资使用统计.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), listMap, getExportKeyList());
	            exportExcel.exportWithSheetName("报修科室物资使用统计");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}  
    }
    private List<String> getExportHeadList(){
        List<String> headList = new ArrayList<String>();
        headList.add("序号");
        headList.add("科室名称");
        headList.add("物资名称");
        headList.add("物资组");
        headList.add("数量");
        headList.add("单价");
        headList.add("总价");
        return headList;
    }
    
    private List<String> getExportKeyList(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("groupName");
        headList.add("goodName");
        headList.add("goodGroupName");
        headList.add("num");
        headList.add("price");
        headList.add("money");
        return headList;
    }
	
	/**
     * 维修科室使用物资统计
     * @return
     */
	public void findRepairUseGoods(){
		UserInfo user=getLoginUser();
		try {
			if(user!=null){
				List<Object[]> list=goodReportService.findRepairPeopleUseGoods(beginTime, endTime, repairTeam, name, goodName, groupName);
				List<Map<String,Object>> listMap=findRepairUseGoodsListMap(list);
				ajax(listMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	public List<Map<String,Object>> findRepairUseGoodsListMap(List<Object[]> list){
		List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
		int i=1;
		if(list!=null && list.size()>0){
			for(Object[] obj:list){
				Map<String,Object> map= new HashMap<String, Object>();
				map.put("no", i);
				map.put("groupName",obj[1]==null?"--":obj[1].toString());
				map.put("name",obj[0]==null?"--":obj[0].toString());
				map.put("goodName",obj[2]==null?"--":obj[2].toString());
				map.put("goodGroupName",obj[3]==null?"--":obj[3].toString());
				map.put("num",obj[4]==null?0:Integer.valueOf(obj[4].toString()));
				map.put("price",obj[5]==null?0:obj[5]);
				map.put("money",obj[6]==null?0:obj[6] );
				listMap.add(map);
				i++;
			}
		}
		return listMap;
		
	}
	
	 /**
     * 导出上报部门使用物资情况
     */
    public  void exportRepairUseGoods(){
    	HttpServletResponse response = this.getResponse();
        response.setContentType("application/vnd.ms-excel");
        OutputStream outp = null;
		try{
			UserInfo userInfo=this.getLoginUser();
			if(userInfo!=null){
				repairTeam = URLDecoder.decode(repairTeam, "utf-8");
				name = URLDecoder.decode(name, "utf-8");
				goodName = URLDecoder.decode(goodName, "utf-8");
				groupName = URLDecoder.decode(groupName, "utf-8");
				List<Object[]> list=goodReportService.findRepairPeopleUseGoods(beginTime, endTime, repairTeam, name, goodName, groupName);
				List<Map<String,Object>> listMap=findRepairUseGoodsListMap(list);
				String fileName = URLEncoder.encode("后勤维修人员使用材料统计.xls", "UTF-8");
		        // 把联系人信息填充到map里面
		        response.addHeader("Content-Disposition",
		                "attachment;filename=" + fileName);// 解决中文
				outp = response.getOutputStream();
	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList2(), listMap, getExportKeyList2());
	            exportExcel.exportWithSheetName("后勤维修人员使用材料统计");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}  
    }
    private List<String> getExportHeadList2(){
        List<String> headList = new ArrayList<String>();
        headList.add("序号");
        headList.add("姓名");
        headList.add("维修班组");
        headList.add("物资名称");
        headList.add("物资组");
        headList.add("数量");
        headList.add("单价");
        headList.add("总价");
        return headList;
    }
    
    private List<String> getExportKeyList2(){
        List<String> headList = new ArrayList<String>();
        headList.add("no");
        headList.add("groupName");
        headList.add("name");
        headList.add("goodName");
        headList.add("goodGroupName");
        headList.add("num");
        headList.add("price");
        headList.add("money");
        return headList;
    }
	
	
	
	
	public IGoodReport getGoodReportService() {
		return goodReportService;
	}

	public void setGoodReportService(IGoodReport goodReportService) {
		this.goodReportService = goodReportService;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getRepairTeam() {
		return repairTeam;
	}

	public void setRepairTeam(String repairTeam) {
		this.repairTeam = repairTeam;
	}

	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
