package cn.com.qytx.hemei.report.goodReport.service;

import java.util.List;

import cn.com.qytx.hemei.defect.domain.DefectApply;
import cn.com.qytx.platform.base.service.BaseService;

public interface IGoodReport extends BaseService<DefectApply> {

	/**
     * 科室使用物资统计
     * @return
     */
	public List<Object[]> findDepartmentUseGoods(String beginTime,String endTime,String repairTeam,String goodName,String groupName);
		
	/**
	 * 维修人员使用物资
	 * @return
	 */
	public List<Object[]> findRepairPeopleUseGoods(String beginTime,String endTime,String repairTeam,String name,String goodName,String groupName);
	
}
