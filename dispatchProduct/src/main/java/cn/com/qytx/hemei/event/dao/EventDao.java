package cn.com.qytx.hemei.event.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import cn.com.qytx.hemei.event.domain.Event;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.org.domain.UserInfo;
/**
 * 功能：
 * 版本：1.0
 * 开发人员: pb
 * 创建日期：2017年4月20日
 * 修改日期：2017年4月20日	
 */
@Repository("eventDao")
public class EventDao extends BaseDao<Event, Integer>{

	public Page<Event> findEventPage(Pageable pageInfo, UserInfo userInfo,
			Event event) {
		String eventName = event.getEventName();
		Integer eventType = event.getEventType();
		String hql = "1=1";
		if(StringUtils.isNotBlank(eventName)){
			hql += " and eventName like '%"+eventName+"%'";
		}
		if(eventType!=null&&eventType.intValue()!=0){
			hql += " and (eventType="+eventType+" or eventType in (select et.id from EventType et where et.path like '%,"+eventType+",%'))";
		}
		return super.unDeleted().findAll( hql,pageInfo);
	}

	
	public List<Event> findEventList(Event event) {
		String eventName = event.getEventName();
		Integer eventType = event.getEventType();
		String hql = "1=1";
		if(StringUtils.isNotBlank(eventName)){
			hql += " and eventName like '%"+eventName+"%'";
		}
		if(eventType!=null&&eventType.intValue()!=0){
			hql += " and (eventType="+eventType+" or eventType in (select et.id from EventType et where et.path like '%,"+eventType+",%'))";
		}
		return super.unDeleted().findAll( hql);
	}


	public List<UserInfo> findUserListByLoginName(String loginName,
			Integer companyId) {
		String sql = "select u.user_id as userId,u.login_name as loginName,u.user_name as userName,u.group_id as groupId,g.group_name as groupName from tb_user_info u INNER JOIN tb_group_info g on u.group_id = g.group_id where 1=0";
		if(StringUtils.isNoneBlank(loginName)){
			sql += " or (u.login_name like '"+loginName+"%' and u.is_delete=0 and g.is_delete=0 and u.company_id="+companyId+")";
		}
		Query dataQuery = super.entityManager.createNativeQuery(sql);
		// 得到查询出来的数据
		dataQuery.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(UserInfo.class));
		return dataQuery.getResultList();	
	}
/**
 * 根据事件类型id 查询事件是否存在
 * @param eventType
 * @return
 */
	public Event findModel( Integer eventType){
		String hql = " eventType ="+eventType;
		
		return super.unDeleted().findOne(hql);
	}
	
	/**
	 * 根据事件名称 查询事件是否存在
	 * @param eventType
	 * @return
	 */
		public Event findByName( String name){
			String hql = " eventName = ?";
			return super.unDeleted().findOne(hql,name);
		}
		
	
}
