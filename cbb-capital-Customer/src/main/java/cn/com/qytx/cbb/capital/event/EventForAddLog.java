package cn.com.qytx.cbb.capital.event;

import org.springframework.context.ApplicationEvent;

import cn.com.qytx.cbb.capital.domain.CapitalLog;

public class EventForAddLog extends ApplicationEvent{

	public EventForAddLog(CapitalLog source) {
		super(source);
	}

	private static final long serialVersionUID = 1L;

}
