package cn.com.qytx.cbb.capital.action;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.service.ICapitalUseLog;
import cn.com.qytx.cbb.capital.util.Tool;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.base.query.Sort.Order;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.utils.DateUtils;

/**
 * 功能:资产使用记录
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月16日
 * 修改日期: 2016年8月16日
 * 修改列表: 
 */
public class CapitalUseLogAction extends BaseActionSupport{
	private static final long serialVersionUID = -6075563344226356473L;
	
	/**
	 * 资产使用日志接口
	 */
	@Resource(name="capitalUseLogService")
	private ICapitalUseLog capitalUseLogService;
	
	private CapitalVo capitalVo;
	
	
	
	
	/**
	 * 功能：日志列表
	 * @return
	 */
	public String capitalUseLogList(){
		UserInfo userInfo = this.getLoginUser();
		if(userInfo!=null){
			capitalVo.setCompanyId(userInfo.getCompanyId());
			Pageable pageable = getPageable();
			Page<CapitalUseLog> pageInfo =  capitalUseLogService.findCapitalUseLogPage(pageable, capitalVo);
			List<CapitalUseLog> capitalLogList = pageInfo.getContent();
			List<Map<String,Object>> capitalList=analyzeResult(capitalLogList, pageable);
            ajaxPage(pageInfo, capitalList);
		}
		return null;
	}

	private List<Map<String, Object>> analyzeResult(
			List<CapitalUseLog> capitalLogList, Pageable pageable) {
		List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();
		if(capitalLogList!=null&&capitalLogList.size()>0){
		    int i = pageable.getPageNumber() * pageable.getPageSize() + 1;
		    SimpleDateFormat sdfYYYYMMDDHHMM = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			for(CapitalUseLog useLog:capitalLogList){
				Map<String,Object> map=new HashMap<String, Object>();
				map.put("no", i);
				map.put("createTime", useLog.getCreateTime()!=null?Tool.formatDate(useLog.getCreateTime()):"--");//操作时间
				Capital capital = useLog.getCapital();
				map.put("goodName", capital!=null?capital.getName():"--");
				map.put("capitalNo", capital!=null?capital.getCapitalNo():"--");
				CapitalGroup capitalGroup = capital!=null?capital.getCapitalGroup():null;
				map.put("capitalGroup", capitalGroup!=null?capitalGroup.getGroupName():"--");
				GroupInfo useGroup = useLog.getUseGroup();
				map.put("useGroupName", useGroup!=null?useGroup.getGroupName():"--");
				map.put("statusStr", capital!=null?getStateStr(capital.getStatus()):"--");
				map.put("location", capital!=null?capital.getStoreLocation():"--");
				map.put("startUseTime", useLog.getUseStartTime()!=null?sdfYYYYMMDDHHMM.format(useLog.getUseStartTime()):"--");
				map.put("endUseTime", useLog.getUseEndTime()!=null?sdfYYYYMMDDHHMM.format(useLog.getUseEndTime()):"--");
				String timeLength = "--";
				if(useLog.getUseStartTime()!=null&&useLog.getUseEndTime()!=null){
					timeLength = DateUtils.formatHHMMSS((useLog.getUseEndTime().getTime()-useLog.getUseStartTime().getTime())/1000);
				}
				map.put("useTimeLength", timeLength);
				mapList.add(map);
				i++;
			}
	   }
	   return  mapList;
	}
	
	/**
	 * 获得资产状态	
	 */
	public static String getStateStr(Integer state){
		String str="";
		if(state==1){
			str= "在库";
		}else if(state==2){
			str= "使用中";
		}else if(state==3){
			str= "已停用";
		}else if(state==4){
			str= "待归库";
		}else if(state==5){
			str= "已禁用";
		}else if(state==6){
			str= "已送达";
		}
		return str;
	}	
	
	
	 /**
     * 功能：资产列表导出
     */
    public void exportUseLog(){
    	HttpServletResponse response = this.getResponse();
		response.setContentType("application/vnd.ms-excel");
		OutputStream outp=null;
		try {
			UserInfo userInfo =getLoginUser();
			if(userInfo!=null){
				  capitalVo.setCompanyId(userInfo.getCompanyId());
				  Order order = new Order(Direction.DESC, "createTime");
		          Sort s = new Sort(order);
		          super.setIDisplayLength(Integer.MAX_VALUE);
		          Pageable pageable = getPageable(s);
		          capitalVo.setCompanyId(userInfo.getCompanyId());
		          if(StringUtils.isNoneBlank(capitalVo.getUserGroupName())){
		        	  capitalVo.setUserGroupName(URLDecoder.decode(capitalVo.getUserGroupName(), "UTF-8"));
		          }
		          if(StringUtils.isNoneBlank(capitalVo.getKeyWord())){
		        	  capitalVo.setKeyWord(URLDecoder.decode(capitalVo.getKeyWord(), "UTF-8"));
		          }
		          Page<CapitalUseLog> pageInfo =  capitalUseLogService.findCapitalUseLogPage(pageable, capitalVo);
		          List<CapitalUseLog> list=pageInfo.getContent();
		          List<Map<String,Object>> capitalList=analyzeResult(list, pageable);				
				
	            // 把报表信息填充到map里面
				response.addHeader("Content-Disposition", "attachment;filename="
						+ new String("设备使用记录统计.xls".getBytes(), "iso8859-1"));// 解决中文
				outp = response.getOutputStream();

	            ExportExcel exportExcel = new ExportExcel(outp, getExportHeadList(), capitalList,
						getExportKeyList());
				exportExcel.export();
			}
		} catch(Exception e){
			e.getStackTrace();
		} finally{
			if(outp!=null){
				try {
					outp.close();
				} catch (Exception e) {
					e.getStackTrace();
				}
			}
		}
    }
    
    /**
     * 
     * 功能：定义导出的资产表头部信息
     * @return
     */
	private List<String> getExportHeadList(){		
		List<String> headList = new ArrayList<String>();
			headList.add("序号");
			headList.add("设备名称");		
			headList.add("设备组");
			headList.add("设备状态");
			headList.add("使用科室");
			headList.add("当前位置");
			headList.add("使用时间");
			headList.add("停用时间");
			headList.add("使用时长");					
		
		return headList;
	}
	 /**
     * 
     * 功能：定义导出的各个字段属性列表
     * @return
     */
	private List<String> getExportKeyList(){
		List<String> exportKey = new ArrayList<String>();
		exportKey.add("no");
		exportKey.add("goodName");
		exportKey.add("capitalGroup");
		exportKey.add("statusStr");
		exportKey.add("useGroupName");
		exportKey.add("location");
		exportKey.add("startUseTime");
		exportKey.add("endUseTime");
		exportKey.add("useTimeLength");
		return exportKey;
	}
		
	
	public CapitalVo getCapitalVo() {
		return capitalVo;
	}

	public void setCapitalVo(CapitalVo capitalVo) {
		this.capitalVo = capitalVo;
	}

	
    
	
}
