/**
 * 
 */
package cn.com.qytx.cbb.capital.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.domain.CapitalImportVo;
import cn.com.qytx.cbb.capital.service.ICapital;
import cn.com.qytx.cbb.capital.service.ICapitalGroup;
import cn.com.qytx.cbb.capital.util.excel.ExcelImport;
import cn.com.qytx.cbb.file.config.FilePathConfig;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月28日
 * 修改日期: 2017年4月28日
 * 修改列表: 
 */
public class CapitalImportAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Resource(name="filePathConfig")
    private FilePathConfig filePathConfig;
	
	
	@Autowired
	private IGroup groupService;
	
	@Autowired
	private ICapital capitalService;
	
	@Autowired
	private ICapitalGroup capitalGroupService;
	
	protected File fileToUpload;
    private String uploadFileName;//设置上传文件的文件名
    private String file;//上传的文件
    private List<CapitalImportVo> erroImportList=new ArrayList<CapitalImportVo>();
    private String errorFile;
    
    private Integer type;
    
     
    public String importCapital(){
    	try {
    	UserInfo userInfo = this.getLoginUser();
    	if(userInfo != null){
        	String result = "";
            if (uploadFile()) {
            	File tempFile = new File(file);
            	if(!tempFile.exists()){
            		result = "要导入的文件不存在,请重新导入!";
            		return null;
            	}
                ExcelImport<CapitalImportVo> schoolExcel = new ExcelImport(CapitalImportVo.class);
        	    List<CapitalImportVo> list = (ArrayList) schoolExcel.importExcel(1,tempFile);
        	    int importSuccessNum = 0;
        	    if(list != null && list.size() > 0){
        	    	String name = "";
        	    	String capitalNo = "";
        	    	String storeLocation = "";
        	    	String capitalGroup = "";
        	    	String capitalModel = "";
        	    	String capitalBrand = "";
        	    	String supplier = "";
        	    	for(CapitalImportVo v:list){
        	    		boolean isErrorRecord = true;
        	    		name = v.getName();
        	    		capitalNo = v.getCapitalNo();
        	    		storeLocation = v.getStoreLocation();
        	    		capitalGroup =v.getCapitalGroup();
        	    		capitalModel = v.getCapitalModel();
        	    		capitalBrand = v.getCapitalBrand();
        	    		supplier = v.getSupplier();
        	    		StringBuffer errorRecord = new StringBuffer();
        	    		if(StringUtils.isNotBlank(capitalNo)){
        	    			int num=capitalService.getCurrCodeMaxCapitalNum(capitalNo, userInfo.getCompanyId());
        	    			if(num>0){
        	    				errorRecord.append("<设备编号不能重复!>");
        	    				isErrorRecord = false;
        	    			}
        	    		}else{
        	    			errorRecord.append("<设备编号不能为空!>");
    	    				isErrorRecord = false;
        	    		}
        	    		
        	    	
        	    		if(!StringUtils.isNotBlank(name)){
        	    			errorRecord.append("<设备名称不能为空!>");
    	    				isErrorRecord = false;
        	    		}
        	    		
        	    		if(!StringUtils.isNotBlank(storeLocation)){
        	    			errorRecord.append("<当前位置不能为空!>");
    	    				isErrorRecord = false;
        	    		}
        	    		CapitalGroup cg= capitalGroupService.findModel(capitalGroup, userInfo.getCompanyId());
        	    		if(!StringUtils.isNotBlank(capitalGroup)){
        	    			errorRecord.append("<设备组不能为空!>");
    	    				isErrorRecord = false;
        	    		}else{
        	    			if(cg==null){
        	    				errorRecord.append("<设备组不存在!>");
        	    				isErrorRecord = false;
        	    			}
        	    		}
        	    		//验证是否是正确的
        	    		if(isErrorRecord){
        	    			Capital c= new Capital();
         	    			c.setName(name);
         	    			c.setCapitalNo(capitalNo);
         	    			c.setStoreLocation(storeLocation);
         	    			c.setCapitalGroup(cg);
         	    			c.setCapitalModel(capitalModel);
         	    			c.setStatus(1);
         	    			c.setCapitalBrand(capitalBrand);
         	    			c.setSupplier(supplier);
         	    			c.setIsDelete(0);
         	    			c.setCompanyId(userInfo.getCompanyId());
         	    			capitalService.saveOrUpdate(c);
        	    			importSuccessNum ++;
        	    		}else{
        	    			v.setImportErrorRecord(errorRecord.toString());
        	    			erroImportList.add(v);
        	    		}
        	    	}
        	    	
    	    	}
                String basePath = getRequest().getScheme() + "://" + getRequest().getServerName() + ":" + getRequest().getServerPort()+"/";
        	    if(erroImportList != null && erroImportList.size() > 0){
        	    	errorFile= exportResult();
        	    	result = "导入成功"+importSuccessNum+"条记录,"+erroImportList.size()+"条记录导入失败,请您<a href='"+basePath+"files"+errorFile+"'><font color='blue'>点击下载失败文件</font>"+"</a>";
        	    }else{
        	    	result = "导入成功!";
        	    }
        	    ajax(result);
            }
    	}
    } catch (Exception e) {
    	e.printStackTrace();
            LOGGER.error(e.getMessage());
            ajax("对不起！导入文件时出错！");
        }
        return null;
    	
    	
    }
    
    
    /**
     * 导出结果
     */
    private String exportResult()
    {
        String filePath="";
        try
        {
           if(erroImportList!=null && erroImportList.size()>0)
           {
               List<String> headList=new ArrayList<String>();
               headList.add("设备名称");
               headList.add("设备编号");
               headList.add("当前位置");
               headList.add("设备组");
               headList.add("设备型号");
               headList.add("品牌信息");
               headList.add("厂商信息");
               List<Map<String, Object>> mapList=new ArrayList<Map<String, Object>>();  //表内容
               Map<String, Object> map=null;
               int no = 1;
               for(CapitalImportVo Vo:erroImportList)
               {
                    map=new HashMap<String, java.lang.Object>();
                    map.put("name",Vo.getName());
	                map.put("capitalNo", Vo.getCapitalNo());
	                map.put("storeLocation", Vo.getStoreLocation());
	                map.put("capitalGroup", Vo.getCapitalGroup());
	                map.put("capitalModel", Vo.getCapitalModel());
	                map.put("capitalBrand", Vo.getCapitalBrand());
	                map.put("supplier", Vo.getSupplier());
	                map.put("failureReason",Vo.getImportErrorRecord());
                    mapList.add(map);
               }
               List<String> keyList=new ArrayList<String>();   //map的key
               keyList.add("name");
               keyList.add("capitalNo");
               keyList.add("storeLocation");
               keyList.add("capitalGroup");
               keyList.add("capitalModel");
               keyList.add("capitalBrand");
               keyList.add("supplier");
               keyList.add("failureReason");//导入错误提示记录
               String fileName="eventImport-"+UUID.randomUUID().toString()+".xls";
               String uploadPath = ""; // 上传的目录
               String catalinaHome = filePathConfig.getFileUploadPath();
               SimpleDateFormat sp = new SimpleDateFormat("MM/dd");
               String datePath = sp.format(new Date()); // 每月一个上传目录
               uploadPath = "/upload/importFail/" + datePath + "/";
               String path = catalinaHome + uploadPath;
               File checkPath = new File(path);
               if (!checkPath.exists()) {
                   //目录不存在，则创建目录
                   checkPath.mkdirs();
               }
               OutputStream os = new FileOutputStream(path+"/"+fileName);
               ExportExcel export=new ExportExcel(os,headList,mapList,keyList);
               export.export();
               filePath = uploadPath+fileName;
           }
        }
        catch (Exception ex)
        {
            LOGGER.error(ex.getMessage());
            ex.printStackTrace();
        }
        return filePath;
    }
    
    
    /**
     * 上传文件
     */
    @SuppressWarnings("unused")
	private boolean uploadFile() {
        String uploadPath = ""; // 上传的目录
        String catalinaHome = filePathConfig.getFileUploadPath();
        SimpleDateFormat sp = new SimpleDateFormat("MM/dd");
        String datePath = sp.format(new Date()); // 每月一个上传目录
        uploadPath = "/upload/import/" + datePath + "/";
        String targetDirectory = catalinaHome + uploadPath;
        try {
            if (fileToUpload != null) {
                String fileName = UUID.randomUUID().toString();
                String ext = ".xls";
                if (uploadFileName != null) {
                    ext = uploadFileName.substring(uploadFileName.lastIndexOf("."));
                }
                if (!ext.equals(".xls")) {
                    return false;
                }
                String saveFileName = fileName + ext;
                File savefile = new File(new File(targetDirectory), saveFileName);
                if (!savefile.getParentFile().exists()){
                    boolean result = savefile.getParentFile().mkdirs();
                    if (!result){
                        return false;
                    }
                }
                FileUtils.copyFile(fileToUpload, savefile);//拷贝文件
                file = targetDirectory + "/" + saveFileName;
            }

        } catch (Exception e) {

            return false;
        }
        return true;
    }
    
    /**
     * 判断字符串是否是整数
     */
    public static boolean isInteger(String value) {
     try {
      Integer.parseInt(value);
      return true;
     } catch (NumberFormatException e) {
      return false;
     }
    }

    
 
  


	public File getFileToUpload() {
		return fileToUpload;
	}


	public String getUploadFileName() {
		return uploadFileName;
	}


	public String getFile() {
		return file;
	}


	


	public String getErrorFile() {
		return errorFile;
	}


	public void setFileToUpload(File fileToUpload) {
		this.fileToUpload = fileToUpload;
	}


	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}


	public void setFile(String file) {
		this.file = file;
	}


	


	public void setErrorFile(String errorFile) {
		this.errorFile = errorFile;
	}


	public Integer getType() {
		return type;
	}


	public void setType(Integer type) {
		this.type = type;
	}


	public List<CapitalImportVo> getErroImportList() {
		return erroImportList;
	}


	public void setErroImportList(List<CapitalImportVo> erroImportList) {
		this.erroImportList = erroImportList;
	}

   
	
	
	

}
