package cn.com.qytx.cbb.capital.service;


import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能:资产日志接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
public interface ICapitalUseLog extends BaseService<CapitalUseLog>{

	public Page<CapitalUseLog> findCapitalUseLogPage(Pageable pageable,CapitalVo capitalVo);
	
	public CapitalUseLog getUnEndUseCapitalUseLog(Integer capitalId);
	
	/**
	 * 查找最新的使用记录
	 * @return
	 */
	public CapitalUseLog findUseCapitalUseLog(Integer capitalId);
}
