package cn.com.qytx.cbb.capital.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.capital.dao.CapitalLogDao;
import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.event.EventForAddLog;
/**
 * 功能:保存日志
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月16日
 * 修改日期: 2016年8月16日
 * 修改列表: 
 */
@Component
@Transactional
public class AddLogListener implements ApplicationListener<EventForAddLog>{

	@Autowired
	private CapitalLogDao capitalLogDao;
	
	@Override
	public void onApplicationEvent(EventForAddLog event) {
		if(event.getSource() instanceof CapitalLog){
			CapitalLog log = (CapitalLog)event.getSource();
			capitalLogDao.saveOrUpdate(log);
		}
	}

}
