package cn.com.qytx.cbb.capital.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.capital.dao.CapitalGroupDao;
import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.service.ICapitalGroup;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能:资产组接口实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
@Service("capitalGroupService")
@Transactional
public class CapitalGroupImpl extends BaseServiceImpl<CapitalGroup> implements ICapitalGroup{

	@Resource(name="capitalGroupDao")
	private CapitalGroupDao capitalGroupDao;
	
	@Override
	public List<CapitalGroup> findCapitalGroupList(Integer companyId,String capitalGroupName) {
		return capitalGroupDao.findCapitalGroupList(companyId,capitalGroupName);
	}

	@Override
	public List<TreeNode> getTreeCapitalGroupList(String path,Integer companyId) {
		List<CapitalGroup> cgList = this.findCapitalGroupList(companyId,null);
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("设备组");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen(true);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(CapitalGroup cg:cgList){
				TreeNode treeNode = new TreeNode();
				treeNode.setId("gid_"+cg.getId());
				treeNode.setName(cg.getGroupName());
				treeNode.setPId("gid_"+cg.getParentId());
				treeNode.setObj(cg.getOrderIndex()+"{1}"+cg.getCapitalCode());// 排序号和编码
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	private Map<Integer,Integer> getSubListCountMap(List<CapitalGroup> cgList){
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		for(CapitalGroup cg:cgList){
			Integer parentId = cg.getParentId();
			if(map.containsKey(parentId)){
				map.put(parentId, map.get(parentId)+1);
			}else{
				map.put(parentId, 1);
			}
		}
		
		return map;
	}
	

	@Override
	public List<TreeNode> getTreeCapitalGroupList_Select(String path,
			Integer companyId) {
		List<CapitalGroup> cgList = this.findCapitalGroupList(companyId,null);
		Map<Integer,Integer> cgMap = this.getSubListCountMap(cgList);
		List<TreeNode> tnList = new ArrayList<TreeNode>();
		TreeNode firstNode = new TreeNode();
		firstNode.setId("gid_0");// 部门ID前加gid表示类型为部门
		firstNode.setName("设备组");
		firstNode.setPId("gid_-1");
		firstNode.setIcon(path + "/images/company.png");
		firstNode.setOpen(true);
		tnList.add(firstNode);
		if(cgList!=null&&cgList.size()>0){
			for(CapitalGroup cg:cgList){
				TreeNode treeNode = new TreeNode();
				if(cgMap!=null&&!cgMap.containsKey(cg.getId())){
					treeNode.setId("uid_"+cg.getId());
				}else{
					treeNode.setId("gid_"+cg.getId());
				}
				treeNode.setName(cg.getGroupName());
				treeNode.setPId("gid_"+cg.getParentId());
				treeNode.setObj(cg.getOrderIndex());// 排序号
				treeNode.setIcon(path + "/images/group.png");
				tnList.add(treeNode);
			}
		}
		return tnList;
	}

	@Override
	public boolean isHasSameGroupName(Integer parentId, String groupName,
			int companyId) {
		return capitalGroupDao.isHasSameGroupName(parentId, groupName, companyId);
	}

	@Override
	public boolean isHasSameCapitalCode(Integer parentId, String capitalCode,
			int companyId) {
		return capitalGroupDao.isHasSameCapitalCode(parentId, capitalCode, companyId);
	}

	@Override
	public boolean isHasChildCapitalGroup(Integer id, int companyId) {
		return capitalGroupDao.isHasChildCapitalGroup(id,companyId);
	}

	@Override
	public List<CapitalGroup> getCapitalGroupListByIds(String Ids) {
		return capitalGroupDao.getCapitalGroupListByIds(Ids);
	}

	@Override
	public CapitalGroup findModel(String name, int companyId) {
		// TODO Auto-generated method stub
		return capitalGroupDao.findModel(name, companyId);
	}

}
