package cn.com.qytx.cbb.capital.domain;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cn.com.qytx.platform.base.domain.DeleteState;
import cn.com.qytx.platform.org.domain.GroupInfo;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:资产表
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月9日
 * 修改日期: 2016年8月9日
 * 修改列表: 
 */
@Entity
@Table(name="tb_cbb_capital")
public class Capital implements java.io.Serializable{
	private static final long serialVersionUID = -3806018446633238202L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	/**
	 * 审批编号
	 */
	@Column(name="approve_no")
	private String approveNo;
	
	/**
	 * 资产类型 1固定资产 2低值易耗品
	 */
	@Column(name="capital_type")
	private Integer capitalType;
	
	/**
	 * 资产组Id
	 */
	@JoinColumn(name="capital_group_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private CapitalGroup capitalGroup;
	
	/**
	 * 资产名称/物品名称
	 */
	@Column(name="name")
	private String name;
	
	/**
	 * 物品编号
	 */
	@Column(name="capital_no")
	private String capitalNo;
	
	/**
	 * 单位-数据字典
	 */
	@Column(name="capital_unit")
	private Integer capitalUnit;
	
	/**
	 * 单位-字符串
	 */
	@Column(name="capital_unit_name")
	private String capitalUnitName;
	
	/**
	 * 资产品牌
	 */
	@Column(name="capital_brand")
	private String capitalBrand;
	
	/**
	 * 资产型号
	 */
	@Column(name="capital_model")
	private String capitalModel;
	
	
	/**
	 * 存放地点
	 */
	@Column(name="store_location")
	private String storeLocation;
	
	/**
	 * 入库日期
	 */
	@Column(name="create_time")
	private Timestamp createTime;
	
	
	/**
	 * 供应商
	 */
	@Column(name="supplier")
	private String supplier;
	
	
	/**
	 * 发票号码
	 */
	@Column(name="invoice_number")
	private String invoiceNumber;
	
	/**
	 * 价格
	 */
	@Column(name="price")
	private Float price;
	
	/**
	 * 购买日期
	 */
	@Column(name="buy_date")
	private Timestamp buyDate;
	
	
	/**
	 * 配送人Id
	 */
	@JoinColumn(name="handle_user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo handleUser;
	
	
	/**
	 * 创建人
	 */
	@Column(name="create_user_id")
	private Integer createUserId;
	
	/**
	 * 修改人
	 */
	@Column(name="update_user_id")
	private Integer updateUserId;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time")
	private Timestamp updateTime;
	
	/**
	 * 物品状态 1在库 2 使用中 3已停用 4 待归库 5 已禁用 6已送达
	 */
	@Column(name="status")
	private Integer status;
	
	/**
	 * 借用人
	 */
	@JoinColumn(name="use_user_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private UserInfo useUser;
	
	
	@JoinColumn(name="use_group_id")
	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.LAZY)
	private GroupInfo useGroup;
	
	/**
	 * 修理次数
	 */
	@Column(name="repair_num")
	private Integer repairNum=0;
	
	/**
	 * 使用次数
	 */
	@Column(name="use_num")
	private Integer useNum=0;
	
	/**
	 * 盘点次数
	 */
	@Column(name="check_num")
	private Integer checkNum=0;
	
	
	/**
	 * 单位Id
	 */
	@Column(name="company_id")
	private Integer companyId;
	
	/**
	 * 备注
	 */
	@Column(name="remark")
	private String remark;
	
	/**
	 * 资产数量/剩余数量
	 */
	@Column(name="capital_num")
	private Integer capitalNum=0;
	
	/**
	 * 原始数量
	 */
	@Column(name="old_num")
	private Integer oldNum;
	
	/**
	 * 领用数量
	 */
	@Column(name="use_count")
	private Integer useCount=0;
	
	/**
	 * 使用时间/领用
	 */
	@Column(name="user_time")
	private Timestamp useTime;
	
	/**
	 * 报废时间
	 */
	@Column(name="scrap_time")
	private Timestamp scrapTime;
	
	/**
	 * 批准记录
	 */
	@Column(name="approve_record")
	private String approveRecord;
	
	/**
	 * 1 已删除 0 未删除
	 */
	@Column(name="is_delete")
	@DeleteState
	private Integer isDelete;
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApproveNo() {
		return approveNo;
	}

	public void setApproveNo(String approveNo) {
		this.approveNo = approveNo;
	}

	public Integer getCapitalType() {
		return capitalType;
	}

	public void setCapitalType(Integer capitalType) {
		this.capitalType = capitalType;
	}

	public CapitalGroup getCapitalGroup() {
		return capitalGroup;
	}

	public void setCapitalGroup(CapitalGroup capitalGroup) {
		this.capitalGroup = capitalGroup;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapitalNo() {
		return capitalNo;
	}

	public void setCapitalNo(String capitalNo) {
		this.capitalNo = capitalNo;
	}

	public Integer getCapitalUnit() {
		return capitalUnit;
	}

	public void setCapitalUnit(Integer capitalUnit) {
		this.capitalUnit = capitalUnit;
	}

	public String getCapitalUnitName() {
		return capitalUnitName;
	}

	public void setCapitalUnitName(String capitalUnitName) {
		this.capitalUnitName = capitalUnitName;
	}

	public String getCapitalBrand() {
		return capitalBrand;
	}

	public void setCapitalBrand(String capitalBrand) {
		this.capitalBrand = capitalBrand;
	}

	public String getCapitalModel() {
		return capitalModel;
	}

	public void setCapitalModel(String capitalModel) {
		this.capitalModel = capitalModel;
	}

	public String getStoreLocation() {
		return storeLocation;
	}

	public void setStoreLocation(String storeLocation) {
		this.storeLocation = storeLocation;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Float getPrice() {
		return price;
	}

	public Timestamp getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Timestamp buyDate) {
		this.buyDate = buyDate;
	}

	public UserInfo getHandleUser() {
		return handleUser;
	}

	public void setHandleUser(UserInfo handleUser) {
		this.handleUser = handleUser;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public UserInfo getUseUser() {
		return useUser;
	}

	public void setUseUser(UserInfo useUser) {
		this.useUser = useUser;
	}

	public Integer getRepairNum() {
		return repairNum;
	}

	public void setRepairNum(Integer repairNum) {
		this.repairNum = repairNum;
	}

	public Integer getUseNum() {
		return useNum;
	}

	public void setUseNum(Integer useNum) {
		this.useNum = useNum;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getCapitalNum() {
		return capitalNum;
	}

	public void setCapitalNum(Integer capitalNum) {
		this.capitalNum = capitalNum;
	}

	public Timestamp getUseTime() {
		return useTime;
	}

	public void setUseTime(Timestamp useTime) {
		this.useTime = useTime;
	}

	public Timestamp getScrapTime() {
		return scrapTime;
	}

	public void setScrapTime(Timestamp scrapTime) {
		this.scrapTime = scrapTime;
	}

	public String getApproveRecord() {
		return approveRecord;
	}

	public void setApproveRecord(String approveRecord) {
		this.approveRecord = approveRecord;
	}

	public Integer getCheckNum() {
		return checkNum;
	}

	public void setCheckNum(Integer checkNum) {
		this.checkNum = checkNum;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getOldNum() {
		return oldNum;
	}

	public void setOldNum(Integer oldNum) {
		this.oldNum = oldNum;
	}

	public Integer getUseCount() {
		return useCount;
	}

	public void setUseCount(Integer useCount) {
		this.useCount = useCount;
	}

	public GroupInfo getUseGroup() {
		return useGroup;
	}

	public void setUseGroup(GroupInfo useGroup) {
		this.useGroup = useGroup;
	}
	
}
