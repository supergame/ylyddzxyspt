package cn.com.qytx.cbb.goods.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.dict.domain.Dict;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.cbb.goods.service.IGood;
import cn.com.qytx.cbb.goods.service.IGoodGroup;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.base.query.Sort.Order;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;

public class GoodWapAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7085403177124323059L;

	@Autowired
	private IUser userService;
	
	@Autowired
	private IGood goodService;
	
	@Autowired
	private IDict dictService;
	
	@Autowired
	private IGoodGroup goodGroupService;
	
	/**
	 * 用户Id
	 */
	private Integer userId;
	
	private String name;
	
	private Integer groupId;
	
	/**
	 * 功能：获得资产列表
	 */
	public void getGoodList() {
		
		try {
			if (userId == null) {
				ajax("101||参数异常!");
				return;
			}
			UserInfo user = userService.findOne(userId);
			if (user != null) {
				 Order order = new Order(Direction.DESC, "createTime");
		         Sort s = new Sort(order);
		         Pageable pageable = getPageable(s);
				 Page<Good> pageList = goodService.findGoodPage(pageable, name, null, null, user.getCompanyId(),groupId);
			     List<Good> list = pageList.getContent();
			     List<Map<String,Object>> goodList=analyzeResult(list, pageable);
				Gson gson = new Gson();
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", pageList.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", pageList.getTotalElements());
				jsonMap.put("aaData", goodList);
				ajax("100||" + gson.toJson(jsonMap));
			}

		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	 /**
		 * 
		 * 功能：查询的列表转换成MAp
		 * @param list
		 * @param pageable
		 * @param companyId
		 * @return
		 */
		private List<Map<String,Object>> analyzeResult(List<Good> list,Pageable pageable){
			List<Map<String,Object>> mapList=new ArrayList<Map<String,Object>>();  
			if(list!=null&&list.size()>0){
				    int i = 1;
					if(pageable!=null){
		    			i = pageable.getPageNumber() * pageable.getPageSize() + 1;
		    		}
					for(Good good:list){
						Map<String,Object> map=new HashMap<String, Object>();
						map.put("no", i);
						map.put("id",good.getId());//ID
						map.put("name",good.getName());//物品名称
						map.put("goodGroup", good.getGoodGroup().getGroupName());//资产组
						map.put("goodlNo", good.getGoodNo());//资产编号
						map.put("type", getGoodType(good.getType()));//资产类型
						map.put("specifications",good.getSpecifications()==null?"--":good.getSpecifications() );
						map.put("unit",good.getUnit()==null?"--":findUnitName(good.getUnit()));
						map.put("price",good.getPrice());
						mapList.add(map);
						i++;
					}
			   }
			   return mapList;
		}
		/*
		 * 获得物资类型	
		 */
		private  String getGoodType(Integer type){
			String typeStr="";
			if(type==null){
				typeStr=" ";
			}else if(type==1){
				typeStr="备件";
			}else if(type==2){
				typeStr="材料";
			}else if(type==3){
				typeStr="设备";
			}else if(type==4){
				typeStr="仪表";
			}else {
				typeStr="其他";
			}
			return typeStr;
		}
		
		
		private String findUnitName(Integer unitId){
			List<Dict> list= dictService.findList("goodsType", 1);
			String name="";
			if(list!=null && list.size()>0){
				for(Dict d:list){
					if(unitId==d.getValue()){
						name=d.getName();
					}
				}
			}
			return name;
		}
		
		/**
		 * 获物资数列表
		 */
		public String goodGroupTreeList() {
			try {
				 if(userId==null){
			        	ajax("101||参数异常!");
			        	return null;
			        }
				UserInfo userInfo = userService.findOne(userId);
				String contextPath = getRequest().getContextPath();
				List<TreeNode> list = null;
				list = goodGroupService.getTreeGoodGroupList(contextPath, userInfo.getCompanyId());
				Gson json = new Gson();
				ajax("100||"+json.toJson(list));
			} catch (Exception e) {
				e.printStackTrace();
				ajax("102||程序异常");
			}
			return null;
		}
		public IUser getUserService() {
			return userService;
		}
		public void setUserService(IUser userService) {
			this.userService = userService;
		}
		public Integer getUserId() {
			return userId;
		}
		public void setUserId(Integer userId) {
			this.userId = userId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getGroupId() {
			return groupId;
		}
		public void setGroupId(Integer groupId) {
			this.groupId = groupId;
		}
		
}

