package cn.com.qytx.cbb.capital.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.capital.dao.CapitalDao;
import cn.com.qytx.cbb.capital.dao.CapitalUseLogDao;
import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalLog;
import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.event.EventForAddLog;
import cn.com.qytx.cbb.capital.service.ICapital;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.utils.spring.SpringUtil;

/**
 * 功能:资产业务实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
@Service("capitalService")
@Transactional
public class CapitalImpl extends BaseServiceImpl<Capital> implements ICapital{

	@Resource(name="capitalDao")
	private CapitalDao capitalDao;
	
	@Resource(name="capitalUseLogDao")
	private CapitalUseLogDao capitalUseLogDao;
	
	@Override
	public Page<Capital> findCapitalPage(Pageable pageable, CapitalVo capitalVo) {
		return capitalDao.findCapitalPage(pageable,capitalVo);
	}

	@Override
	public int getCurrCodeMaxCapitalNum(String capitalCode, int companyId) {
		return capitalDao.getCurrCodeMaxCapitalNum(capitalCode,companyId);
	}

	/* (non-Javadoc)
	 * @see cn.com.qytx.cbb.capital.service.ICapital#getCurrNoMaxCapitalNum(java.lang.String, int)
	 */
	@Override
	public int getCurrNoMaxCapitalNum(String capitalCode, int companyId) {
		// TODO Auto-generated method stub
		return capitalDao.getCurrNoMaxCapitalNum(capitalCode, companyId);
	}

	@Override
	public void capitalUse(String ipAddress,UserInfo userInfo,Capital capital,Integer status,String startUseTime,String endUseTime) {
		Capital ct = capitalDao.findOne(capital.getId());
		ct.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		ct.setUpdateUserId(userInfo.getUserId());
		ct.setStatus(status);
		if(StringUtils.isNotBlank(capital.getStoreLocation())){
			ct.setStoreLocation(capital.getStoreLocation());
		}
		if(capital.getUseGroup()!=null&&capital.getUseGroup().getGroupId()!=null){
			ct.setUseGroup(capital.getUseGroup());
		}
		Timestamp currTs = new Timestamp(System.currentTimeMillis());
		CapitalUseLog cul = capitalUseLogDao.getUnEndUseCapitalUseLog(capital.getId());
		if(cul!=null){
			if(StringUtils.isNotBlank(endUseTime)){
				cul.setUseEndTime(Timestamp.valueOf(endUseTime));
			}else{
				cul.setUseEndTime(currTs);
			}
			Long useLongTime=(cul.getUseEndTime().getTime()-cul.getUseStartTime().getTime())/1000;
			cul.setUseLongTime(useLongTime.intValue());
			cul.setStopUser(userInfo);
		}else{
			cul = new CapitalUseLog();
			cul.setCompanyId(userInfo.getCompanyId());
			if(StringUtils.isNotBlank(startUseTime)){
				cul.setUseStartTime(Timestamp.valueOf(startUseTime));
			}else{
				cul.setUseStartTime(currTs);
			}
			cul.setUseGroup(ct.getUseGroup());
			cul.setCreateTime(currTs);
			cul.setCreateUser(userInfo);
			cul.setIsDelete(0);
			cul.setCapital(ct);
		}
		capitalDao.saveOrUpdate(ct);
		capitalUseLogDao.saveOrUpdate(cul);
		
		/*添加日志开始*/
		CapitalLog log = new CapitalLog();
		log.setCapitalGroup(ct.getCapitalGroup());//日志
		log.setCapitalName(ct.getName());
		log.setCapitalNo(ct.getCapitalNo());
		log.setCompanyId(userInfo.getCompanyId());
		log.setCreateTime(new Timestamp(System.currentTimeMillis()));
		log.setCreateUser(userInfo);
		log.setIpAddress(ipAddress);
		if(status.intValue()==2){
			log.setType(6);
			log.setRemark("使用成功");
		}else{
			log.setType(7);
			log.setRemark("停用成功");
		}
		
		SpringUtil.getApplicationContext().publishEvent(new EventForAddLog(log));//发布添加日志广播
	}

}
