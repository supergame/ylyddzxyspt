package cn.com.qytx.cbb.capital.action;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.service.ICapital;
import cn.com.qytx.cbb.capital.service.ICapitalUseLog;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.DateUtils;

public class CapitalUseLogWapAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4836468047636297770L;

	private Integer capitalId;
	/**
	 * 资产使用日志接口
	 */
	@Resource(name="capitalUseLogService")
	private ICapitalUseLog capitalUseLogService;
	@Autowired
	private ICapital capitalService;
	
	
	@Autowired
	private IUser userService;
	
	private Integer userId;
	
	
	public void findCapitalUseLog(){
		try {
			
			if (userId == null) {
				ajax("101||参数异常!");
				return;
			}
			UserInfo user = userService.findOne(userId);
			if(user!=null){
				SimpleDateFormat sdfYYYYMMDDHHMM = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				CapitalUseLog capitalUseLog=capitalUseLogService.findUseCapitalUseLog(capitalId);
				Capital capital= capitalService.findOne(capitalId);
				
				List<Map<String,Object>> listMap= new ArrayList<Map<String,Object>>();
				Map<String,Object> mapStartUse=new HashMap<String, Object>();
				Map<String,Object> mapEndUse=new HashMap<String, Object>();
				if(capitalUseLog!=null){
					//开启
					mapStartUse.put("time",capitalUseLog.getUseStartTime()!=null?sdfYYYYMMDDHHMM.format(capitalUseLog.getUseStartTime()):"--" );//开始时间
					mapStartUse.put("groupName",capitalUseLog.getUseGroup()!=null?capitalUseLog.getUseGroup().getGroupName():"--");
					//操作人
					mapStartUse.put("pperator", capitalUseLog.getCreateUser()!=null?capitalUseLog.getCreateUser().getUserName():"--");
					//使用地点
					mapStartUse.put("locationName",capital.getStoreLocation()!=null?capital.getStoreLocation():"--" );
					String timeLength = "--";
					if(capitalUseLog.getUseStartTime()!=null){
				    	timeLength = DateUtils.formatHHMMSS((new Time(System.currentTimeMillis()).getTime()-capitalUseLog.getUseStartTime().getTime())/1000);
				    }
					mapStartUse.put("userTime", timeLength);
					mapStartUse.put("type", 1);//开启
					listMap.add(mapStartUse);
					//停止
					if(capitalUseLog.getUseEndTime()!=null){
						mapEndUse.put("time",capitalUseLog.getUseEndTime()!=null?sdfYYYYMMDDHHMM.format(capitalUseLog.getUseEndTime()):"--" );//停止时间
						//操作人
						mapEndUse.put("pperator", capitalUseLog.getStopUser()!=null?capitalUseLog.getStopUser().getUserName():"--");
						String timeLengthEnd = "--";
						if(capitalUseLog.getUseStartTime()!=null && capitalUseLog.getUseEndTime()!=null){
							timeLengthEnd = DateUtils.formatHHMMSS((capitalUseLog.getUseEndTime().getTime()-capitalUseLog.getUseStartTime().getTime())/1000);
					    }
						mapEndUse.put("userTime", timeLengthEnd);
						mapEndUse.put("type", 2);//开启
						listMap.add(mapEndUse);
					}
					Gson gson = new Gson();
					ajax("100||" + gson.toJson(listMap));
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	
	public Integer getCapitalId() {
		return capitalId;
	}

	public void setCapitalId(Integer capitalId) {
		this.capitalId = capitalId;
	}


	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
	
	
}
