package cn.com.qytx.cbb.capital.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.service.ICapital;
import cn.com.qytx.cbb.capital.service.ICapitalGroup;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.base.query.Sort.Order;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IUser;
import cn.com.qytx.platform.utils.DateUtils;

public class CapitalWapAction extends BaseActionSupport {
	private static final long serialVersionUID = 690397804391727778L;

	@Autowired
	ICapital icapital;

	@Autowired
	ICapitalGroup iCapitalGroup;
	
	

	// 资产显示实体
	private CapitalVo capitalVo;

	@Resource
	private IUser userService;
	
	/**
	 * 资产组业务接口
	 */
	@Resource(name = "capitalGroupService")
	private ICapitalGroup capitalGroupService;
	

	/**
	 * 资产Id
	 */
	private Integer capitalId;
	
	/**
	 * 资产
	 */
	private Capital capital;
	/**
	 * 用户Id
	 */
	private Integer userId;
	
	private String capitalGroupName;

	
	/**
	 * 功能：获得资产列表
	 */
	public void getCapitalList() {
		
		try {
			if (userId == null) {
				ajax("101||参数异常!");
				return;
			}
			UserInfo user = userService.findOne(userId);
			if (user != null) {
				capitalVo.setCompanyId(user.getCompanyId());
				if (capitalVo.getType() != null) {
					if (capitalVo.getType() == 2) {// 查看本部门
						capitalVo.setCurrentGroupId(user.getGroupId());
					}else{
						capitalVo.setCurrentGroupId(capitalVo.getUserGroupId());
					}
				}
				Order order = new Order(Direction.DESC, "createTime");
				Sort s = new Sort(order);
				Pageable pageable = getPageable(s);
				Page<Capital> pageInfo = icapital.findCapitalPage(pageable,
						capitalVo);
				List<Capital> list = pageInfo.getContent();
				List<Map<String, Object>> capitalList = analyzeResult(list,
						pageable);
				Gson gson = new Gson();
				Map<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("iTotalRecords", pageInfo.getTotalElements());
				jsonMap.put("iTotalDisplayRecords", pageInfo.getTotalElements());
				jsonMap.put("aaData", capitalList);
				ajax("100||" + gson.toJson(jsonMap));
			}

		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	
	/**
	 * 获取资源组列表
	 */
	public void capitalGroupList() {
		try {
			List<CapitalGroup> list = capitalGroupService.findCapitalGroupList(1, capitalGroupName);
			ajax(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 功能：查询的列表转换成MAp
	 * 
	 * @param list
	 * @param pageable
	 * @param companyId
	 * @return
	 */
	private List<Map<String, Object>> analyzeResult(List<Capital> list,
			Pageable pageable) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		if (list != null && list.size() > 0) {
			int i = 1;
			if (pageable != null) {
				i = pageable.getPageNumber() * pageable.getPageSize() + 1;
			}
			for (Capital capital : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("no", i);
				map.put("id", capital.getId());// ID
				map.put("goodName", capital.getName());// 物品名称
				map.put("capitalGroup", capital.getCapitalGroup()
						.getGroupName());// 资产组
				map.put("capitalNo", capital.getCapitalNo());// 资产编号
				map.put("capitalModel", StringUtils.isNotBlank(capital
						.getCapitalModel()) ? capital.getCapitalModel() : "--");// 资产型号
				map.put("status", capital.getStatus());// 资产状态
				map.put("statusStr", capital.getStatus() == null ? "--"
						: getStateStr(capital.getStatus()));// 资产状态
				map.put("location", capital.getStoreLocation() == null ? "--"
						: capital.getStoreLocation());// 存放地址
				if (capital.getStatus() == 1 || capital.getStatus() == 5) {// 使用中
					map.put("usePeople", "--");// 借用人
				} else if (capital.getStatus() == 4) {// 配送人
					UserInfo u = capital.getHandleUser();
					map.put("usePeople", u == null ? "--" : u.getUserName());// 借用人
				} else {// 在库/报废
					UserInfo useUser = capital.getUseUser();
					map.put("usePeople",
							useUser == null ? "--" : useUser.getUserName());// 使用人
				}
				map.put("useGroupName", capital.getUseGroup() == null ? "--"
						: capital.getUseGroup().getGroupName());
				
				
				mapList.add(map);
				i++;
			}
		}
		return mapList;
	}
	
	/**
	 * 获得资产状态	
	 */
	public String getStateStr(Integer state){
		String str="";
		if(state==1){
			str= "在库";
		}else if(state==2){
			str= "使用中";
		}else if(state==3){
			str= "已停用";
		}else if(state==4){
			str= "待归库";
		}else if(state==5){
			str= "已禁用";
		}else if(state==6){
			str= "已送达";
		}
		return str;
	}

	

	/**
	 * 获取地点列表
	 */
	public String capitalGroupTreeList() {
		try {
			 if(userId==null){
		        	ajax("101||参数异常!");
		        	return null;
		        }
			UserInfo userInfo = userService.findOne(userId);
			String contextPath = getRequest().getContextPath();
			List<TreeNode> list = null;
			list = iCapitalGroup
					.getTreeCapitalGroupList(contextPath,userInfo.getCompanyId());
			Gson json = new Gson();
			ajax("100||"+json.toJson(list));
		} catch (Exception e) {
			e.printStackTrace();
			ajax("102||程序异常");
		}
		return null;
	}
	
	
	
	
	/**
	 * 获取资产详情
	 */
	public void getCapitalDetail(){
		try{
			 if(userId==null || capitalId==null){
		        	ajax("101||参数异常!");
		        	return;
		        }
			 UserInfo user = userService.findOne(userId);
			if(user!=null){
				Capital c=icapital.findOne(capitalId);
				Map<String,Object> map=new HashMap<String,Object>();
				//资产名称
				map.put("id", c.getId());
				map.put("name", c.getName());
				//资产组
				CapitalGroup currCg = iCapitalGroup.findOne(c.getCapitalGroup().getId());
				List<CapitalGroup> cgList = iCapitalGroup.getCapitalGroupListByIds(currCg.getPathId());
				int cgSize = cgList.size();
				String capitalGroup="设备组>"+"";
				for(int j=0;j<cgSize;j++){
					CapitalGroup tempCg = cgList.get(j);
					if(j==(cgSize-1)){
					capitalGroup += tempCg.getGroupName();
					}else{
						capitalGroup += tempCg.getGroupName()+">";
					}
				}
				map.put("capitalGroupName",currCg.getGroupName() );
				map.put("capitalGroup",capitalGroup);
				//资产组ID
				map.put("capitalGroupId", c.getCapitalGroup().getId());
				//资产编号
				map.put("capitalNo", c.getCapitalNo());
				//使用科室
				map.put("useGroupName", c.getUseGroup()!=null?c.getUseGroup().getGroupName():"");
				map.put("useGroupId", c.getUseGroup()!=null?c.getUseGroup().getGroupId():"");
				
				map.put("statu",c.getStatus());
				map.put("statusStr",c.getStatus()!=null?getStateStr(c.getStatus()):"--");
				//资产型号
				map.put("capitalModel", StringUtils.isNotBlank(c.getCapitalModel())?c.getCapitalModel():"--");
				map.put("capitalBrand", StringUtils.isNotBlank(c.getCapitalBrand())?c.getCapitalBrand():"--");
				map.put("storeLocation", StringUtils.isNotBlank(c.getStoreLocation())?c.getStoreLocation():"--");
				//供应商
				map.put("supplier", StringUtils.isNotBlank(c.getSupplier())?c.getSupplier():"--");
				//备注
				map.put("remark", StringUtils.isNotBlank(c.getRemark())?c.getRemark():"--");
				//资产类型
				map.put("currUser", user.getUserName());
				Gson json=new Gson();
				ajax("100||"+json.toJson(map));
			}
		}catch(Exception e){
			e.printStackTrace();
			ajax("102||程序异常");
		}
	}
	
	
	/**
	 * 设备借用
	 */
	public void capitalBorrow(){
		try {
			if(userId!=null){
				UserInfo user= userService.findOne(userId);
				if(user!=null){
					icapital.capitalUse(null,user,capital,2,DateUtils.date2LongStr(new Date()),null);
					ajax("100||"+1);
				}
			}
		} catch (Exception e) {
            e.printStackTrace();  
            ajax("102||程序异常");
		}
	}
	
	
	/**
	 * 设备停用
	 */
	public void capitalDisable(){
		try {
			if(userId!=null){
				UserInfo user= userService.findOne(userId);
				if(user!=null){
					icapital.capitalUse(null,user,capital,3,null,DateUtils.date2LongStr(new Date()));
					ajax("100||"+1);
				}
			}
		} catch (Exception e) {
            e.printStackTrace();  
            ajax("102||程序异常");
		}
	}	
	public CapitalVo getCapitalVo() {
		return capitalVo;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setCapitalVo(CapitalVo capitalVo) {
		this.capitalVo = capitalVo;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCapitalId() {
		return capitalId;
	}

	public void setCapitalId(Integer capitalId) {
		this.capitalId = capitalId;
	}

	public ICapital getIcapital() {
		return icapital;
	}

	public void setIcapital(ICapital icapital) {
		this.icapital = icapital;
	}

	public Capital getCapital() {
		return capital;
	}

	public void setCapital(Capital capital) {
		this.capital = capital;
	}


	public String getCapitalGroupName() {
		return capitalGroupName;
	}


	public void setCapitalGroupName(String capitalGroupName) {
		this.capitalGroupName = capitalGroupName;
	}	
	

}
