package cn.com.qytx.cbb.capital.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.qytx.cbb.capital.dao.CapitalUseLogDao;
import cn.com.qytx.cbb.capital.domain.CapitalUseLog;
import cn.com.qytx.cbb.capital.service.ICapitalUseLog;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.service.impl.BaseServiceImpl;

/**
 * 功能:资产日志业务实现
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
@Service("capitalUseLogService")
@Transactional
public class CapitalUseLogImpl extends BaseServiceImpl<CapitalUseLog> implements ICapitalUseLog{

	@Resource(name="capitalUseLogDao")
	private CapitalUseLogDao capitalLogDao;

	@Override
	public Page<CapitalUseLog> findCapitalUseLogPage(Pageable pageable,
			CapitalVo capitalVo) {
		return capitalLogDao.findCapitalUseLogPage(pageable, capitalVo);
	}

	@Override
	public CapitalUseLog getUnEndUseCapitalUseLog(Integer capitalId) {
		return capitalLogDao.getUnEndUseCapitalUseLog(capitalId);
	}

	@Override
	public CapitalUseLog findUseCapitalUseLog(Integer capitalId) {
		// TODO Auto-generated method stub
		return capitalLogDao.findUseCapitalUseLog(capitalId);
	}

	

	
	

}
