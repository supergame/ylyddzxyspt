/**
 * 
 */
package cn.com.qytx.cbb.goods.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.qytx.cbb.capital.util.excel.ExcelImport;
import cn.com.qytx.cbb.dict.domain.Dict;
import cn.com.qytx.cbb.dict.service.IDict;
import cn.com.qytx.cbb.file.config.FilePathConfig;
import cn.com.qytx.cbb.goods.domain.Good;
import cn.com.qytx.cbb.goods.domain.GoodGroup;
import cn.com.qytx.cbb.goods.domain.GoodImportVo;
import cn.com.qytx.cbb.goods.service.IGood;
import cn.com.qytx.cbb.goods.service.IGoodGroup;
import cn.com.qytx.cbb.org.util.ExportExcel;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;
import cn.com.qytx.platform.base.query.Sort;
import cn.com.qytx.platform.base.query.Sort.Direction;
import cn.com.qytx.platform.base.query.Sort.Order;
import cn.com.qytx.platform.org.domain.UserInfo;
import cn.com.qytx.platform.org.service.IGroup;

/**
 * 功能: 
 * 版本: 1.0
 * 开发人员: 王刚
 * 创建日期: 2017年4月28日
 * 修改日期: 2017年4月28日
 * 修改列表: 
 */
public class GoodImportAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Resource(name="filePathConfig")
    private FilePathConfig filePathConfig;
	@Autowired
	private IDict dictService;
	
	@Autowired
	private IGroup groupService;
	
	@Autowired
	private IGood goodService;
	
	@Autowired
	private IGoodGroup goodGroupService;
	
	protected File fileToUpload;
    private String uploadFileName;//设置上传文件的文件名
    private String file;//上传的文件
    private List<GoodImportVo> erroImportList=new ArrayList<GoodImportVo>();
    private String errorFile;
     
    public String importGood(){
    	try {
    	UserInfo userInfo = this.getLoginUser();
    	if(userInfo != null){
        	String result = "";
            if (uploadFile()) {
            	File tempFile = new File(file);
            	if(!tempFile.exists()){
            		result = "要导入的文件不存在,请重新导入!";
            		return null;
            	}
                ExcelImport<GoodImportVo> schoolExcel = new ExcelImport(GoodImportVo.class);
        	    List<GoodImportVo> list = (ArrayList) schoolExcel.importExcel(1,tempFile);
        	    int importSuccessNum = 0;
        	    if(list != null && list.size() > 0){
        	    	String name = "";
        	    	String goodGroup = "";
        	    	String type = "";
        	    	String specifications = "";
        	    	String unit = "";
        	    	String price = "";
        	    	for(GoodImportVo v:list){
        	    		boolean isErrorRecord = true;
        	    		name = v.getName();
        	    		goodGroup = v.getGoodGroup();
        	    		type = v.getType();
        	    		specifications =v.getSpecifications();
        	    		unit = v.getUnit();
        	    		price = v.getPrice();
        	    		StringBuffer errorRecord = new StringBuffer();
        	    		if(!StringUtils.isNotBlank(name)){
        	    			errorRecord.append("<物资名称不能为空!>");
    	    				isErrorRecord = false;
        	    		}
        	    		
        	    		if(!StringUtils.isNotBlank(goodGroup)){
        	    			errorRecord.append("<物资组不能为空!>");
    	    				isErrorRecord = false;
        	    		}
        	    		GoodGroup cg= goodGroupService.findModel(goodGroup, userInfo.getCompanyId());
        	    		if(!StringUtils.isNotBlank(goodGroup)){
        	    			errorRecord.append("<物资组不能为空!>");
    	    				isErrorRecord = false;
        	    		}else{
        	    			if(cg==null){
        	    				errorRecord.append("<物资组不存在!>");
        	    				isErrorRecord = false;
        	    			}
        	    		}
        	    		//验证是否是正确的
        	    		if(isErrorRecord){
        	    			Good g= new Good();
     					    Order order = new Order(Direction.DESC, "createTime");
     				        Sort s = new Sort(order);
     				        Pageable pageable = getPageable(s);
     					    Page<Good> pageList = goodService.findGoodPage(pageable, null, null, null, userInfo.getCompanyId(),cg.getId());
     					    List<Good> lists= pageList.getContent();
     					    Good lastGood = null;
     					    if(lists!=null && lists.size()>0){
     					    	lastGood= lists.get(0);
     					    }
     					    String no="0000";
     					    int num=0;
     					    if(lastGood!=null){
     					    	num =lastGood.getNum();
     					    }
     					    num+=1;
						    if(num>=1 && num<10){
						    	no="0000"+num;
						    }else if(num>=10 && num<100){
						    	no="000"+num;
						    }else if(num>=100 && num<1000){
						    	no="00"+num;
						    }else if(num>=1000&& num<10000){
						    	no="0"+num;
						    }else if(num>=10000){
						    	no=""+num;
						    }
     					    g.setNum(num);
     					    g.setGoodNo(cg.getGoodCode()+num);//生成编码
         	    			g.setName(name);
         	    			g.setType(findTypeId(type));
         	    			g.setGoodGroup(cg);
         	    			g.setSpecifications(specifications);
         	    			g.setIsDelete(0);
         	    			g.setUnit(findUnitId(unit));
         	    			g.setPrice(Float.parseFloat(price));
         	    			g.setCompanyId(userInfo.getCompanyId());
         	    			goodService.saveOrUpdate(g);
        	    			importSuccessNum ++;
        	    		}else{
        	    			v.setImportErrorRecord(errorRecord.toString());
        	    			erroImportList.add(v);
        	    		}
        	    	}
        	    	
    	    	}
                String basePath = getRequest().getScheme() + "://" + getRequest().getServerName() + ":" + getRequest().getServerPort()+"/";
        	    if(erroImportList != null && erroImportList.size() > 0){
        	    	errorFile= exportResult();
        	    	result = "导入成功"+importSuccessNum+"条记录,"+erroImportList.size()+"条记录导入失败,请您<a href='"+basePath+"files"+errorFile+"'><font color='blue'>点击下载失败文件</font>"+"</a>";
        	    }else{
        	    	result = "导入成功!";
        	    }
        	    ajax(result);
            }
    	}
    } catch (Exception e) {
    	e.printStackTrace();
            LOGGER.error(e.getMessage());
            ajax("对不起！导入文件时出错！");
        }
        return null;
    	
    	
    }
    /**
     * 查找类型Id
     * @return
     */
    private Integer findTypeId(String type){
    	if("备件".equals(type)){
    		return 1;
    	}else if("材料".equals(type)){
    		return 2;
    	}else if("设备".equals(type)){
    		return 3;
    	}else if("仪表".equals(type)){
    		return 4;
    	}else{
    		return 5;
    	}
    }
    
	private Integer findUnitId(String unit){
		List<Dict> list= dictService.findList("goodsType", 1);
		Integer id=null;
		if(list!=null && list.size()>0){
			for(Dict d:list){
				if(d.getName().equals(unit)){
					id=d.getValue();
				}
			}
		}
		return id;
	}
    
    
    /**
     * 导出结果
     */
    private String exportResult()
    {
        String filePath="";
        try
        {
           if(erroImportList!=null && erroImportList.size()>0)
           {
               List<String> headList=new ArrayList<String>();
               headList.add("物资名称");
               headList.add("物资组");
               headList.add("类型");
               headList.add("规格");
               headList.add("单位");
               headList.add("单价");
               List<Map<String, Object>> mapList=new ArrayList<Map<String, Object>>();  //表内容
               Map<String, Object> map=null;
               int no = 1;
               for(GoodImportVo Vo:erroImportList)
               {
                    map=new HashMap<String, java.lang.Object>();
                    map.put("name",Vo.getName());
	                map.put("goodGroup", Vo.getGoodGroup());
	                map.put("type", Vo.getType());
	                map.put("specifications", Vo.getSpecifications());
	                map.put("unit", Vo.getUnit());
	                map.put("price", Vo.getPrice());
	                map.put("failureReason",Vo.getImportErrorRecord());
                    mapList.add(map);
               }
               List<String> keyList=new ArrayList<String>();   //map的key
               keyList.add("name");
               keyList.add("goodGroup");
               keyList.add("type");
               keyList.add("specifications");
               keyList.add("unit");
               keyList.add("price");
               keyList.add("failureReason");//导入错误提示记录
               String fileName="eventImport-"+UUID.randomUUID().toString()+".xls";
               String uploadPath = ""; // 上传的目录
               String catalinaHome = filePathConfig.getFileUploadPath();
               SimpleDateFormat sp = new SimpleDateFormat("MM/dd");
               String datePath = sp.format(new Date()); // 每月一个上传目录
               uploadPath = "/upload/importFail/" + datePath + "/";
               String path = catalinaHome + uploadPath;
               File checkPath = new File(path);
               if (!checkPath.exists()) {
                   //目录不存在，则创建目录
                   checkPath.mkdirs();
               }
               OutputStream os = new FileOutputStream(path+"/"+fileName);
               ExportExcel export=new ExportExcel(os,headList,mapList,keyList);
               export.export();
               filePath = uploadPath+fileName;
           }
        }
        catch (Exception ex)
        {
            LOGGER.error(ex.getMessage());
            ex.printStackTrace();
        }
        return filePath;
    }
    
    
    /**
     * 上传文件
     */
    @SuppressWarnings("unused")
	private boolean uploadFile() {
        String uploadPath = ""; // 上传的目录
        String catalinaHome = filePathConfig.getFileUploadPath();
        SimpleDateFormat sp = new SimpleDateFormat("MM/dd");
        String datePath = sp.format(new Date()); // 每月一个上传目录
        uploadPath = "/upload/import/" + datePath + "/";
        String targetDirectory = catalinaHome + uploadPath;
        try {
            if (fileToUpload != null) {
                String fileName = UUID.randomUUID().toString();
                String ext = ".xls";
                if (uploadFileName != null) {
                    ext = uploadFileName.substring(uploadFileName.lastIndexOf("."));
                }
                if (!ext.equals(".xls")) {
                    return false;
                }
                String saveFileName = fileName + ext;
                File savefile = new File(new File(targetDirectory), saveFileName);
                if (!savefile.getParentFile().exists()){
                    boolean result = savefile.getParentFile().mkdirs();
                    if (!result){
                        return false;
                    }
                }
                FileUtils.copyFile(fileToUpload, savefile);//拷贝文件
                file = targetDirectory + "/" + saveFileName;
            }

        } catch (Exception e) {

            return false;
        }
        return true;
    }
    
    /**
     * 判断字符串是否是整数
     */
    public static boolean isInteger(String value) {
     try {
      Integer.parseInt(value);
      return true;
     } catch (NumberFormatException e) {
      return false;
     }
    }

    
 
  


	public File getFileToUpload() {
		return fileToUpload;
	}


	public String getUploadFileName() {
		return uploadFileName;
	}


	public String getFile() {
		return file;
	}


	


	public String getErrorFile() {
		return errorFile;
	}


	public void setFileToUpload(File fileToUpload) {
		this.fileToUpload = fileToUpload;
	}


	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}


	public void setFile(String file) {
		this.file = file;
	}


	


	public void setErrorFile(String errorFile) {
		this.errorFile = errorFile;
	}



	

   
	
	
	

}
