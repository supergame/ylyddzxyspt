package cn.com.qytx.cbb.goods.service;

import java.util.List;

import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.goods.domain.GoodGroup;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.service.BaseService;

/**
 * 功能:资产接口
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月10日
 * 修改日期: 2016年8月10日
 * 修改列表: 
 */
public interface IGoodGroup extends BaseService<GoodGroup>{

	
	/**
	 * 功能：获取资产组列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<GoodGroup> findGoodGroupList(Integer companyId);
	
	
	/**
	 * 功能：获取资产组树列表
	 * @param companyId 公司Id
	 * @return
	 */
	public List<TreeNode> getTreeGoodGroupList(String path,Integer companyId);
	
	
	public List<TreeNode> getTreeGoodGroupList_Select(String path,Integer companyId);
	
	/**
	 * 在指定ID下面是否有相同的资产组名称
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameGroupName(Integer parentId,String groupName,int companyId);
	
	
	/**
	 * 在指定ID下面是否有相同的资产组编码
	 * 功能：parentId
	 * @param parentId
	 * @param groupName
	 * @return
	 */
	public boolean isHasSameGoodCode(Integer parentId,String goodCode,int companyId);
	
	
	/**
	 * 功能：判断是否存在子资产组
	 * @param id
	 * @param companyId
	 * @return
	 */
	public boolean isHasChildGoodGroup(Integer id,int companyId);
	
	/**
	 * 功能：通过Id传获取资产组列表
	 * @param Ids
	 * @return
	 */
	public List<GoodGroup> getGoodGroupListByIds(String Ids);
	
	/**
	 * 功能：判断是否存在资产组
	 * @param name
	 * @param companyId
	 * @return
	 */
	public GoodGroup findModel(String name, int companyId);
	
}
