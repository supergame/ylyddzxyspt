package cn.com.qytx.cbb.capital.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cn.com.qytx.cbb.capital.domain.Capital;
import cn.com.qytx.cbb.capital.vo.CapitalVo;
import cn.com.qytx.platform.base.dao.BaseDao;
import cn.com.qytx.platform.base.query.Page;
import cn.com.qytx.platform.base.query.Pageable;

/**
 * 功能:资产持久层操作
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
@Repository("capitalDao")
public class CapitalDao extends BaseDao<Capital, Integer> {

	public Page<Capital> findCapitalPage(Pageable pageable, CapitalVo capitalVo) {
		StringBuffer sbHql = new StringBuffer();
		sbHql.append("select c from Capital c where isDelete=0");
		if(capitalVo.getCompanyId()!=null){
			sbHql.append(" and c.companyId="+capitalVo.getCompanyId());
		}
		if(StringUtils.isNoneBlank(capitalVo.getCapitalStatus())){
			sbHql.append(" and c.status in ("+capitalVo.getCapitalStatus()+")");
		}
		if(StringUtils.isNotBlank(capitalVo.getKeyWord())){
			sbHql.append(" and (c.name like '%"+capitalVo.getKeyWord()+"%' or c.capitalNo like '%"+capitalVo.getKeyWord()+"%')");
		}
		if(StringUtils.isNotBlank(capitalVo.getUseUserIds())){
			sbHql.append(" and c.useUser.userId in ("+capitalVo.getUseUserIds()+")");
		}
		if(capitalVo.getCapitalGroup()!=null&&capitalVo.getCapitalGroup().intValue()!=0){
			sbHql.append(" and (c.capitalGroup.pathId like '%,"+capitalVo.getCapitalGroup()+",%')");
		}
		if(capitalVo.getCurrentGroupId()!=null){
			sbHql.append(" and (c.useGroup.groupId in (select g.groupId from GroupInfo g where g.groupId="+capitalVo.getCurrentGroupId()+"))");
		}
		if(StringUtils.isNotBlank(capitalVo.getUserGroupName())){
			sbHql.append(" and c.useGroup.groupId in (select g.groupId from GroupInfo g where g.groupName like '%"+capitalVo.getUserGroupName()+"%')");
		}
		
		if(StringUtils.isNoneBlank(capitalVo.getLocationName())){
			sbHql.append(" and storeLocation='"+capitalVo.getLocationName()+"'");
		}
		
		sbHql.append(" order by createTime desc,capitalNo desc");
		return super.findByPage(pageable, sbHql.toString());
	}

	public int getCurrCodeMaxCapitalNum(String capitalCode, int companyId) {
		String hql = "isDelete=0 and companyId = "+companyId+" and capitalNo like '"+capitalCode+"_%'";
		return super.count(hql);
	}

	public int getCurrNoMaxCapitalNum(String capitalCode, int companyId) {
		String hql = "isDelete=0 and companyId = "+companyId+" and capitalNo = '"+capitalCode+"_%'";
		return super.count(hql);
	}

	
}
