package cn.com.qytx.cbb.goods.action;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import cn.com.qytx.cbb.capital.domain.CapitalGroup;
import cn.com.qytx.cbb.capital.service.ICapitalGroup;
import cn.com.qytx.cbb.goods.domain.GoodGroup;
import cn.com.qytx.cbb.goods.service.IGoodGroup;
import cn.com.qytx.cbb.org.util.TreeNode;
import cn.com.qytx.platform.base.action.BaseActionSupport;
import cn.com.qytx.platform.org.domain.UserInfo;

/**
 * 功能:资产组管理
 * 版本: 1.0
 * 开发人员: panbo
 * 创建日期: 2016年8月12日
 * 修改日期: 2016年8月12日
 * 修改列表: 
 */
public class GoodGroupAction extends BaseActionSupport {
	private static final long serialVersionUID = 1L;
	/**
	 * 资产组业务接口
	 */
	@Resource(name = "goodGroupService")
	private IGoodGroup goodGroupService;
	/**
	 * 1 代表左侧树 2 代表 下拉树
	 */
	private Integer treeType;

	private GoodGroup goodGroup;

	/**
	 * 获取资源组树列表
	 */
	public void goodGroupTreeList() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				String contextPath = getRequest().getContextPath();
				List<TreeNode> list = null;
				if(treeType!=null&&treeType.intValue()==1){
					list = goodGroupService
							.getTreeGoodGroupList(contextPath,user.getCompanyId());
				}else{
					list = goodGroupService
							.getTreeGoodGroupList_Select(contextPath,user.getCompanyId());
				}
				Gson json = new Gson();
				ajax(json.toJson(list));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 资产组信息修改或新增资产组 资产组信息修改 成功：1； 资产组添加成功：0；
	 */
	public String saveOrUpdateGoodGroup() {
		try {
			UserInfo user = this.getLoginUser();
			if (user != null) {
				if (goodGroup.getId() != null) {//修改
					GoodGroup oldGoodGroup = goodGroupService.findOne(goodGroup.getId());
					if(oldGoodGroup!=null){
						String oldGroupName = oldGoodGroup.getGroupName();
						String oldGoodCode = oldGoodGroup.getGoodCode();
						//判断是否存在资产组名称
				    	if(!oldGroupName.equals(goodGroup.getGroupName())){
				    		boolean  isSame=goodGroupService.isHasSameGroupName(oldGoodGroup.getParentId(), goodGroup.getGroupName(),user.getCompanyId());
				        	if(isSame){
				                ajax("2");
				                return null;
				        	}
				    	}
				    	//判断是否存在资产组编码重复
				    	if(!oldGoodCode.equals(goodGroup.getGoodCode())){
				    		boolean isExistCapitalCode = goodGroupService.isHasSameGoodCode(oldGoodGroup.getParentId(), goodGroup.getGoodCode(), user.getCompanyId());
							if(isExistCapitalCode){
								ajax("3");
								return null;
							}
				    	}
				    	oldGoodGroup.setGoodCode(goodGroup.getGoodCode());
				    	oldGoodGroup.setGroupName(goodGroup.getGroupName());
				    	oldGoodGroup.setOrderIndex(goodGroup.getOrderIndex());
				    	oldGoodGroup.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				    	oldGoodGroup.setUpdateUserId(user.getUserId());
				    	goodGroupService.saveOrUpdate(oldGoodGroup);
						ajax("1");
					}
				} else {
						Integer parentId = 0;
						if(goodGroup.getParentId()!=null){
							parentId = goodGroup.getParentId();
						}
						//判断是否存在资产组名称
						boolean isExistGroupName = goodGroupService.isHasSameGroupName(parentId, goodGroup.getGroupName(), user.getCompanyId());
						if(isExistGroupName){
							ajax("2");
							return null;
						}
						//判断是否存在资产组编码
						boolean isExistGroupCode = goodGroupService.isHasSameGoodCode(parentId, goodGroup.getGoodCode(), user.getCompanyId());
						if(isExistGroupCode){
							ajax("3");
							return null;
						}
						goodGroup.setCompanyId(user.getCompanyId());
						goodGroup.setCreateTime(new Timestamp(System.currentTimeMillis()));
						goodGroup.setCreateUserId(user.getUserId());
						goodGroup.setIsDelete(0);
						goodGroup.setUpdateTime(new Timestamp(System.currentTimeMillis()));
						goodGroup.setUpdateUserId(user.getUserId());
						goodGroupService.saveOrUpdate(goodGroup);
						GoodGroup parentCapitalGroup = goodGroupService.findOne(parentId);
						if(parentCapitalGroup!=null){
							//设置路径
			    			String pathIdParent = parentCapitalGroup.getPathId();
			    			if(StringUtils.isNotBlank(pathIdParent)){
			    				goodGroup.setPathId(pathIdParent+goodGroup.getId()+",");
			    			}
			            	//设置级别
			    			Integer gradeParent=parentCapitalGroup.getGrade();
			    			if(gradeParent!=null){
			    				goodGroup.setGrade(gradeParent+1);
			    			}
						}else{
							goodGroup.setPathId(","+String.valueOf(goodGroup.getId())+",");
							goodGroup.setGrade(1);
						}
						goodGroupService.saveOrUpdate(goodGroup);
						ajax("0_"+goodGroup.getId());// 添加成功
					}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}
	
	
	/**
	 * 功能：删除资产组
	 * @return
	 */
	public String deleteGoodGroup(){
		UserInfo userInfo = this.getLoginUser();
		try{
			if(userInfo!=null){
				boolean isHasChild = goodGroupService.isHasChildGoodGroup(goodGroup.getId(),userInfo.getCompanyId());
				if(isHasChild){
					ajax("1");
					return null;
				}
				goodGroupService.delete(goodGroup.getId(), false);
				ajax("0");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public Integer getTreeType() {
		return treeType;
	}

	public void setTreeType(Integer treeType) {
		this.treeType = treeType;
	}

	public GoodGroup getGoodGroup() {
		return goodGroup;
	}

	public void setGoodGroup(GoodGroup goodGroup) {
		this.goodGroup = goodGroup;
	}

	
	
	
	
}
