$(function(){
	initUnit();
	var goodId=$("#goodId").val();
	if(goodId){
		$("#addOrUpdate").val("修改");
		findModleGood(goodId)
	}else{
		$("#addOrUpdate").val("新增");
	}
	loadGoodsGroupTree();
	//新增或修改
	$("#addOrUpdate").click(function(){
		addOrUpdateGood();
	})
	
});

function toggleSelectUnit(){
	$("#unit").show();
}


function toggleSelectGroup(){
	$("#goodsGroupTree").show();
}

function loadGoodsGroupTree(){
	var goodsGroupId = $("#goodsGroupId").val();
	if(!goodsGroupId){
		goodsGroupId = "";
	}else{
		goodsGroupId = "gid_"+goodsGroupId;
	}
	var url = basePath+"goods/selectGoodGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"goodsGroupTree",
				type:"radio",
				defaultSelectId:goodsGroupId,
				data:	data,
				click:	callBackGoodsGroup
			});
		}
	});
}

function callBackGoodsGroup(data)
{
    if(data){
    	var groupIds = [];
    	var groupNames = [];
    	$(data).each(function(i,item){
    		var uId = item.id.indexOf("gid_");
    		if(item.id.indexOf("gid_") != -1){
    			groupIds.push(item.id.substring(4));
    			groupNames.push(item.name);
        	}
    	});
        $("#goodsGroupText").text(groupNames[0]);
        $("#goodsGroupId").val(groupIds[0]);
    }
}



//增加资产
function addOrUpdateGood(){	
	var goodId = $("#goodId").val();
	var goodName = $("#goodName").val();
	if(!goodName){
		art.dialog.alert("物资名称不能为空!")
		return;
	}
	var specifications = $("#specifications").val();
	var type=$(":radio[name='goodType']:checked").val(); 
	var goodsGroupId = $("#goodsGroupId").val();//设备组
	if(!goodsGroupId){
		art.dialog.alert("物资组不能为空!")
		return;
	}
	var price = $("#price").val();
	var unitId = $("#unitId").val();
	var data={
			"type":type,
			"name":goodName,
			"groupId":goodsGroupId
	}
	$.ajax({
		url:basePath+"goods/findGoodBy.action",
		type:'post',
		data:data,
		dataType:'json',
		success:function(result){
			if(result==1){
				art.dialog.alert("物资已存在!");
				return
			}else if(result==2){
				art.dialog.tips("网络异常!");
				return;
			}else{
				var url = basePath+"goods/addOrUpdate.action";
				var param={
						"good.id":goodId,	
						"good.name":goodName,
						"good.goodGroup.id":goodsGroupId,
						"good.type":type,
						"good.specifications":specifications,
						"good.unit":unitId,
						"good.price":price
					}
				$.ajax({
					url:url,
					type:'post',
					data:param,
					dataType:'json',
					success:function(result){
						if(result==1){
							art.dialog.tips("修改成功!");
							goBack();
						}else if(result==2){
							art.dialog.tips("新增成功!");
							goBack();
						}else{
							art.dialog.tips("操作失败!");
						}
					}
				});
			}
		}
	})
	
}


/**
 * 返回
 */
function goBack(){
	window.location.href=basePath+"logined/goods/goodManage.jsp";
}

/**
 * 初始化单位
 * 
 */
function initUnit(){
	$.ajax({
		url:basePath+"goods/initUnit.action",
		type:"POST",
		data:null,
		dataType:"json",
		success:function(result){
			var html =""
			if(result){
				for(var i=0;i<result.length;i++){
					html+="<option value ='"+result[i].value+"' onclick='selectThis(this)'>"+result[i].name+"</option>"
				}
			}
			$("#unit").html(html);
		}
	})
	$("#unitText").html("请选择");
	$("#unitId").val(0);
}

function selectThis(obj){
	var value=$(obj).attr("value");
	var name=$(obj).text();
	$("#unitText").html(name);
	$("#unitId").val(value);
	$("#unit").hide();
}


function findModleGood(id){
	$.ajax({
		url:basePath+"goods/findDetail.action",
	    type:'post',
	    data:{"id":id},
	    dataType:'json',
	    success:function(result){
	    	if(result){
	    		$("#goodName").val(result.name);
	    		$("#goodNo").val(result.goodNo);
	    		$("#goodsGroupText").html(result.goodGroupName);
	    		$("#goodsGroupId").val(result.goodGroupId);
	    		 var value = document.getElementsByName("goodType");
	                for(var i =0;i<value.length;i++){
	                    var val =value[i];
	                    if(val.value==result.typeId){
	                    	val.checked=true;
	                        break;
	                    }
	                }
	    		
	    		$("#specifications").val(result.specifications);
	    		
	    		$("#unitText").html(result.unit);
	    		$("#unitId").val(result.unitId);
	    		$("#price").val(result.price);
	    	}
	    }
	})
}
