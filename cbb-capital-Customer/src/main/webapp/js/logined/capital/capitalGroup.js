$(function(){
	
	getCapitalGroupTree("");
	
});


function getCapitalGroupTree(defaultSelectId){
	var url = basePath+"capital/selectCapitalGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
			
		}
	});
}

function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	if(vid!="gid_0"){//修改资产组
		$("#operationType").val(2);
		$("#operationTitle").html("修改资产组");
		$("#formPage").show();
		$("#noData").hide();
		$("#groupName").val(name);
		$("#id").val(vid.substring(4));
		if(index_code!=null&&index_code!=""){
			$("#orderIndex").val(index_code.split("{1}")[0]);
			$("#capitalCode").val(index_code.split("{1}")[1]);
		}
	}else{
		$("#id").val("");
		$("#formPage").hide();
		$("#noData").show();
	}
}


//新增
function addCapitalGroup(){
	var id = $("#id").val();
	if(id!=undefined){
		$("#operationType").val(1);
		$("#operationTitle").html("新增资产组");
		$("#orderIndex").val("");
		$("#capitalCode").val("");
		$("#groupName").val("");
		$("#orderIndex").val("");
		$("#formPage").show();
		$("#noData").hide();
	}else{
		art.dialog.alert("请选择想要添加到的位置!");
		$("#formPage").hide();
		$("#noData").show();
	}
}
//删除
function delCapitalGroup(){
	var groupId = $("#id").val();
	if(groupId!=undefined&&groupId!=""){
		art.dialog.confirm("确定要删除吗?",function(){
			param = {"capitalGroup.id":groupId};
			$.ajax({
				url:basePath+"capital/deleteCapitalGroup.action",
				type:"POST",
				data:param,
				dataType:"text",
				success:function(result){
					if(result=="0"){
						art.dialog.tips("删除成功!");
						$("#id").val("");
						$(".formPage").hide();
						$("#noData").show();
						getCapitalGroupTree("");
					}else if(result=="1"){
						art.dialog.alert("该资产组下存在子资产组,不能删除!");
					}else{
						art.dialog.alert("操作失败,请稍后重试!");
					}
				}
			});
		});
	}else{
		art.dialog.alert("请选择要删除的资产组!");
	}
};
function saveOrUpdate(){
	var capitalGroup = {};
	var param;
	var groupName = $("#groupName").val();
	var orderIndex = $("#orderIndex").val();
	var capitalCode = $("#capitalCode").val();
	var operationType = $("#operationType").val();
	var id = $("#id").val();
	if(!groupName){
		art.dialog.alert("资产组名称不能为空!");
		return false;
	}
	if(!orderIndex){
		art.dialog.alert("排序号不能为空!");
		return false;
	}
	if(!capitalCode){
		art.dialog.alert("编码不能为空!");
		return false;
	}
	
	if(operationType==1){//添加
		if(id==""){
			id = 0;
		}
		param = {
				"capitalGroup.parentId":id,
				"capitalGroup.orderIndex":orderIndex,
				"capitalGroup.groupName":groupName,
				"capitalGroup.capitalCode":capitalCode
		}
	}else{
		param = {
				"capitalGroup.id":id,
				"capitalGroup.orderIndex":orderIndex,
				"capitalGroup.groupName":groupName,
				"capitalGroup.capitalCode":capitalCode
		}
	}

	$.ajax({
		url:basePath+"capital/saveOrUpdateCapitalGroup.action",
		type:"POST",
		data:param,
		dataType:"text",
		success:function(result){
			if(result!=null&&result.indexOf("0_")>-1){
				art.dialog.tips("添加成功!");
				//getCapitalGroupTree("gid_"+result.split("_")[1]);
				getCapitalGroupTree("");
			}else if(result=="1"){
				art.dialog.tips("修改成功!");
				//getCapitalGroupTree("gid_"+scope.id);
				getCapitalGroupTree("");
			}else if(result=="2"){
				art.dialog.alert("同层资产组名称不能重复!");
			}else if(result=="3"){
				art.dialog.alert("同层资产组编码不能重复!");
			}else{
				art.dialog.alert("操作失败,请稍后重试!");
			}
		}
	});
};
