$(function(){
	/**
	 * 页面状态用来区分页面
	 * 0入库; 1领用； 2归还； 3调拨； 4维修； 5报废； 6盘点； 
	 * */
	var type = $("#type").val();
	getCapitalDetail(type);
})

/**
	 * 获取资源详情
	 */
	function getCapitalDetail(type){
		var capitalId = art.dialog.data("capitalId");
			var param={
					"capitalId":capitalId
			};
			$.ajax({
				url:basePath+"capital/getCapitalDetail.action",
				type:'post',
				data:param,
				dataType:'json',
				success:function(data){
					$("#goodName").html(data.name);
					$("#name").html(data.name);
					$("#statusStr").html(data.statusStr);
					$("#capitalTypeStrDetail").html(data.capitalTypeStr);
					$("#useNum").html(data.useNum);
					if(data.capitalType==2){
						$("#oldNum").html(data.oldNum);
					}else{
						$("#oldNum").html(1);
					}
					$("#useCount").html(data.useCount);
					$("#repairNum").html(data.repairNum);
					$("#remaindNum").html(data.remaindNum);
					$("#useTime").html(data.useTime);
					$("#checkNum").html(data.checkNum);
					$("#capitalUnit").html(data.capitalUnit);
					if(data.price=="--"){
						$("#price").html("--");
					}else{
						$("#price").html(data.price+"元/"+data.capitalUnit);
					}
					
					$("#approveNo").html(data.approveNo);
					$("#supplier").html(data.supplier);
					$("#useGroupName").html(data.useGroupName);
					$("#useUserName").html(data.useUserName);
					$("#buyDate").html(data.buyDate);
					$("#storeLocation").html(data.name);
					$("#handleUserName").html(data.handleUserName);
					$("#invoiceNumber").html(data.invoiceNumber);
					$("#scrapTime").html(data.scrapTime);
					$("#remark").html(data.remark);
					$("#capitalGroup").html(data.capitalGroup);
					$("#storageTime").html(data.createTime);
					$("#capitalNo").html(data.capitalNo);
					$("#capitalBrand").html(data.capitalBrand);
					$("#capitalModel").html(data.capitalModel);
					$("#location").html(data.storeLocation);
					if(type==3||type==2){
						$("#useGroup").html(data.useGroupName);
						$("#usePeople").html(data.useUserName);
					}
					if(type==6){
						if(data.capitalType==1||data.capitalType==3){
							$("#pandianResult1").removeClass('hide');
							$("#pandianResult2").addClass('hide');
						}else{
							$("#pandianResult2").removeClass('hide');
							$("#pandianResult1").addClass('hide');
						}
					}
					if(type==4){
						if(data.status==1){
							$("#location").html(data.storeLocation);
						}
						if(data.status==2){
							$("#useGroup").html(data.useGroupName);
							$("#usePeople").html(data.useUserName);
						}
					}
					if(type==1){
						if(data.capitalType==2){
							$("#lingyongNum").removeClass('hide');
							$("#isShowCapitalNum").val(1);//显示
							$("#capitalTypeStr").html("办公用品(剩余数量"+data.capitalNum+")");
						}else{
							$("#capitalTypeStr").html(data.capitalTypeStr);
							$("#lingyongNum").addClass('hide');
							$("#isShowCapitalNum").val(2);//不显示
						}
					}
					if(type==5){
						if(data.capitalType==2){
							$("#capitalTypeStr").html("办公用品(剩余数量"+data.capitalNum+")");
						}else{
							$("#capitalTypeStr").html(data.capitalTypeStr);
						}
					}else{
						$("#approveRecord").html(data.approveRecord);
					}
				}
			})
		}
/**
 * 查看日志
 */
function findLog(){
	var capitalNo=$("#capitalNo").html();
	var url=basePath+"logined/capital/capitalLogList.jsp?capitalNo="+capitalNo;
	art.dialog.open(url, {
		title : '设备日志',
		width : 800,
		height : 600,
		lock : true,
		opacity : 0.08,
		 button : [{
					name : '关闭',
					callback : function() {
						art.dialog.data("id", "");
						return true;
					}
				}]
	});
}
