$(function(){
	
	getCapitalGroupTree("gid_0");
	
	$(document).keydown(function(event){
		var code=event.which;
		if (code == 13) {
			getTable();
			return false;
		}
	}); 
	
    getTable();
});




function getTable(){
	var capitalStatus=$("#capitalStatus").val();
    var keyWord=$.trim($("#keyWord").val());
    
    var useStartTime=$("#useStartTime").val();
    var useEndTime=$("#useEndTime").val();
    if(useStartTime){
    	useStartTime += ":00";
    }
    if(useEndTime){
    	useEndTime += ":00";
    }
    var disableStartTime=$("#disableStartTime").val();
    var disableEndTime=$("#disableEndTime").val();
    if(disableStartTime){
    	disableStartTime += ":00";
    }
    if(disableEndTime){
    	disableEndTime += ":00";
    }
    
    
	$('#capitalTable').dataTable({
		"bProcessing" : true,
		'bServerSide' : true,
		'fnServerParams' : function(aoData) {
			aoData.push({
						"name" : "capitalVo.capitalStatus",
						"value" : capitalStatus
					},{
						"name":"capitalVo.keyWord",
						"value":keyWord
					},{
						"name":"capitalVo.userGroupName",
						"value":$("#userGroupName").val()
					},{
						"name":"capitalVo.capitalGroup",
						"value":$("#selectGroupId").val()
					},{
						"name":"capitalVo.useStartTime",
						"value":useStartTime
					},{
						"name":"capitalVo.useEndTime",
						"value":useEndTime
					},{
						"name":"capitalVo.disableStartTime",
						"value":disableStartTime
					},{
						"name":"capitalVo.disableEndTime",
						"value":disableEndTime
					}
				);
		},
		"sAjaxSource" : basePath+"capital/capitalUse_capitalUseLogList.action",
		"sServerMethod" : "POST",
		"sPaginationType" : "full_numbers",
		"bPaginate" : true, // 翻页功能
		"bLengthChange" : false, // 改变每页显示数据数量
		"bFilter" : false, // 过滤功能
		"bSort" : false, // 排序功能
		"bInfo" : true,// 页脚信息
		"bAutoWidth" : false,// 自动宽度
		"bDestroy" : true,
		"iDisplayLength" : 15, // 每页显示多少行
		"aoColumns" : [
			    {
					"mDataProp" : "no",
				}, {
					"mDataProp" : "goodName",
				    "sClass" : "longTxt"
				}, {
					"mDataProp" : "capitalNo",
					"sClass" : "longTxt"
				}, {
					"mDataProp" : "capitalGroup"
				},{
					"mDataProp" : "statusStr"
				},{
					"mDataProp" : "useGroupName"
				},{
					"mDataProp" : "location"
				},{
					"mDataProp" : "startUseTime"
				},{
					"mDataProp" : "endUseTime"
				},{
					"mDataProp" : "useTimeLength"
				}],
		"oLanguage" : {
			"sUrl" : basePath + "plugins/datatable/cn.txt" // 中文包
		},
		"fnDrawCallback" : function(oSettings) {
			$('#knowlegeTable tbody  tr td').each(function() {
				this.setAttribute('title', $(this).text());
			});
              
		},
		"fnInitComplete" : function() {
			// 重置iframe高度
		
		},
		"aoColumnDefs" : []
	});
}	

function getCapitalGroupTree(defaultSelectId){
	var url = basePath+"capital/selectCapitalGroup.action";
	var param = {"treeType":1};
	$.ajax({
		url : url,
		type:'post',
		data:param,
		dataType : 'json',
		success : function(data) {
			qytx.app.tree.base({
				id	:	"myTree",
				defaultSelectId:defaultSelectId,
				data:	data,
				click:	function(nodes){
					onTreeNodeClick(nodes);
				}
			});
		}
	});
}

function onTreeNodeClick(nodes){
	if(nodes&&nodes.length > 0){
		var name = nodes[0].name;
		var vid = nodes[0].id;
		var pId = nodes[0].pId;
		var index_code = nodes[0].obj;
		treeNodeClick(name, vid, pId,index_code);
	}
}

function treeNodeClick(name, vid, pid,index_code) {
	var type=$("#type").val();
	if(vid.substring(4)!="0"){
		$("#selectGroupId").val(vid.substring(4));
	}else{
		$("#selectGroupId").val("");
	}
	getTable(type);
}


function exportUseLog(){
	var capitalStatus=$("#capitalStatus").val();
    var keyWord=$.trim($("#keyWord").val());
    var useStartTime=$("#useStartTime").val();
    var useEndTime=$("#useEndTime").val();
    if(useStartTime){
    	useStartTime += ":00";
    }
    if(useEndTime){
    	useEndTime += ":00";
    }
    var disableStartTime=$("#disableStartTime").val();
    var disableEndTime=$("#disableEndTime").val();
    if(disableStartTime){
    	disableStartTime += ":00";
    }
    if(disableEndTime){
    	disableEndTime += ":00";
    }
	var url = basePath+"capital/capitalUse_exportUseLog.action?capitalVo.capitalStatus="+capitalStatus+"&capitalVo.keyWord="+encodeURI(encodeURI(keyWord));
	url += "&capitalVo.userGroupName="+encodeURI(encodeURI($("#userGroupName").val()))+"&capitalVo.capitalGroup="+$("#selectGroupId").val();
	url += "&capitalVo.useStartTime="+useStartTime+"&capitalVo.useEndTime="+useEndTime+"&capitalVo.disableStartTime="+disableStartTime+"&capitalVo.disableEndTime="+disableEndTime;
	window.location.href = url;
}
