<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>物资详情</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/form/skins/form_default.css"
	rel="stylesheet" type="text/css" />
<link href="${ctx }flat/plugins/datatable/skins/datatable_default.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}flat/js/base.js"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/goods/goodDetail.js"></script>
<style>
.inputTable th {
	width: 105px;
}
.big_title {
	border-bottom: 1px solid #017a8f;
	color: #017a8f;
	font-size: 16px;
	height: 40px;
	line-height: 45px;
	overflow: hidden;
	padding: 0 10px;
	text-align: left;
}
</style>
</head>
<body style=" background: #fff">
	<div class="formPage" style="padding-top:0px;width:auto;">
		<div class="formbg">
			<div class="content_form">
				<table width="100%" cellspacing="0" cellpadding="0" border="0"
					class="inputTable" style="font-size:14px;padding-top: 0px">
					<tbody>
						<tr>
							<th>物资名称：</th>
							<td id="name"></td>
						</tr>
						<tr>
						    <th>物资编号：</th>
							<td id="goodNo"></td>
						</tr>
						<tr>
							<th>物资组：</th>
							<td id="goodGroupName"></td>
							
						</tr>
						<tr>
						    <th>类型：</th>
							<td id="type"></td>
						</tr>
						<tr>
							<th>规格：</th>
							<td id="specifications"></td>
							
						</tr>
						<tr>
						    <th>单位：</th>
							<td id="unit"></td>
						</tr>
						<tr>
							<th>单价：</th>
							<td id="price"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>