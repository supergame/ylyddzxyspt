<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>资产入库</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
  	<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
	<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<style >
    .p15{padding-right: 15px;}
    .new{border-radius: 3px;height: 32px;}
 	.textarea{width:100% ; height: 74px;border: 1px solid #e4e4e4;}
  	.formPage .textarea:hover , .formPage .formTextarea:hover{border:1px solid #cecece;}
	.formPage .textarea:focus , .formPage .formTextarea:focus{border:1px solid #459fd2;} 
	.formPage .divselect12:hover , .formPage .divselect12:hover{border:1px solid #459fd2;}
	.formPage .divselect12:focus , .formPage .divselect12:focus{border:1px solid #cecece;}
	.location{float: left;display: block;} 
</style>
<script type="text/javascript" src="${ctx}js/common/validate.js?version=${version}"></script>
<!-- tree -->
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<script TYPE="text/javascript" src="${ctx }js/logined/goods/goodAdd.js"></script>
</head>
<input  type="hidden" id="goodId" value="${param.goodId}"/>
<body>
<form id="form1">
<div class="formPage" style="padding-top:0px">
  <div class="formbg" style="height: 510px;">
    <div class="big_title">物资信息</div>  
    <div class="content_form">
      <table width="100%" border="0" cellpadding="0" cellspacing="0"  class="inputTable">
        <tr>
          <th><em class="requireField">*</em><label>物资名称：</label></th>
          <td><input id="goodName" type="text" class="formText " style="display:block;border-radius: 3px;height: 32px;width: 50%;" placeholder="请输入设备名称（最多50字）" maxlength="50" name="goodName" 
           valid="required" errmsg="good.goodName_not_null"/>
          </td>
        </tr>
         <tr>
          <th><em class="requireField">*</em><label>物资组：</label></th>
           <td >
            <div class="divselect12  divselect13 " onmouseleave="javascript:$('#goodsGroupTree').hide()">
		      	<cite class="cite12 text_ellipsis cite13"  onClick="toggleSelectGroup()" id="goodsGroupText"></cite>
	      		<ul id="goodsGroupTree" class="ztree leibie"  style="display: none;height:200px !important;width:200px !important;position: relative;z-index: 999;">
				</ul>
		  	</div>
		  	<input type="hidden" id="goodsGroupId" errmsg="good.goodsGroupId_not_null" name="goodsGroupId" valid="required"/>
           </td>
        </tr>
         <tr>
          <th><em class="requireField">*</em><label>类型：</label></th>
          <td> <label class="p15"><input name="goodType" type="radio" value="1" checked="checked"/>备件 </label> 
			   <label class="p15"><input name="goodType" type="radio" value="2" />材料 </label> 
			   <label class="p15"><input name="goodType" type="radio" value="3" />设备 </label> 
			   <label class="p15"><input name="goodType" type="radio" value="4" />仪表 </label> 
			   <label class="p15"><input name="goodType" type="radio" value="5" />其它 </label> 
		 </td>
        </tr>
        <tr>
          <th><label>规格：</label></th>
          <td><input type="text" class="formText" style="border-radius: 3px;height:32px;width:50%;" id="specifications" maxlength="50" /></td>
        </tr>
        <tr>
           <th><label>单位：</label></th>
          <td>
            <div class="divselect12  divselect13" >
		      	<cite class="cite12 text_ellipsis cite13"  onClick="toggleSelectUnit()" id="unitText"></cite>
	      		<ul id="unit" class="ztree leibie"  style="display: none;height:200px !important;width:200px !important;position: relative;z-index: 999;">
				</ul>
		  	</div>
		  	<input type="hidden" id="unitId"/>
           </td>
        </tr>
         <tr>
           <th>单价：</label></th>
          <td>
          <input type="text" class="formText" style="border-radius: 3px;height: 32px;width:50%;" id="price" maxlength="50" />
          </td> 
        </tr>
        </table>
    </div>
</div>
  <div class="buttonArea" id="isShowOper">
    <input type="button" style="height: 38px;" class="formButton_grey" value="取消" onClick="goBack()"/>
    <input type="button" style="height: 38px;" class="formButton_green"  id="addOrUpdate"  />
  </div>
</div>
</form>
</body>
</html>