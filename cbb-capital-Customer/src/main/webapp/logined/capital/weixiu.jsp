<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="weixiuApp">
<head>
    <title>资产维修</title>
    <jsp:include page="../../common/flatHead.jsp"/>
    <link href="${ctx }flat/css/reset.css" rel="stylesheet" type="text/css" />
  	<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${ctx}flat/plugins/tree/skins/tree_default.css" type="text/css"/>
	<link href="${ctx }flat/plugins/form/skins/form_default.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="${ctx}plugins/My97DatePicker/WdatePicker.js"></script>
<style >
	 body{background-color: #fff;}
  	.formPage{width: 100%;height: 100%;padding-top: 0;} 
</style>
<script type="text/javascript" src="${ctx }js/logined/capital/weixiu.js"></script>
<script type="text/javascript" src="${ctx }js/logined/capital/commonDetail.js"></script>
</head>
<body >
<input id="type" type="hidden" value="${param.type}">
<div class="formPage">
  <div class="formbg" >
    <div class="content_form">
    <form name="form" >
      <table width="100%" border="0" cellpadding="0" cellspacing="0"  class="inputTable">
        <tr>
          <th><label>资产名称：</label></th>
          <td id="goodName"></td>
        </tr>
        <tr>
          <th><label>资产组：</label></th>
          <td id="capitalGroup"></td>
        </tr>
        <tr>
          <th><label>资产编号：</label></th>
          <td id="capitalNo"></td>
        </tr>
        <tr>
          <th><label>入库日期：</label></th>
          <td id="storageTime"></td>
        </tr>
        <tr>
          <th><label>资产品牌：</label></th>
          <td id="capitalBrand"></td>
        </tr>
        <tr>
          <th><label>资产型号：</label></th>
          <td id="capitalModel"></td>
        </tr>
        <tr>
          <th><label>资产类型：</label></th>
          <td id="capitalTypeStr"></td>
        </tr>
        <tr >
          <th><label>存放位置：</label></th>
          <td id="location"></td>
        </tr>
        <tr >
          <th><label>使用部门：</label></th>
          <td id="useGroup"></td>
        </tr>
        <tr >
          <th><label>使用人：</label></th>
          <td id="usePeople"></td>
        </tr>
        <tr>
			<th><label>维修时间：</label></th>
			<td>
				<input type="text" id="repairTime"  onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd'})" class="Wdate formText" />
			</td> 
		</tr>
		<tr>
		<th><label>维修费用：</label></th>
			<td>
				<input id="repairMoney" name="repairMoney" type="text" class="formText" style="width:78%;" placeholder="请输入维修费用" maxlength="6" /> 元
				
			</td> 
		</tr>
		<tr>
		<th><label>维修单位：</label></th>
			<td>
				<input id="repairCompany" type="text" class="formText" style="width:78%;" placeholder="请输入维修单位" maxlength="50" />
			</td> 
		</tr>
		<tr>
		<th><label>维修结果：</label></th>
			<td>
				<input id="repairResult" type="text" class="formText" style="width:78%;" placeholder="请输入维修结果" maxlength="50"/>
			</td> 
		</tr>
     </table>
     </form>
  </div>
</div>
</div>
</body>
</html>