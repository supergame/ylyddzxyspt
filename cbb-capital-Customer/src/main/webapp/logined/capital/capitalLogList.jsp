<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<meta charset="utf-8">
<title>设备日志</title>
<jsp:include page="../../common/flatHead.jsp" />
<link href="${ctx}flat/css/reset.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/hm_style.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/css/main.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/tree/skins/tree_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}flat/plugins/Accormenus/skins/Accormenus_default.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/flat/plugins/datatable/skins/datatable_default.css" rel="stylesheet" type="text/css" />
<!-- tree -->
<script type="text/javascript" src="${ctx}plugins/tree/ztree/jquery.ztree.all-3.5.min.js?version=${version}"></script>
<!-- 页面JS -->
<script type="text/javascript" src="${ctx }js/logined/capital/capitalLogList.js"></script>
<body>
<input type="hidden" id="capitalNo" value="${param.capitalNo}">
	<div class="list" style="overflow:visible;">
		<div class="center_content">
		<table cellpadding="0" cellspacing="0"  class="pretty dataTable" style="font-size: 14px;" id="myTable">
			<thead>
				<tr>
		       	 	<th style="width:42px;" id="no">序号</th>
		        	<th class="tdCenter" style="width:120px;" id="createTime">操作时间</th>
		        	<th class="tdCenter" style="width:120px;" id="userName">操作人</th>
		        	<th class="tdCenter" style="width:60px;" id="typeStr">操作类型</th>
		        	<th class="tdCenter" style="width:150px;" id="ipAddress">IP地址</th>
		     	</tr>
			</thead>
		</table>
		</div>
	</div>
</body>
</html>